package com.vantec.epr.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vantec.epr.command.CustomValueGeneratorCommand;
import com.vantec.epr.command.DocumentReferenceGenerator;
import com.vantec.epr.entity.BudgetBody;
import com.vantec.epr.entity.BudgetDetail;
import com.vantec.epr.entity.BudgetHeader;
import com.vantec.epr.model.BudgetRequest;
import com.vantec.epr.repository.BudgetBodyRepository;
import com.vantec.epr.repository.BudgetDetailRepository;
import com.vantec.epr.repository.BudgetHeaderRepository;
import com.vantec.epr.repository.DepartmentRepository;
import com.vantec.epr.repository.UserDivDeptRepository;
import com.vantec.epr.repository.UsersRepository;

@Service
public class NewBudgetServiceImpl implements NewBudgetService {

	@Autowired
	BudgetHeaderRepository budgetHeaderRepository;

	@Autowired
	BudgetBodyRepository budgetBodyRepository;

	@Autowired
	BudgetDetailRepository budgetDetailRepository;

	@Autowired
	UserDivDeptRepository userDivDeptRepository;

	@Autowired
	UsersRepository usersRepository;

	@Autowired
	DepartmentRepository departmentRepository;

	private static final DecimalFormat df = new DecimalFormat("0.00");
	private static Logger logger = LoggerFactory.getLogger(BudgetServiceImpl.class);

	@Override
	public String saveBudgets(BudgetRequest[] budgetRequestList) {
		for (BudgetRequest budgetRequest : budgetRequestList) {

			logger.info("user id  " + budgetRequest.getUserId());
			logger.info("budgetRequest.getFy()  " + budgetRequest.getFy());
			logger.info("budgetRequest.getCostCentreName()  " + budgetRequest.getCostCentreName());
			logger.info("planned value  " + budgetRequest.getPlanned());
			logger.info("number of months  " + budgetRequest.getNoOfMonths());
			BudgetHeader budgetHeader = budgetHeaderRepository
					.findByFinancialYearAndDepartmentName(budgetRequest.getFy(), budgetRequest.getCostCentreName());

			if (budgetHeader == null) {
				budgetHeader = createBudgetHeader(budgetRequest);
			}
			if (budgetRequest.getBudgetBodyId() == null) {
				BudgetBody budgetBody = createBudgetBody(budgetRequest, budgetHeader);
				createBudgetDetail(budgetRequest, budgetBody);
			} else {
				BudgetBody budgetBody = updateBudgetBody(budgetRequest, budgetHeader);
				updateBudgetDetail(budgetRequest, budgetBody);

			}

		}
		return "Budget Created";

	}

	private void createBudgetDetail(BudgetRequest budgetRequest, BudgetBody budgetBody) {

		Double preAvailble = 0.0;
		Long id = budgetBody.getId();
		for (int i = 0; i < budgetRequest.getMonthsSelected().length; i++) {

			BudgetDetail budgetDetail = new BudgetDetail();
			budgetDetail.setBudgetBodyId(id);
			// budgetDetail.setAccrued(budgetRequest.getAccrued());
			budgetDetail.setCategoryName(budgetRequest.getCategoryName());
			budgetDetail.setCostCentreName(budgetRequest.getCostCentreName());
			budgetDetail.setMonth(budgetRequest.getMonthsSelected()[i]);
			budgetDetail.setYear(budgetRequest.getYear());
			budgetDetail.setCreatedBy(budgetRequest.getCreatedBy());
			budgetDetail.setDateCreated(new Date());
			budgetDetail.setDateUpdated(new Date());
			budgetDetail.setItem(budgetRequest.getItem());
			// Double plannedValue = (double) Math.round(budgetRequest.getMonthsValue()[i]);
			// logger.info("pppp plannedValue " + plannedValue);
			double roundPlannedValue = Double.parseDouble(df.format(budgetRequest.getMonthsValue()[i]));
			budgetDetail.setPlanned(roundPlannedValue);
			budgetDetail.setSpend(budgetRequest.getSpend());
			budgetDetail.setCommited(0.0);
			// budgetDetail.setAccrued(budgetDetail.getSpend()-budgetDetail.getPlanned());
			BigDecimal available = new BigDecimal(preAvailble + (budgetDetail.getPlanned() - budgetDetail.getSpend()))
					.setScale(4, RoundingMode.HALF_UP);
			budgetDetail.setAvailable(available.doubleValue());
			budgetDetail.setUpdatedBy(budgetRequest.getCreatedBy());

			// to calculate avaialble
			preAvailble = budgetDetail.getAvailable();

			budgetDetailRepository.save(budgetDetail);

		}

	}

	private BudgetHeader createBudgetHeader(BudgetRequest budgetRequest) {
		BudgetHeader budgetHeader = new BudgetHeader();
		budgetHeader.setCreatedBy(budgetRequest.getCreatedBy());
		budgetHeader.setDateCreated(new Date());
		budgetHeader.setDateUpdated(new Date());
		budgetHeader.setDepartmentName(budgetRequest.getCostCentreName());
		budgetHeader.setFinancialYear(budgetRequest.getFy());
		budgetHeader.setUpdatedBy(budgetRequest.getCreatedBy());
		budgetHeader = budgetHeaderRepository.save(budgetHeader);

		CustomValueGeneratorCommand<String> customValueGeneratorCommand = new DocumentReferenceGenerator();
		String budgetReference = customValueGeneratorCommand.generate(budgetHeader.getId(), "BUD");
		budgetHeader.setBudgetReferenceCode(budgetReference);
		budgetHeader = budgetHeaderRepository.save(budgetHeader);
		return budgetHeader;
	}

	private BudgetBody createBudgetBody(BudgetRequest budgetRequest, BudgetHeader budgetHeader) {

		BudgetBody budgetBody = new BudgetBody();
		budgetBody.setCategoryName(budgetRequest.getCategoryName());
		budgetBody.setCostCentreName(budgetRequest.getCostCentreName());
		budgetBody.setCreatedBy(budgetRequest.getCreatedBy());
		budgetBody.setDateCreated(new Date());
		budgetBody.setDateUpdated(new Date());
		budgetBody.setItem(budgetRequest.getItem());
		// logger.info("body monthly value "+budgetRequest.getBodyMonthlyValue());
		// logger.info("planned value "+budgetRequest.getPlanned());
		if (budgetRequest.getBodyMonthlyValue() == null) {
			logger.info("monthly value is null seetting planned value");
			double roundPlannedValue = Double.parseDouble(df.format(budgetRequest.getPlanned()));
			logger.info("" + roundPlannedValue);
			budgetBody.setPlanned(roundPlannedValue);
		} else {
			// logger.info("setting monthly value");
			double roundPlannedValue = Double.parseDouble(df.format(budgetRequest.getBodyMonthlyValue()));
			// logger.info(""+roundPlannedValue);
			budgetBody.setPlanned(roundPlannedValue);
		}
		budgetBody.setSageCode(budgetRequest.getSageCode());
		budgetBody.setSpend(budgetRequest.getSpend());
		budgetBody.setAccrued(budgetBody.getPlanned() - budgetBody.getSpend());
		budgetBody.setAvailable(budgetBody.getPlanned() - budgetBody.getSpend());
		budgetBody.setCommited(0.0);
		budgetBody.setUpdatedBy(budgetRequest.getCreatedBy());
		budgetBody.setBudgetheaderId(budgetHeader.getId());
		budgetBody.setPaymentFrequency(budgetRequest.getPaymentFrequency());
		budgetBody.setUnitPrice(budgetRequest.getUnitPrice());
		budgetBody.setNoOfItems(budgetRequest.getNoOfItems());

		budgetBody = budgetBodyRepository.save(budgetBody);
		return budgetBody;
	}

	private void updateBudgetDetail(BudgetRequest budgetRequest, BudgetBody budgetBody) {

		Double preAvailble = 0.0;
		for (int i = 0; i < budgetRequest.getMonthsSelected().length; i++) {

			BudgetDetail budgetDetail = budgetDetailRepository.findByBudgetBodyIdAndMonth(budgetBody.getId(),
					budgetRequest.getMonthsSelected()[i]);
			if (budgetDetail != null) {
				budgetDetail.setCategoryName(budgetRequest.getCategoryName());
				budgetDetail.setCostCentreName(budgetRequest.getCostCentreName());
				budgetDetail.setItem(budgetRequest.getItem());
				budgetDetail.setDateUpdated(new Date());
				budgetDetail.setUpdatedBy(budgetRequest.getCreatedBy());
				double roundPlannedValue = Double.parseDouble(df.format(budgetRequest.getMonthsValue()[i]));
				budgetDetail.setPlanned(roundPlannedValue);
				BigDecimal available = new BigDecimal(
						preAvailble + (budgetDetail.getPlanned() - budgetDetail.getSpend()))
						.setScale(4, RoundingMode.HALF_UP);
				budgetDetail.setAvailable(available.doubleValue());
				

			}
			else {
				budgetDetail = new BudgetDetail();
				budgetDetail.setBudgetBodyId(budgetBody.getId());
				// budgetDetail.setAccrued(budgetRequest.getAccrued());
				budgetDetail.setCategoryName(budgetRequest.getCategoryName());
				budgetDetail.setCostCentreName(budgetRequest.getCostCentreName());
				budgetDetail.setMonth(budgetRequest.getMonthsSelected()[i]);
				budgetDetail.setYear(budgetRequest.getYear());
				budgetDetail.setCreatedBy(budgetRequest.getCreatedBy());
				budgetDetail.setDateCreated(new Date());
				budgetDetail.setDateUpdated(new Date());
				budgetDetail.setItem(budgetRequest.getItem());
				// Double plannedValue = (double) Math.round(budgetRequest.getMonthsValue()[i]);
				// logger.info("pppp plannedValue " + plannedValue);
				double roundPlannedValue = Double.parseDouble(df.format(budgetRequest.getMonthsValue()[i]));
				budgetDetail.setPlanned(roundPlannedValue);
				budgetDetail.setSpend(budgetRequest.getSpend());
				budgetDetail.setCommited(0.0);
				// budgetDetail.setAccrued(budgetDetail.getSpend()-budgetDetail.getPlanned());
				BigDecimal available = new BigDecimal(preAvailble + (budgetDetail.getPlanned() - budgetDetail.getSpend()))
						.setScale(4, RoundingMode.HALF_UP);
				budgetDetail.setAvailable(available.doubleValue());
				budgetDetail.setUpdatedBy(budgetRequest.getCreatedBy());

				// to calculate avaialble
				preAvailble = budgetDetail.getAvailable();

				
			}
			budgetDetailRepository.save(budgetDetail);
		}

	}

	private BudgetBody updateBudgetBody(BudgetRequest budgetRequest, BudgetHeader budgetHeader) {

		BudgetBody budgetBody = budgetBodyRepository.getOne(budgetRequest.getBudgetBodyId());
		budgetBody.setCategoryName(budgetRequest.getCategoryName());
		budgetBody.setCostCentreName(budgetRequest.getCostCentreName());
		budgetBody.setDateUpdated(new Date());
		budgetBody.setItem(budgetRequest.getItem());
		budgetBody.setSageCode(budgetRequest.getSageCode());
		double roundPlannedValue = Double.parseDouble(df.format(budgetRequest.getPlanned()));
		logger.info("" + roundPlannedValue);
		budgetBody.setPlanned(roundPlannedValue);
		budgetBody.setAccrued(budgetBody.getPlanned() - budgetBody.getSpend());
		budgetBody.setUpdatedBy(budgetRequest.getCreatedBy());
		budgetBody = budgetBodyRepository.save(budgetBody);
		return budgetBody;
	}
	
	public String deleteBudgets(Long[] budgetRequestList) {
		
		for (Long budgetBodyId : budgetRequestList) {
			budgetDetailRepository.deleteAllByBudgetBodyId(budgetBodyId);
			budgetBodyRepository.delete(budgetBodyRepository.getOne(budgetBodyId));
		}
		
		return "Deleted";
		
	}

}
