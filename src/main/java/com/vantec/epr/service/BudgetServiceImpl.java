package com.vantec.epr.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vantec.epr.command.CustomValueGeneratorCommand;
import com.vantec.epr.command.DocumentReferenceGenerator;
import com.vantec.epr.entity.BudgetBody;
import com.vantec.epr.entity.BudgetDetail;
import com.vantec.epr.entity.BudgetHeader;
import com.vantec.epr.model.BudgetRequest;
import com.vantec.epr.repository.BudgetBodyRepository;
import com.vantec.epr.repository.BudgetDetailRepository;
import com.vantec.epr.repository.BudgetHeaderRepository;
import com.vantec.epr.repository.DepartmentRepository;
import com.vantec.epr.repository.UserDivDeptRepository;
import com.vantec.epr.repository.UsersRepository;

@Service("BudgetService")
public class BudgetServiceImpl implements BudgetService {
	
	@Autowired
	BudgetHeaderRepository budgetHeaderRepository;
	
	@Autowired
	BudgetBodyRepository budgetBodyRepository;
	
	@Autowired
	BudgetDetailRepository budgetDetailRepository;
	
	
	
	@Autowired
	UserDivDeptRepository userDivDeptRepository;
	
	@Autowired
	UsersRepository usersRepository;
	
	@Autowired
	DepartmentRepository departmentRepository;
	
	private static final DecimalFormat df = new DecimalFormat("0.00");
	private static Logger logger = LoggerFactory.getLogger(BudgetServiceImpl.class);

	@Override
	public String saveBudget(BudgetRequest[] budgetRequestList) {
		
		for (BudgetRequest budgetRequest : budgetRequestList) {
			
		
		
		logger.info("user id  " + budgetRequest.getUserId());
		//Users user = usersRepository.findById(budgetRequest.getUserId());
		//UserDivisionDepartment userDivisionDepartment = userDivDeptRepository.findByUser(user);
		//Department department = departmentRepository.findById(userDivisionDepartment.getDeptId());
		//budgetRequest.setDeptName(department.getDeptName());
		
		logger.info("budgetRequest.getFy()  " + budgetRequest.getFy());
		logger.info("budgetRequest.getCostCentreName()  " + budgetRequest.getCostCentreName());
		logger.info("planned value  " + budgetRequest.getPlanned());
		logger.info("number of months  " + budgetRequest.getNoOfMonths());
		BudgetHeader budgetHeader = budgetHeaderRepository.findByFinancialYearAndDepartmentName(budgetRequest.getFy(), budgetRequest.getCostCentreName());
		
		
		
		if(budgetRequest.getConsecutiveMonths()==null){
		
			 if(budgetHeader == null){
				 budgetHeader = createBudgetHeader(budgetRequest);
			 }
			 BudgetBody budgetBody = createBudgetBody(budgetRequest,budgetHeader);
			 createBudgetDetail(budgetRequest, budgetBody);
			
		}else{
			 String fyYear = budgetRequest.getFy();
			 BudgetBody budgetBody = null;
			 Integer remainingMonth = 0;
			 Double avail=0.0;
				for(int i=0;i<=budgetRequest.getConsecutiveMonths().length-1;i++){
					 String monthYear = budgetRequest.getConsecutiveMonths()[i];
					 String monthYears[] = monthYear.split("-");
					 String month = monthYears[0];
					 String year = monthYears[1];
					  
				  
					 if(i==0){
						 if(budgetHeader == null){
							 budgetHeader = createBudgetHeader(budgetRequest);
						 }
						 Integer monthsForFirstYear = getTotalNumberOfMonthsForFirstYear(month,budgetRequest.getNoOfMonths());
						 //logger.info("monthsForFirstYear " + monthsForFirstYear);;
						 double value = getBudgetBodyPlannedValue(budgetRequest.getMonthlyValue(),monthsForFirstYear);
						 budgetRequest.setBodyMonthlyValue(value);
						 remainingMonth = budgetRequest.getNoOfMonths() - monthsForFirstYear;
						 budgetBody = createBudgetBody(budgetRequest,budgetHeader);
						 avail = createBudgetConsecutiveMonths(budgetRequest,budgetBody.getId(),month,fyYear,avail);
					 }else if(i>0){
					     if(month.equalsIgnoreCase("april") && i!=0){
							 Integer nextYear = Integer.parseInt(year)+1;
							 fyYear = "FY" + year + " - " + nextYear;
					    	 budgetRequest.setFy(fyYear);
					    	 budgetHeader = budgetHeaderRepository.findByFinancialYearAndDepartmentName(budgetRequest.getFy(), budgetRequest.getCostCentreName());
					    	 if(budgetHeader == null){
					    		 budgetHeader = createBudgetHeader(budgetRequest);
					    	 }
							 Integer monthsForNextYear  = getTotalNumberOfMonthsForFirstYear("april",remainingMonth);
							 double value = getBudgetBodyPlannedValue(budgetRequest.getMonthlyValue(),monthsForNextYear);
							 budgetRequest.setBodyMonthlyValue(value);
							 remainingMonth = remainingMonth - monthsForNextYear;
							 //logger.info("remainingMonth " + remainingMonth);
					    	 budgetBody = createBudgetBody(budgetRequest,budgetHeader);
					    	 avail = createBudgetConsecutiveMonths(budgetRequest,budgetBody.getId(),month,fyYear,avail);
					     }else{
					    	 avail = createBudgetConsecutiveMonths(budgetRequest,budgetBody.getId(),month,fyYear,avail);
					     }
						
					 }
					 
				}
			
		}
		
		}
	
		
		return "Budget Created";
		
	}


	private void createBudgetDetail(BudgetRequest budgetRequest, BudgetBody budgetBody) {
		//One Off Payment
		if(budgetRequest.getMonth() != null && budgetRequest.getNoOfMonths()==0 ){
			createBudgetDetailOneOffPayement(budgetRequest,budgetBody.getId());
		}
		
		//Specified Months equally
		if(budgetRequest.getMonths()!=null && budgetRequest.getMonths().length>0){
			budgetRequest.setPlanned(budgetRequest.getPlanned()/budgetRequest.getMonths().length);
			createBudgetSpecifiedMonthsEqually(budgetRequest,budgetBody.getId());
		}
		
		//Specified Months not equally
		if(budgetRequest.getMonthsSelected()!=null && budgetRequest.getMonthsSelected().length>0){
			createBudgetSpecifiedMonthsNotEqually(budgetRequest,budgetBody.getId());
		}
		
		
		//Consecutive Months
		if(budgetRequest.getNoOfMonths()>0){
			//createBudgetConsecutiveMonths(budgetRequest,budgetBody.getId());
		}
	}

	private BudgetHeader createBudgetHeader(BudgetRequest budgetRequest) { 
		BudgetHeader budgetHeader = new BudgetHeader();
		budgetHeader.setCreatedBy(budgetRequest.getCreatedBy());
		budgetHeader.setDateCreated(new Date());
		budgetHeader.setDateUpdated(new Date());
		budgetHeader.setDepartmentName(budgetRequest.getCostCentreName());
		budgetHeader.setFinancialYear(budgetRequest.getFy());
		budgetHeader.setUpdatedBy(budgetRequest.getCreatedBy());
		budgetHeader = budgetHeaderRepository.save(budgetHeader);
		
		CustomValueGeneratorCommand<String> customValueGeneratorCommand = new DocumentReferenceGenerator();
		String budgetReference = customValueGeneratorCommand.generate(budgetHeader.getId(), "BUD");
		budgetHeader.setBudgetReferenceCode(budgetReference);
		budgetHeader = budgetHeaderRepository.save(budgetHeader);
		return budgetHeader;
	}
	

	private BudgetBody createBudgetBody(BudgetRequest budgetRequest,BudgetHeader budgetHeader) {
		
		BudgetBody budgetBody = new BudgetBody();
		budgetBody.setCategoryName(budgetRequest.getCategoryName());
		budgetBody.setCostCentreName(budgetRequest.getCostCentreName());
		budgetBody.setCreatedBy(budgetRequest.getCreatedBy());
		budgetBody.setDateCreated(new Date());
		budgetBody.setDateUpdated(new Date());
		budgetBody.setItem(budgetRequest.getItem());
		//logger.info("body monthly value "+budgetRequest.getBodyMonthlyValue());
		//logger.info("planned value "+budgetRequest.getPlanned());
		if(budgetRequest.getBodyMonthlyValue()==null){
			logger.info("monthly value is null seetting planned value");
			double roundPlannedValue = Double.parseDouble(df.format(budgetRequest.getPlanned()));
			logger.info(""+roundPlannedValue);
			budgetBody.setPlanned(roundPlannedValue);
		}else{
			//logger.info("setting monthly value");
			double roundPlannedValue = Double.parseDouble(df.format(budgetRequest.getBodyMonthlyValue()));
			//logger.info(""+roundPlannedValue);
			budgetBody.setPlanned(roundPlannedValue);
		}
		budgetBody.setSageCode(budgetRequest.getSageCode());
		budgetBody.setSpend(budgetRequest.getSpend());
		budgetBody.setAccrued(budgetBody.getPlanned() - budgetBody.getSpend());
		budgetBody.setAvailable(budgetBody.getPlanned() - budgetBody.getSpend());
		budgetBody.setCommited(0.0);
		budgetBody.setUpdatedBy(budgetRequest.getCreatedBy());
		budgetBody.setBudgetheaderId(budgetHeader.getId());
		budgetBody.setPaymentFrequency(budgetRequest.getPaymentFrequency());
		budgetBody.setUnitPrice(budgetRequest.getUnitPrice());
		budgetBody.setNoOfItems(budgetRequest.getNoOfItems());
		
		budgetBody = budgetBodyRepository.save(budgetBody);
		return budgetBody;
	}


	
	private void createBudgetDetailOneOffPayement(BudgetRequest budgetRequest,Long id){
		
		BudgetDetail budgetDetail = new BudgetDetail();
		budgetDetail.setBudgetBodyId(id);
		//budgetDetail.setAccrued(budgetRequest.getAccrued());
		budgetDetail.setCategoryName(budgetRequest.getCategoryName());
		budgetDetail.setCostCentreName(budgetRequest.getCostCentreName());
		budgetDetail.setMonth(budgetRequest.getMonth());
		budgetDetail.setYear(budgetRequest.getYear());
		budgetDetail.setCreatedBy(budgetRequest.getCreatedBy());
		budgetDetail.setDateCreated(new Date());
		budgetDetail.setDateUpdated(new Date());
		budgetDetail.setItem(budgetRequest.getItem());
		//Double plannedValue = (double) Math.round(budgetRequest.getPlanned());
		//logger.info("pppp " + plannedValue);
		double roundPlannedValue = Double.parseDouble(df.format(budgetRequest.getPlanned()));
		budgetDetail.setPlanned(roundPlannedValue);
		budgetDetail.setSpend(budgetRequest.getSpend());
		budgetDetail.setCommited(0.0);
		//budgetDetail.setAccrued(budgetDetail.getPlanned() - budgetDetail.getSpend());
		BigDecimal available = new BigDecimal(budgetDetail.getPlanned() - budgetDetail.getSpend()).setScale(4, RoundingMode.HALF_UP);
		budgetDetail.setAvailable(available.doubleValue());
		budgetDetail.setUpdatedBy(budgetRequest.getCreatedBy());
		
		budgetDetailRepository.save(budgetDetail);
		
	}
	
	private void createBudgetSpecifiedMonthsEqually(BudgetRequest budgetRequest,Long id){
	   
	   Double preAvailble = 0.0;
       for(int i=0; i< budgetRequest.getMonths().length;i++){
			
			BudgetDetail budgetDetail = new BudgetDetail();
			budgetDetail.setBudgetBodyId(id);
			//budgetDetail.setAccrued(budgetRequest.getAccrued());
			budgetDetail.setCategoryName(budgetRequest.getCategoryName());
			budgetDetail.setCostCentreName(budgetRequest.getCostCentreName());
			budgetDetail.setMonth(budgetRequest.getMonths()[i]);
			budgetDetail.setYear(budgetRequest.getYear());
			budgetDetail.setCreatedBy(budgetRequest.getCreatedBy());
			budgetDetail.setDateCreated(new Date());
			budgetDetail.setDateUpdated(new Date());
			budgetDetail.setItem(budgetRequest.getItem());
			//Double plannedValue = (double) Math.round(budgetRequest.getPlanned());
			//logger.info("pppp plannedValue " + plannedValue);
			double roundPlannedValue = Double.parseDouble(df.format(budgetRequest.getPlanned()));
			budgetDetail.setPlanned(roundPlannedValue);
			budgetDetail.setSpend(budgetRequest.getSpend());
			budgetDetail.setCommited(0.0);
		//	budgetDetail.setAccrued(budgetDetail.getPlanned() - budgetDetail.getSpend());
			BigDecimal available = new BigDecimal(preAvailble + (budgetDetail.getPlanned() - budgetDetail.getSpend())).setScale(4, RoundingMode.HALF_UP);
			budgetDetail.setAvailable(available.doubleValue());
			
			
			budgetDetail.setUpdatedBy(budgetRequest.getCreatedBy());
			
			//to calculate avaialble
			preAvailble = budgetDetail.getAvailable();
			
			budgetDetailRepository.save(budgetDetail);
		}
		
	}
	
	private void createBudgetSpecifiedMonthsNotEqually(BudgetRequest budgetRequest,Long id){
		
		 Double preAvailble = 0.0;
		 for(int i=0; i< budgetRequest.getMonthsSelected().length;i++){
				
				BudgetDetail budgetDetail = new BudgetDetail();
				budgetDetail.setBudgetBodyId(id);
				//budgetDetail.setAccrued(budgetRequest.getAccrued());
				budgetDetail.setCategoryName(budgetRequest.getCategoryName());
				budgetDetail.setCostCentreName(budgetRequest.getCostCentreName());
				budgetDetail.setMonth(budgetRequest.getMonthsSelected()[i]);
				budgetDetail.setYear(budgetRequest.getYear());
				budgetDetail.setCreatedBy(budgetRequest.getCreatedBy());
				budgetDetail.setDateCreated(new Date());
				budgetDetail.setDateUpdated(new Date());
				budgetDetail.setItem(budgetRequest.getItem());
				//Double plannedValue = (double) Math.round(budgetRequest.getMonthsValue()[i]);
				//logger.info("pppp plannedValue " + plannedValue);
				double roundPlannedValue = Double.parseDouble(df.format(budgetRequest.getMonthsValue()[i]));
				budgetDetail.setPlanned(roundPlannedValue);
				budgetDetail.setSpend(budgetRequest.getSpend());
				budgetDetail.setCommited(0.0);
				//budgetDetail.setAccrued(budgetDetail.getSpend()-budgetDetail.getPlanned());
				BigDecimal available = new BigDecimal(preAvailble + (budgetDetail.getPlanned() - budgetDetail.getSpend())).setScale(4, RoundingMode.HALF_UP);
				budgetDetail.setAvailable(available.doubleValue());
				budgetDetail.setUpdatedBy(budgetRequest.getCreatedBy());
				
				//to calculate avaialble
				preAvailble = budgetDetail.getAvailable();
				
				budgetDetailRepository.save(budgetDetail);
				
			}
		
	}
	
	private Double createBudgetConsecutiveMonths(BudgetRequest budgetRequest,Long id,String month,String year,Double avail){
	
		
		//for(int i=0; i< budgetRequest.getNoOfMonths();i++){
			
			BudgetDetail budgetDetail = new BudgetDetail();
			budgetDetail.setBudgetBodyId(id);
		//	budgetDetail.setAccrued(budgetRequest.getAccrued());
			budgetDetail.setCategoryName(budgetRequest.getCategoryName());
			budgetDetail.setCostCentreName(budgetRequest.getCostCentreName());
			budgetDetail.setMonth(month);
			budgetDetail.setYear(year);
			budgetDetail.setCreatedBy(budgetRequest.getCreatedBy());
			budgetDetail.setDateCreated(new Date());
			budgetDetail.setDateUpdated(new Date());
			budgetDetail.setItem(budgetRequest.getItem());
			//double plannedValue = (double)Math.round((budgetRequest.getPlanned()/ budgetRequest.getNoOfMonths()));
			//logger.info("pppp plannedValue " + plannedValue);
			double roundPlannedValue = Double.parseDouble(df.format((budgetRequest.getPlanned()/ budgetRequest.getNoOfMonths())));
			budgetDetail.setPlanned(roundPlannedValue);
			//budgetDetail.setPlanned(plannedValue);
			budgetDetail.setSpend(budgetRequest.getSpend());
			budgetDetail.setCommited(0.0);
		//	budgetDetail.setAccrued(budgetDetail.getSpend()-budgetDetail.getPlanned());
			BigDecimal available = new BigDecimal((budgetDetail.getPlanned()-budgetDetail.getSpend())+avail).setScale(4, RoundingMode.HALF_UP);
			budgetDetail.setAvailable(available.doubleValue());
			budgetDetail.setUpdatedBy(budgetRequest.getCreatedBy());
			
			budgetDetailRepository.save(budgetDetail);
			
			return budgetDetail.getAvailable();
	//	}
		
	}
	
//	private Double getYealyValueForConsecutiveMonths(Double totalValue,String startMonth,Integer numberOfMonths){
//		
//		//3 refers April
//		if(startMonth.equalsIgnoreCase("3")){
//			totalValue = totalValue / (numberOfMonths/12);
//		}else{
//			totalValue = totalValue / ((numberOfMonths/12)+1);
//		}
//		
//		return totalValue;
//	}
	
//	private Integer getTotalNumberOfMonthsForThatYear(Integer remainingTotalMonth){
//		
//	
//			if(remainingTotalMonth >= 12){
//				   return 12;
//			}else {
//				return remainingTotalMonth;
//			}
//		
//		
//	}
	
	private Double getBudgetBodyPlannedValue(double monthlyvalue,Integer totalNumberOfMonthsForThatYear){
		//logger.info("total Number Of Months For Tha tYear " + totalNumberOfMonthsForThatYear.intValue());;
		//logger.info("monthlyvalue " + monthlyvalue);;
		return totalNumberOfMonthsForThatYear.intValue() * monthlyvalue;
	}
	
	
	private int getTotalNumberOfMonthsForFirstYear(String startMonth,Integer totalmonth){
		
		if(startMonth.equalsIgnoreCase("april")){
			if(totalmonth >= 12){
				   return 12;
			}else {
				return totalmonth;
			}
		}else if(startMonth.equalsIgnoreCase("may")){
			if(totalmonth >= 11){
				   return 11;
			}else {
				return totalmonth;
			}
		}else if(startMonth.equalsIgnoreCase("june")){
			if(totalmonth >= 10){
				   return 10;
			}else {
				return totalmonth;
			}
		}else if(startMonth.equalsIgnoreCase("july")){
			if(totalmonth >= 9){
				   return 9;
			}else {
				return totalmonth;
			}
		}else if(startMonth.equalsIgnoreCase("august")){
			if(totalmonth >= 8){
				   return 8;
			}else {
				return totalmonth;
			}
		}
		else if(startMonth.equalsIgnoreCase("september")){
			if(totalmonth >= 7){
				   return 7;
			}else {
				return totalmonth;
			}
		}else if(startMonth.equalsIgnoreCase("october")){
			if(totalmonth >= 6){
				   return 6;
			}else {
				return totalmonth;
			}
		}else if(startMonth.equalsIgnoreCase("november")){
			if(totalmonth >= 5){
				   return 5;
			}else {
				return totalmonth;
			}
		}else if(startMonth.equalsIgnoreCase("december")){
			if(totalmonth >= 4){
				   return 4;
			}else {
				return totalmonth;
			}
		}else if(startMonth.equalsIgnoreCase("march")){
			if(totalmonth >= 3){
				   return 1;
			}else {
				return totalmonth;
			}
		}else if(startMonth.equalsIgnoreCase("february")){
			if(totalmonth >= 2){
				   return 2;
			}else {
				return totalmonth;
			}
		}else if(startMonth.equalsIgnoreCase("january")){
			if(totalmonth >= 1){
				   return 3;
			}else {
				return totalmonth;
			}
		}
		
		return 12;
	}

	public void updateBudgetForPO(BudgetBody budgetBody, Date poRequestDate, Double poAmount, String user) {

		BigDecimal budgetBodyCommited = new BigDecimal(budgetBody.getCommited() + poAmount).setScale(4,
				RoundingMode.HALF_UP);
		budgetBody.setCommited(budgetBodyCommited.doubleValue());
		BigDecimal budgetBodyAvail = new BigDecimal(
				budgetBody.getPlanned() - budgetBody.getCommited() - budgetBody.getSpend()).setScale(4,
						RoundingMode.HALF_UP);
		budgetBody.setAvailable(budgetBodyAvail.doubleValue());
		budgetBody.setUpdatedBy(user);
		budgetBody.setDateUpdated(new Date());

		budgetBodyRepository.save(budgetBody);

		Calendar cal = Calendar.getInstance();
		cal.setTime(poRequestDate);
		String currentMonth = new SimpleDateFormat("MMMMM").format(cal.getTime());
		BudgetDetail budgetDetail = budgetDetailRepository.findByBudgetBodyIdAndMonth(budgetBody.getId(), currentMonth);
		if (budgetDetail != null) {

			BigDecimal budgetDetailCommited = new BigDecimal(budgetDetail.getCommited() + poAmount).setScale(4,
					RoundingMode.HALF_UP);
			budgetDetail.setCommited(budgetDetailCommited.doubleValue());
			budgetDetail.setUpdatedBy(user);
			budgetDetail.setDateUpdated(new Date());
			//- budgetDetail.getSpend()
			BigDecimal budgetDetailAvail = new BigDecimal(
					budgetDetail.getAvailable() - poAmount).setScale(4,
							RoundingMode.HALF_UP);
			budgetDetail.setAvailable(budgetDetailAvail.doubleValue());
			BudgetDetail budgetDetailUpdated = budgetDetailRepository.save(budgetDetail);

			List<BudgetDetail> budgetDetails = budgetDetailRepository
					.findAllByBudgetBodyIdOrderByIdAsc(budgetBody.getId());
			Double preAvailable = budgetDetailUpdated.getAvailable();
			for (BudgetDetail currentBudgetDet : budgetDetails) {
				if (currentBudgetDet.getId() > budgetDetailUpdated.getId()) {
					BigDecimal detailAvailable = new BigDecimal(preAvailable + (currentBudgetDet.getPlanned()
							- currentBudgetDet.getCommited() - currentBudgetDet.getSpend())).setScale(4,
									RoundingMode.HALF_UP);
					currentBudgetDet.setAvailable(detailAvailable.doubleValue());
					currentBudgetDet.setUpdatedBy(user);
					currentBudgetDet.setDateUpdated(new Date());
					budgetDetailRepository.save(currentBudgetDet);
					preAvailable = detailAvailable.doubleValue();
				}
			}
		}

	}
	
	public void updateBudgetForInvoice(BudgetBody budgetBody,  Date poRequestDate, Date invoiceDate, Double invoiceAmount, String user){
		
		BigDecimal budgetBodySpent = new BigDecimal(budgetBody.getSpend() + invoiceAmount).setScale(4,
				RoundingMode.HALF_UP);
		BigDecimal budgetBodyCommited = new BigDecimal(budgetBody.getCommited() - invoiceAmount).setScale(4,
				RoundingMode.HALF_UP);
		budgetBody.setSpend(budgetBodySpent.doubleValue());
		budgetBody.setCommited(budgetBodyCommited.doubleValue());
		BigDecimal budgetBodyAvail = new BigDecimal(
				budgetBody.getPlanned() - budgetBody.getCommited() - budgetBody.getSpend()).setScale(4,
						RoundingMode.HALF_UP);
		budgetBody.setAvailable(budgetBodyAvail.doubleValue());
		budgetBody.setUpdatedBy(user);
		budgetBody.setDateUpdated(new Date());

		budgetBodyRepository.save(budgetBody);
		// First update Committed quantity
		Calendar cal = Calendar.getInstance();
		cal.setTime(poRequestDate);
		String currentMonth = new SimpleDateFormat("MMMMM").format(cal.getTime());
		BudgetDetail budgetDetailPO = budgetDetailRepository.findByBudgetBodyIdAndMonth(budgetBody.getId(), currentMonth);
		if (budgetDetailPO != null) {
			BigDecimal budgetDetailCommitted = new BigDecimal(budgetDetailPO.getCommited() - invoiceAmount).setScale(4,
					RoundingMode.HALF_UP);
			budgetDetailPO.setCommited(budgetDetailCommitted.doubleValue());
			BigDecimal budgetDetailAvail = new BigDecimal(
					budgetDetailPO.getAvailable() + invoiceAmount).setScale(4,
							RoundingMode.HALF_UP);
			budgetDetailPO.setAvailable(budgetDetailAvail.doubleValue());
			budgetDetailPO.setUpdatedBy(user);
			budgetDetailPO.setDateUpdated(new Date());
			BudgetDetail budgetDetailUpdated = budgetDetailRepository.save(budgetDetailPO);
			List<BudgetDetail> budgetDetails = budgetDetailRepository
					.findAllByBudgetBodyIdOrderByIdAsc(budgetBody.getId());
			Double preAvailable = budgetDetailUpdated.getAvailable();
			for (BudgetDetail currentBudgetDet : budgetDetails) {
				if (currentBudgetDet.getId() > budgetDetailUpdated.getId()) {
					BigDecimal detailAvailable = new BigDecimal(preAvailable + (currentBudgetDet.getPlanned()
							- currentBudgetDet.getCommited() - currentBudgetDet.getSpend())).setScale(4,
									RoundingMode.HALF_UP);
					currentBudgetDet.setAvailable(detailAvailable.doubleValue());
					currentBudgetDet.setUpdatedBy(user);
					currentBudgetDet.setDateUpdated(new Date());
					budgetDetailRepository.save(currentBudgetDet);
					preAvailable = detailAvailable.doubleValue();
				}
		}
		}
		
		
		// spend quantity update
		cal = Calendar.getInstance();
		cal.setTime(invoiceDate);
		currentMonth = new SimpleDateFormat("MMMMM").format(cal.getTime());
		BudgetDetail budgetDetailInvoice = budgetDetailRepository.findByBudgetBodyIdAndMonth(budgetBody.getId(), currentMonth);
		if (budgetDetailInvoice != null) {
			BigDecimal budgetDetailSpent = new BigDecimal(budgetDetailInvoice.getSpend() + invoiceAmount).setScale(4,
					RoundingMode.HALF_UP);
			budgetDetailInvoice.setSpend(budgetDetailSpent.doubleValue());
			BigDecimal budgetDetailAvail = new BigDecimal(
					budgetDetailInvoice.getAvailable() - invoiceAmount).setScale(4,
							RoundingMode.HALF_UP);
			budgetDetailInvoice.setAvailable(budgetDetailAvail.doubleValue());
			budgetDetailInvoice.setUpdatedBy(user);
			budgetDetailInvoice.setDateUpdated(new Date());
			BudgetDetail budgetDetailUpdatedInvoice = budgetDetailRepository.save(budgetDetailInvoice);
			List<BudgetDetail> budgetDetails = budgetDetailRepository
					.findAllByBudgetBodyIdOrderByIdAsc(budgetBody.getId());
			Double preAvailable = budgetDetailUpdatedInvoice.getAvailable();
			for (BudgetDetail currentBudgetDet : budgetDetails) {
				if (currentBudgetDet.getId() > budgetDetailUpdatedInvoice.getId()) {
					BigDecimal detailAvailable = new BigDecimal(preAvailable + (currentBudgetDet.getPlanned()
							- currentBudgetDet.getCommited() - currentBudgetDet.getSpend())).setScale(4,
									RoundingMode.HALF_UP);
					currentBudgetDet.setAvailable(detailAvailable.doubleValue());
					currentBudgetDet.setUpdatedBy(user);
					currentBudgetDet.setDateUpdated(new Date());
					preAvailable = detailAvailable.doubleValue();
				}
		}
		}
		
	}
	
//	public String updateDetail(){
//		
//		List<BudgetBody> bodies = budgetBodyRepository.findAll();
//		String arrMonth [] = {"April", "May", "June", "July", "August", "September", "October", "November", "December", "January", "February", "March"};
//		for(BudgetBody body : bodies){
//			
//			List<BudgetDetail> details = budgetDetailRepository.findAllByBudgetBodyIdOrderByIdAsc(body.getId());
//			if(details!=null){
//				if(details.size() == 12){
//					//just copy all of them as is 
//					for(BudgetDetail detail: details){
//						budgetDetailTempRepository.save(copyToTemp(detail));
//					}
//				}else{
//					BudgetDetail prevDetail = budgetDetailRepository.findFirstDetail(body.getId());
//					double prevAvailable = prevDetail.getAvailable();
//					for(int i=0; i<12; i++){
//						BudgetDetail detail = budgetDetailRepository.findByBudgetBodyIdAndMonth(body.getId(), arrMonth[i]);
//						if(detail == null){
//							budgetDetailTempRepository.save(copyToTempMonth(prevDetail, arrMonth[i], prevAvailable));
//						}else{
//							prevAvailable = detail.getAvailable();
//							budgetDetailTempRepository.save(copyToTemp(detail));
//						}
//					}
//					
//				}
//			}
//			
//		}
//		
//		return "OK";
//		
//	}
	
//	private BudgetDetailTemp copyToTemp(BudgetDetail detail){
//		BudgetDetailTemp temp = new BudgetDetailTemp();
//		temp.setAvailable(detail.getAvailable());
//		temp.setBudgetBodyId(detail.getBudgetBodyId());
//		temp.setCategoryName(detail.getCategoryName());
//		temp.setCommited(detail.getCommited());
//		temp.setCostCentreName(detail.getCostCentreName());
//		temp.setCreatedBy(detail.getCreatedBy());
//		temp.setDateCreated(detail.getDateCreated());
//		temp.setDateUpdated(detail.getDateUpdated());
//		temp.setItem(detail.getItem());
//		temp.setMonth(detail.getMonth());
//		temp.setPlanned(detail.getPlanned());
//		temp.setSpend(detail.getSpend());
//		temp.setUpdatedBy(detail.getUpdatedBy());
//		temp.setYear(detail.getYear());
//		temp.setIsNew(false);
//		return temp;
//		
//		
//	}
	
//	private BudgetDetailTemp copyToTempMonth(BudgetDetail detail, String month, double available){
//		BudgetDetailTemp temp = new BudgetDetailTemp();
//		temp.setAvailable(available);
//		temp.setBudgetBodyId(detail.getBudgetBodyId());
//		temp.setCategoryName(detail.getCategoryName());
//		temp.setCommited(0d);
//		temp.setCostCentreName(detail.getCostCentreName());
//		temp.setCreatedBy(detail.getCreatedBy());
//		temp.setDateCreated(detail.getDateCreated());
//		temp.setDateUpdated(detail.getDateUpdated());
//		temp.setItem(detail.getItem());
//		temp.setMonth(month);
//		temp.setPlanned(0d);
//		temp.setSpend(0d);
//		temp.setUpdatedBy(detail.getUpdatedBy());
//		temp.setYear(detail.getYear());
//		temp.setIsNew(true);
//		return temp;
//		
//		
//	}
//	
	

//	@Override
//	public String deleteBudget(String budgetBodyId) {
//		
//		String response = "OK";
//		budgetBodyRepository.delete(budgetBodyId);
//		budgetDetailRepository.deleteAllByBudgetBodyId(Long.parseLong(budgetBodyId));
//		return response;
//	}
//	

	public static void main(String args[]){
		
		double a = 12233.1234;
		//Double plannedValue = (double) Math.round(a);
		double roundPlannedValue = Double.parseDouble(df.format(a));
		logger.info("result " + roundPlannedValue );
		
	}


	
	
}
