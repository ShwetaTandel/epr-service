package com.vantec.epr.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.vantec.epr.entity.Capex;
import com.vantec.epr.model.ApproverRequest;
import com.vantec.epr.model.ApproversResponse;
import com.vantec.epr.model.CapexFormModel;
import com.vantec.epr.model.CapexResponse;
import com.vantec.epr.model.CreditNoteModel;
import com.vantec.epr.model.DeliveryNoteModel;
import com.vantec.epr.model.DivisionResponse;
import com.vantec.epr.model.GetOrderListRequest;
import com.vantec.epr.model.InvoiceModel;
import com.vantec.epr.model.PurchaseOrderRequest;
import com.vantec.epr.model.PurchaseOrderResponse;
import com.vantec.epr.model.QuotesRequest;
import com.vantec.epr.model.SaveDivDeptRequest;
import com.vantec.epr.model.SupplierModel;
import com.vantec.epr.model.TravelRequestModel;
import com.vantec.epr.model.UpdateStatusRequest;
import com.vantec.epr.model.UserRequest;

@Service
public interface EPRService {

	public String testService();
	public String savePurchaseOrder( PurchaseOrderRequest  purchaseOrderReq);
	public String addPurchaseOrderQuotes(QuotesRequest quotesReq);
	public PurchaseOrderResponse getPurchaseOrder(Long orderId);
	public String updatePurchaseOrderStatus(UpdateStatusRequest statusReq);
	public ApproversResponse getApprovers(Long divId, String currSttaus, Long requestorId, Float amount);
	public List<PurchaseOrderResponse> getPurchaseOrderList(GetOrderListRequest listReq);
	public List<DivisionResponse> getDivisions();
	public String saveUser( UserRequest  userReq);
	public String saveDivision(SaveDivDeptRequest request);
	public String saveDepartment(SaveDivDeptRequest request);
	public String addDeliveryNote(DeliveryNoteModel deliveryNoteReq);
	public String addInvoice(InvoiceModel invoiceReq);
	public String deleteInvoice(InvoiceModel invoiceReq);
	public String addCreditNote(CreditNoteModel creditReq);
	public String finalizeOrder(InvoiceModel invoiceReq);
	public String addApprovers(ApproverRequest req);
	public String saveSupplier(SupplierModel req);
	public String saveTravelRequest(TravelRequestModel req);
	public TravelRequestModel getTravelRequest(Long travelId);
	public String updateTravelRequestStatus(UpdateStatusRequest statusReq);
	public String updateExpenseCodes();
	public String updateCapexForm(CapexFormModel capexFormModel);
	public List<CapexResponse> getCapexRequests(long userId);
	public List<CapexResponse> getAllCapexRequests(String status);
	
}
