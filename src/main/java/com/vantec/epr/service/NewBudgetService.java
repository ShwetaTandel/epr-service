package com.vantec.epr.service;

import org.springframework.stereotype.Service;

import com.vantec.epr.model.BudgetRequest;

@Service
public interface NewBudgetService {
	
	public String saveBudgets(BudgetRequest[] budgetRequest);
	public String deleteBudgets(Long[] budgetRequestList);
}
