package com.vantec.epr.service;

import java.util.Date;

import org.springframework.stereotype.Service;

import com.vantec.epr.entity.BudgetBody;
import com.vantec.epr.model.BudgetRequest;

@Service
public interface BudgetService {

	public String saveBudget(BudgetRequest[] budgetRequest);
	public void updateBudgetForPO(BudgetBody budgetBody, Date poRequestDate, Double poAmount, String user);
	public void updateBudgetForInvoice(BudgetBody budgetBody, Date poRequestDate,  Date invoiceDate,Double poAmount, String user);
	
//	public String updateDetail();
	
	
}
