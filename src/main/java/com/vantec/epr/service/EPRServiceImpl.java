package com.vantec.epr.service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vantec.epr.dao.ApproversDAO;
import com.vantec.epr.entity.Approvers;
import com.vantec.epr.entity.BudgetBody;
import com.vantec.epr.entity.BudgetHeader;
import com.vantec.epr.entity.Capex;
import com.vantec.epr.entity.CreditNote;
import com.vantec.epr.entity.DeliveryNote;
import com.vantec.epr.entity.DeliveryNoteDetails;
import com.vantec.epr.entity.Department;
import com.vantec.epr.entity.DeptExpense;
import com.vantec.epr.entity.Division;
import com.vantec.epr.entity.Invoice;
import com.vantec.epr.entity.InvoiceDetails;
import com.vantec.epr.entity.PurchaseOrder;
import com.vantec.epr.entity.PurchaseOrderAdminDetails;
import com.vantec.epr.entity.PurchaseOrderHistory;
import com.vantec.epr.entity.PurchaseOrderItems;
import com.vantec.epr.entity.PurchaseOrderQuotes;
import com.vantec.epr.entity.Supplier;
import com.vantec.epr.entity.TravelAccommodationDetails;
import com.vantec.epr.entity.TravelQuotes;
import com.vantec.epr.entity.TravelRequest;
import com.vantec.epr.entity.TravelRequestDetails;
import com.vantec.epr.entity.TravelRequestHistory;
import com.vantec.epr.entity.UserDivisionDepartment;
import com.vantec.epr.entity.Users;
import com.vantec.epr.model.AdminDetails;
import com.vantec.epr.model.ApproverItem;
import com.vantec.epr.model.ApproverRequest;
import com.vantec.epr.model.ApproversResponse;
import com.vantec.epr.model.CapexFormModel;
import com.vantec.epr.model.CapexResponse;
import com.vantec.epr.model.CreditNoteModel;
import com.vantec.epr.model.DeliveryNoteModel;
import com.vantec.epr.model.DepartmentToCharge;
import com.vantec.epr.model.DivisionResponse;
import com.vantec.epr.model.GetOrderListRequest;
import com.vantec.epr.model.InvoiceModel;
import com.vantec.epr.model.Item;
import com.vantec.epr.model.PurchaseOrderRequest;
import com.vantec.epr.model.PurchaseOrderResponse;
import com.vantec.epr.model.Quote;
import com.vantec.epr.model.QuotesRequest;
import com.vantec.epr.model.SaveDivDeptRequest;
import com.vantec.epr.model.SupplierModel;
import com.vantec.epr.model.TravelAccommodationModel;
import com.vantec.epr.model.TravelModeModel;
import com.vantec.epr.model.TravelRequestModel;
import com.vantec.epr.model.UpdateStatusRequest;
import com.vantec.epr.model.User;
import com.vantec.epr.model.UserRequest;
import com.vantec.epr.repository.ApproversRepository;
import com.vantec.epr.repository.BudgetBodyRepository;
import com.vantec.epr.repository.BudgetHeaderRepository;
import com.vantec.epr.repository.CapexRepository;
import com.vantec.epr.repository.DepartmentRepository;
import com.vantec.epr.repository.DeptExpenseRepository;
import com.vantec.epr.repository.DivisionRepository;
import com.vantec.epr.repository.EquipmentsReqRepository;
import com.vantec.epr.repository.PurchaseOrderAdminDetailsRepository;
import com.vantec.epr.repository.PurchaseOrderCreditNoteRepository;
import com.vantec.epr.repository.PurchaseOrderDeliveryNotesDetailsRepository;
import com.vantec.epr.repository.PurchaseOrderDeliveryNotesRepository;
import com.vantec.epr.repository.PurchaseOrderDeptToChargeRepository;
import com.vantec.epr.repository.PurchaseOrderHisrtoryRepository;
import com.vantec.epr.repository.PurchaseOrderInvoiceDetailsRepository;
import com.vantec.epr.repository.PurchaseOrderInvoiceRepository;
import com.vantec.epr.repository.PurchaseOrderItemRepository;
import com.vantec.epr.repository.PurchaseOrderQuotesRepository;
import com.vantec.epr.repository.PurchaseOrderRepository;
import com.vantec.epr.repository.SupplierRepository;
import com.vantec.epr.repository.TravelQuotesRepository;
import com.vantec.epr.repository.TravelRequestAccommodationRepository;
import com.vantec.epr.repository.TravelRequestDetailsRepository;
import com.vantec.epr.repository.TravelRequestHistoryRepository;
import com.vantec.epr.repository.TravelRequestRepository;
import com.vantec.epr.repository.UserDivDeptRepository;
import com.vantec.epr.repository.UsersRepository;
import com.vantec.epr.util.EPRServiceHelper;
import com.vantec.epr.util.ExpenseCodeExcelFileReader;
import com.vantec.epr.util.PurchaseOrderActionEnum;
import com.vantec.epr.util.PurchaseOrderStatusEnum;
import com.vantec.epr.util.Stage2ApproverLevels;

@Service("EPRService")
public class EPRServiceImpl implements EPRService {
	private static Logger logger = LoggerFactory.getLogger(EPRServiceImpl.class);
	private EPRServiceHelper helper = new EPRServiceHelper();

	@Autowired
	PurchaseOrderRepository purchaseOrderRepo;
	@Autowired
	private PurchaseOrderQuotesRepository poQuotesRepo;
	@Autowired
	private TravelQuotesRepository travelQuotesRepo;

	@Autowired
	private PurchaseOrderItemRepository poItemsRepo;

	@Autowired
	private PurchaseOrderHisrtoryRepository poHistRepo;
	@Autowired
	private ApproversRepository approversRepo;
	@Autowired
	private PurchaseOrderDeptToChargeRepository poDeptToChargeRepo;

	@Autowired
	private PurchaseOrderAdminDetailsRepository poAdminDetailsRepo;

	@Autowired
	private PurchaseOrderDeliveryNotesRepository poDeliveryNotesRepo;
	@Autowired
	private PurchaseOrderInvoiceRepository poInvoiceRepo;
	@Autowired
	private PurchaseOrderCreditNoteRepository poCreditNoteRepo;

	@Autowired
	private PurchaseOrderInvoiceDetailsRepository poInvoiceDetailsRepo;

	@Autowired
	private PurchaseOrderDeliveryNotesDetailsRepository poDeliveryNotesDetailsRepo;

	@Autowired
	SupplierRepository suppRepo;

	@Autowired
	private DepartmentRepository deptRepo;
	@Autowired
	private DivisionRepository divRepo;
	@Autowired
	private UsersRepository userRepo;
	@Autowired
	private UserDivDeptRepository userDivDeptRepo;
	
	@Autowired
	private ApproversDAO approverDao;
	
	@Autowired
	private TravelRequestRepository travelReqRepo;
	@Autowired
	private TravelRequestDetailsRepository travelDetailsRepo;
	@Autowired
	private TravelRequestAccommodationRepository travelHotelRepo;
	@Autowired
	private TravelRequestHistoryRepository travelHistRepo;
	
	@Autowired
	private DeptExpenseRepository deptExpenseRepository;
	
	@Autowired
	private BudgetBodyRepository budgetBodyRepository;
	
	@Autowired
	private BudgetHeaderRepository budgetHeaderRepository;
	
	@Autowired
	private CapexRepository capexRepository;
	
	@Autowired
	private EquipmentsReqRepository equipmentReqRepository;
	
	
	@Autowired
	private PurchaseOrderItemRepository pOItemRepository;
	
	
	@Autowired
	private BudgetService budgetService;
	
	public String testService() {
		return "You have reached the service";
	}

	/**
	 * Create a new purchase Order
	 */
	@Transactional
	public String savePurchaseOrder(PurchaseOrderRequest purchaseOrderReq) { 
		PurchaseOrder purchaseOrder = new PurchaseOrder();
		boolean isEdit = purchaseOrderReq.getAction().equals(PurchaseOrderActionEnum.EDIT.name());
		boolean isApprove = purchaseOrderReq.getAction().equals(PurchaseOrderActionEnum.APPROVE.name());
		boolean isApproveFinal = purchaseOrderReq.getAction().equals(PurchaseOrderActionEnum.APPROVEFINAL.name());
		boolean isAdminEdit = purchaseOrderReq.getAction().equals(PurchaseOrderActionEnum.ADMINEDIT.name());
		boolean isNew = purchaseOrderReq.getAction().equals(PurchaseOrderActionEnum.NEW.name());
		boolean isChangeOrder =  purchaseOrderReq.getAction().equals(PurchaseOrderActionEnum.CHANGEORDER.name());
		boolean isL1Approve = purchaseOrderReq.getAction().equals(PurchaseOrderActionEnum.L1APPROVE.name());
		boolean amountChanged = false;
		if (purchaseOrderReq.getPurchaseOrderId() != null) {
			purchaseOrder = purchaseOrderRepo.findOne(purchaseOrderReq.getPurchaseOrderId());
			// in case of edit .. delete all the items first
			poItemsRepo.delete(purchaseOrder.getItems());
			purchaseOrder.getItems().clear();
			if (isApprove || isAdminEdit || isApproveFinal || isChangeOrder ) {
				poDeptToChargeRepo.delete(purchaseOrder.getDeptsToCharge());
				purchaseOrder.getDeptsToCharge().clear();
			}

		}
		if (purchaseOrder.getGrand_total()!=null && purchaseOrder.getGrand_total()!= purchaseOrderReq.getGrand_total().doubleValue()) {
			amountChanged = true;
		}
		helper.createPurchaseOrderObjectFromRequest(purchaseOrderReq, purchaseOrder);
		
		String orderHistoryComments = "";
		// Set user details and the status
		Users user = userRepo.findById(purchaseOrderReq.getRequestorId());
		Long divId = deptRepo.findById(purchaseOrderReq.getRequestorDeptId()).getDivisionId();

		if (isAdminEdit) {
			// THIS P0 FLOW /*DONT change status */
			logger.info("I am in admin edit");
			purchaseOrder.setCurrApproverId(purchaseOrderReq.getCurrApproverId());
			purchaseOrder.setStatus(purchaseOrderReq.getNextLevel() + PurchaseOrderStatusEnum._PENDING.name());
			orderHistoryComments = "Purchase Order edited by Purchasing Deptartment";

		} else if (isApprove) {
			/* APPROVAL FLOW - FOR ADMIN */
			if (purchaseOrderReq.getAdminDetails() != null) {
				String currStatus = purchaseOrder.getStatus();
				purchaseOrder.setApprovalDate(purchaseOrderReq.getApprovalDate());
				purchaseOrder.setCurrApproverId(purchaseOrderReq.getCurrApproverId());
				purchaseOrder.setStatus(currStatus.replace(PurchaseOrderStatusEnum._PENDING.name(),
						PurchaseOrderStatusEnum._APPROVED.name()));
				orderHistoryComments = "Purchase Order approved by Purchasing Department";
			}
		} else if (isApproveFinal) {
			/* APPROVAL FLOW - FOR ADMIN - FINAL APPROVAL LEVEL */

			purchaseOrder.setStatus(PurchaseOrderStatusEnum._COMPLETED.name());
			purchaseOrder.setPurchaseOrderNumber(purchaseOrder.getPurchaseOrderId() + 499);
			purchaseOrder.setApprovalDate(purchaseOrderReq.getApprovalDate());
			purchaseOrder.setCurrApproverId(purchaseOrderReq.getRequestorId());
			orderHistoryComments = "The purchase order is completed";
		} else if(isChangeOrder){
			// THIS ORDER CHAGE FLOW
			logger.info("I am in order edit");
			
			orderHistoryComments = "Completed Purchase Order edited by Purchasing Deptartment:"+purchaseOrderReq.getAdminDetails().getComments();
			
		}else {
		
			logger.info("Save po - flow Edit is " + isEdit + " New is "+isNew);
			/* NEW FLOW and EDIT - NORMAL AND EMERGENCY */
			purchaseOrder.setCurrApproverId(purchaseOrderReq.getCurrApproverId());
			purchaseOrder.setIsFinalized(false);
			if (isNew) {
				purchaseOrder.setCreateAt(new Date());
				purchaseOrder.setCreatedBy(user.getUserName());
				purchaseOrder.setRequestorId(user.getId());
				if (purchaseOrder.getIsEmergency() != null && purchaseOrder.getIsEmergency()) {
					orderHistoryComments = "New Emergency Purchase order created";
				} else {
					orderHistoryComments = "New Purchase request created";
				}
			} else if (isEdit) {
				orderHistoryComments = "Purchase request edited";
			}
			if (purchaseOrderReq.getIsFreeOfCharge() != null && purchaseOrderReq.getIsFreeOfCharge()) {
				// EMERGENCY AND FOP
				logger.info("save po - new flow - FOP ");
				purchaseOrder.setStatus(PurchaseOrderStatusEnum._COMPLETED.name());
				purchaseOrder.setIsAccrued(false);
				purchaseOrder.setApprovalDate(new Date());
			} else {
				logger.info("save po - new flow - NOT FOP");
				purchaseOrder.setStatus(purchaseOrderReq.getNextLevel() + PurchaseOrderStatusEnum._PENDING.name());
			}

		}
		purchaseOrder.setUpdatedAt(new Date());
		purchaseOrder.setLastUpdatedBy(user.getUserName());
		PurchaseOrder savedPO = purchaseOrderRepo.save(purchaseOrder);
		if (purchaseOrderReq.getIsEmergency() != null && purchaseOrderReq.getIsEmergency()) {
			savedPO.setPurchaseOrderNumber(savedPO.getPurchaseOrderId() + 499);
		}

		// Populate items
		for (Item item : purchaseOrderReq.getItems()) {
//			PurchaseOrderItems newItem;
//			if(Long.valueOf(item.getItemId()) != null ) {
//				 newItem = poItemsRepo.findById(item.getItemId());	
//			}
//			else {
//				 newItem = new PurchaseOrderItems();
//			}
			
			
			Capex capex = null;
			CapexFormModel capexDetails = item.getCapexDetails();
			if(capexDetails.getStatus() != null ) {
				if(capexDetails.getStatus().equals("CA1_PENDING")) {
					capex = saveCapexForm(capexDetails);
				}
				else {
					capex = saveCapexForm(capexDetails);
				}
				
			}
//			if( Long.valueOf(item.getCapexId()) == 0 || Long.valueOf(item.getCapexId()) == null) {
//				capex =  saveCapexForm(item.getCapexDetails());
//			}
//			else {
//				capex = capexRepository.getOne(item.getCapexId());
//			}
			
			PurchaseOrderItems newItem = new PurchaseOrderItems();
			
			if(isNew || isEdit ) {
				newItem.setCreateAt(new Date());
				newItem.setCreatedBy(user.getFirstName() + " " + user.getLastName() );
			}else {
				newItem.setUpdatedAt(new Date());
				newItem.setLastUpdatedBy(user.getFirstName() + " " + user.getLastName() );
			}
			
			newItem.setItemName(item.getItem());
			newItem.setQty(item.getQuantity());
			newItem.setUnitPrice(item.getPrice());
			newItem.setDeptToCharge(item.getDeptToCharge());
			newItem.setExpenseCode(item.getExpenseCode());
			newItem.setPurchaseOrder(savedPO);
			newItem.setIsAsset(item.getIsAsset());
			
			if(capex != null) {
				newItem.setCapex_id(capex.getId());;
			}
			
			savedPO.getItems().add(newItem);
			//poItemsRepo.save(newItem);
			PurchaseOrderItems savedItem = poItemsRepo.save(newItem);
			
//			CapexFormModel capexDetails = item.getCapexDetails();
//			if(capexDetails.getStatus() != null) {
//				capexDetails.setItemId(savedItem.getId());
//				saveCapexForm(capexDetails);
//			}
			
			//In case of changing order we need to update Invoice Details Item numbers with new ones
			if(isChangeOrder){
				
				List<Invoice> invoiceList = poInvoiceRepo.findByPurchaseOrder(savedPO);
				if(invoiceList!=null && !invoiceList.isEmpty()){
					for(Invoice inv : invoiceList){
						List<InvoiceDetails> invoiceDetailsList = poInvoiceDetailsRepo.findByInvoiceAndItemName(inv, newItem.getItemName());
						if(invoiceDetailsList!=null && !invoiceDetailsList.isEmpty()){
							for(InvoiceDetails det: invoiceDetailsList){
								det.setItemId(newItem.getId());
							}
						}
					}
				}
			}
			
		}

		if (isApprove || isAdminEdit || isApproveFinal || isChangeOrder ) {
			// This is purchasing dept bit

			AdminDetails reqAdminDetails = purchaseOrderReq.getAdminDetails();
			PurchaseOrderAdminDetails poAdminDetails = new PurchaseOrderAdminDetails();
			if (reqAdminDetails != null) {

				if (reqAdminDetails.getAdminDetailId() != 0 ) {
					poAdminDetails.setId(reqAdminDetails.getAdminDetailId());
				}else if(savedPO.getAdminDetails()!=null){
					poAdminDetails.setId(savedPO.getAdminDetails().getId());
				}
				poAdminDetails.setHandledBy(reqAdminDetails.getHandledBy());
				if (reqAdminDetails.getAlreadyPurchased().equals("Yes")) {
					poAdminDetails.setIsAlreadyPurchased(true);
				} else if (reqAdminDetails.getAlreadyPurchased().equals("No")) {
					poAdminDetails.setIsAlreadyPurchased(false);
				} else if (reqAdminDetails.getAlreadyPurchased().equals("Emergency")) {
					savedPO.setIsEmergency(true);
				}
				savedPO.setIsAccrued(purchaseOrderReq.getIsAccrued());
				if(isChangeOrder){
					poAdminDetails.setOrderPlacedDate(poAdminDetailsRepo.findByPurchaseOrder(savedPO).getOrderPlacedDate());
				}
				
				
				poAdminDetails.setSupplierNameCode(reqAdminDetails.getSupplierNameAndCode());// .replace("-AND-",
																								// "&"));
				poAdminDetails.setPaymentTerms(reqAdminDetails.getPaymentTerms());
				poAdminDetails.setPaymentType(reqAdminDetails.getPaymentType());
				poAdminDetails.setIsProcessFollowed(reqAdminDetails.getIsProcessFollowed());
				poAdminDetails.setExpenseType(reqAdminDetails.getExpenseTypeAndCode());// .replace("-AND-",
																						// "&"));
				poAdminDetails.setSavings(reqAdminDetails.getSavings());
				poAdminDetails.setIsCompanyAsset(reqAdminDetails.getIsCompanyAsset());
				poAdminDetails.setCapexComplete(reqAdminDetails.getCapexCompelete());
				poAdminDetails.setFinanceNotes(reqAdminDetails.getFinanceNotes());
				poAdminDetails.setPurchasingComments(reqAdminDetails.getPurchasingComments());
				poAdminDetails.setPurchaseOrder(savedPO);
				savedPO.setAdminDetails(poAdminDetails);
				poAdminDetailsRepo.save(poAdminDetails);

			}
		}
			
			if (isApprove || isAdminEdit || isApproveFinal || isChangeOrder || isL1Approve) {
			
			if (purchaseOrder.getIsBudgeted().equalsIgnoreCase("Yes")) {
				boolean needUpdates = false;
				// map budget
				if ((purchaseOrder.getBudgetBodyId() != null
						&& purchaseOrderReq.getBudgetBodyId().doubleValue() != purchaseOrder.getBudgetBodyId().doubleValue()&& !isEdit)|| amountChanged) {
					BudgetBody oldBudgetBody = budgetBodyRepository.findById(purchaseOrder.getBudgetBodyId());
					// EDIT Flow
					needUpdates = true;
					budgetService.updateBudgetForPO(oldBudgetBody, purchaseOrder.getRequestedDate(), purchaseOrder.getGrand_total().doubleValue(), purchaseOrder.getLastUpdatedBy());
					logger.info("Body id changed in edit flow amountchanged ="+amountChanged);
	
				}
				if (isNew || needUpdates || isEdit || isL1Approve) {
					BudgetBody budgetBody = budgetBodyRepository.findById(purchaseOrderReq.getBudgetBodyId());
					if (budgetBody != null) {
						purchaseOrder.setBudgetBodyId(purchaseOrderReq.getBudgetBodyId());
						// Set if EPR is over budget
						if (budgetBody.getAvailable().doubleValue() < purchaseOrder.getGrand_total().doubleValue()) {
							purchaseOrder.setIsOverBudget(true);
						} else {
							purchaseOrder.setIsOverBudget(false);
						}
						budgetService.updateBudgetForPO(budgetBody, purchaseOrder.getRequestedDate(),
								purchaseOrder.getGrand_total().doubleValue(), purchaseOrder.getLastUpdatedBy());
						
						
					}
				}
			}
			

		}
		/* Update the order history */
		PurchaseOrderHistory newHist = new PurchaseOrderHistory();
		newHist.setPurchaseOrderId(savedPO.getPurchaseOrderId());
		newHist.setLevel((helper.getLevel(purchaseOrder.getStatus())));
		newHist.setStatus(helper.getStatus(purchaseOrder.getStatus()));
		newHist.setComments(orderHistoryComments);
		newHist.setLastUpdatedBy(user.getUserName());
		newHist.setUpdatedAt(new Date());
		newHist.setUpdatedByUserId(user.getId());
		poHistRepo.save(newHist);

		if (isApprove || isL1Approve) {
			savedPO.setStatus(purchaseOrderReq.getNextLevel() + PurchaseOrderStatusEnum._PENDING.name());
			savedPO.setLastUpdatedBy(user.getUserName());
			savedPO.setUpdatedAt(new Date());
			purchaseOrderRepo.save(savedPO);
			newHist = new PurchaseOrderHistory();
			newHist.setPurchaseOrderId(savedPO.getPurchaseOrderId());
			newHist.setLevel((helper.getLevel(savedPO.getStatus())));
			newHist.setStatus(helper.getStatus(savedPO.getStatus()));
			newHist.setLastUpdatedBy(user.getUserName());
			newHist.setUpdatedAt(new Date());
			newHist.setComments("Moved to Next Level Pending");
			newHist.setUpdatedByUserId(user.getId());
			poHistRepo.save(newHist);
		}
		
//		if (purchaseOrder.getIsBudgeted().equalsIgnoreCase("Yes")) {
//			boolean needUpdates = false;
//			// map budget
//			if ((purchaseOrder.getBudgetBodyId() != null
//					&& purchaseOrderReq.getBudgetBodyId().doubleValue() != purchaseOrder.getBudgetBodyId().doubleValue()&& !isEdit)|| amountChanged) {
//				BudgetBody oldBudgetBody = budgetBodyRepository.findById(purchaseOrder.getBudgetBodyId());
//				// EDIT Flow
//				needUpdates = true;
//				budgetService.updateBudgetForPO(oldBudgetBody, purchaseOrder.getRequestedDate(), purchaseOrder.getGrand_total().doubleValue(), purchaseOrder.getLastUpdatedBy());
//				logger.info("Body id changed in edit flow amountchanged ="+amountChanged);
//
//			}
//			if (isNew || needUpdates || isEdit) {
//				BudgetBody budgetBody = budgetBodyRepository.findById(purchaseOrderReq.getBudgetBodyId());
//				if (budgetBody != null) {
//					purchaseOrder.setBudgetBodyId(purchaseOrderReq.getBudgetBodyId());
//					// Set if EPR is over budget
//					if (budgetBody.getAvailable().doubleValue() < purchaseOrder.getGrand_total().doubleValue()) {
//						purchaseOrder.setIsOverBudget(true);
//					} else {
//						purchaseOrder.setIsOverBudget(false);
//					}
//					budgetService.updateBudgetForPO(budgetBody, purchaseOrder.getRequestedDate(),
//							purchaseOrder.getGrand_total().doubleValue(), purchaseOrder.getLastUpdatedBy());
//					
//					
//				}
//			}
//		}
		
		
		logger.info("OK-" + savedPO.getPurchaseOrderId() + "-" + savedPO.getStatus());
		return "OK-" + savedPO.getPurchaseOrderId() + "-" + savedPO.getStatus();
	}

	/**
	 * Add quote links and files for purchase order
	 */
	@Transactional
	public String addPurchaseOrderQuotes(QuotesRequest quotesReq) {

		List<String> quoteNames = new ArrayList<String>();
		for (Quote quote : quotesReq.getQuotes()) {

			if (!quote.getPath().equals("N/A") && quote.getState().equals("update")) {
				quoteNames.add(quote.getQuote());
			}
		}
		if (!quoteNames.isEmpty()) {
			poQuotesRepo.deleteByQuoteNamesAndOrderId(quotesReq.getOrderId(), quoteNames);
		} else {
			poQuotesRepo.deleteByPurchaseOrderId(quotesReq.getOrderId());
		}
		for (Quote quote : quotesReq.getQuotes()) {
			if (quote.getState().equals("new")) {
				PurchaseOrderQuotes newQuote = new PurchaseOrderQuotes();
				newQuote.setPath(quote.getPath());
				String st = quote.getQuote();// .replace("-AND-", "&");
				int ind = st.indexOf("//");
				newQuote.setQuote(ind == -1 ? st : st.substring(ind + 2));
				newQuote.setPurchaseOrder(quotesReq.getOrderId());
				poQuotesRepo.save(newQuote);
			}
		}
		return "OK";
	}

	/**
	 * Get Purchase Order based on ID
	 */
	public PurchaseOrderResponse getPurchaseOrder(Long orderId) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setLenient(false);
		PurchaseOrderResponse resp = new PurchaseOrderResponse();
		PurchaseOrder po = purchaseOrderRepo.findOne(orderId);
		Map<Long, Integer> outstandingDelivery = new HashMap<Long, Integer>();
		Map<Long, Integer> outstandingInvoiceQty = new HashMap<Long, Integer>();
		// Section I
		resp.setPurchaseOrderId(orderId);
		resp.setPurchaseOrderNumber(po.getPurchaseOrderNumber());
		resp.setTitle(po.getTitle());

		resp.setReasonDescription(po.getReasonDescription());
		resp.setProjectName(po.getProjectName());
		resp.setIsBudgeted(po.getIsBudgeted());
		resp.setIsRecharge(po.getIsRecharge());
		resp.setRechargeTo(po.getRechargeTo());
		resp.setRechargeRef(po.getRechargeRef());
		resp.setRechargeOthers(po.getRechargeOthers());
		resp.setIsGDPRFlagged(po.getIsGDPRFlagged());
		resp.setIsFinalized(po.getIsFinalized());
		if (po.getDeliveryDate() != null) {
			resp.setDeliveryDate(sdf.format(po.getDeliveryDate()));
		}
		if (po.getRequestedDate() != null) {
			resp.setRequestedDate(sdf.format(po.getRequestedDate()));
		}
		resp.setPurchaseType(po.getPurchaseType());
		resp.setReasonOfChoice(po.getReasonOfChoice());
		resp.setReasonType(po.getReasonType());
		Department dept = deptRepo.findById(po.getDeptToChargeId());
		resp.setDeptToChargeId(po.getDeptToChargeId());
		if (dept != null) {
			resp.setDeptToChargeName(dept.getDeptName());
			resp.setDeptToChargeDivId(dept.getDivisionId());

		}
		dept = deptRepo.findById(po.getRequestorDeptId());
		if (dept != null) {
			resp.setRequestorDeptName(dept.getDeptName());
		}
		resp.setRequestorDeptId(po.getRequestorDeptId());
		resp.setRequestorDivId(deptRepo.findById(po.getRequestorDeptId()).getDivisionId());

		// Section II
		resp.setPreferredSupplier(po.getPreferred_supplier());
		resp.setCurrApproverId(po.getCurrApproverId());
		Users approver = userRepo.findById(po.getCurrApproverId());

		resp.setCurrApproverName(approver.getFirstName() + " " + approver.getLastName());
		resp.setSupplierContactDetails(po.getSupplierContactDetails());
		resp.setDoaApprovalRequired(po.getDoaApprovalRequired());
		resp.setNumberOfQuotes(po.getNumberOfQuotes());
		if(po.getBudgetBodyId()!=null){
			BudgetBody body = budgetBodyRepository.findById(po.getBudgetBodyId());
			if(body != null) {
				resp.setBudgetBodyId(body.getId());
				resp.setBudgetBodyItem(body.getItem());
				resp.setOverBudget(po.getIsOverBudget());
				
				BudgetHeader header = budgetHeaderRepository.findOne(body.getBudgetheaderId());
				resp.setFinancialYear(header.getFinancialYear());
				resp.setBudgetCategory(body.getSageCode());
				resp.setBudgetCostCentre(body.getCostCentreName());
			}
		}

		// QUOTES and LINKS
		List<PurchaseOrderQuotes> quotesList = poQuotesRepo.findByPurchaseOrderId(orderId);
		for (PurchaseOrderQuotes quote : quotesList) {
			Quote newQt = new Quote();
			newQt.setQuote(quote.getQuote());
			newQt.setPath(quote.getPath());
			resp.getQuotes().add(newQt);
		}

		// ITEMS SECTION
		resp.setDiscountsAchieved(po.getDiscountsAchieved());
		if (po.getContractFromDate() != null) {
			resp.setContractFromDate(sdf.format(po.getContractFromDate()));
		}
		if (po.getContractToDate() != null) {
			resp.setContractToDate(sdf.format(po.getContractToDate()));
		}
		resp.setSpecialRequirements(po.getSpecialRequirements());
		resp.setDiscountsAchieved(po.getDiscountsAchieved());
		resp.setCarrierCharges(po.getCarrierCharges() == null ? 00.00f : po.getCarrierCharges());
		resp.setDeliveryAddress(po.getDeliveryAddress());

		resp.setDiscount(po.getDiscount() == null ? 00.00f : po.getDiscount());
		resp.setGrand_total(po.getGrand_total());
		// Items
		for (PurchaseOrderItems item : po.getItems()) {
			Item newItem = new Item();
			newItem.setItemId(item.getId());
			newItem.setItem(item.getItemName());
			newItem.setQuantity(item.getQty());
			newItem.setPrice(item.getUnitPrice());
			newItem.setDeptToCharge(item.getDeptToCharge());
			newItem.setExpenseCode(item.getExpenseCode());
			newItem.setIsAsset(item.getIsAsset());
			newItem.setCapexId(item.getCapex_id());
			//newItem.setTotal(BigDecimal.valueOf(item.getQty() * item.getUnitPrice())
				//	.setScale(2, BigDecimal.ROUND_HALF_UP).floatValue());
			newItem.setTotal(BigDecimal.valueOf(item.getQty() * item.getUnitPrice())
			.setScale(4, BigDecimal.ROUND_HALF_UP).floatValue());
			outstandingDelivery.put(item.getId(), item.getQty());
			outstandingInvoiceQty.put(item.getId(), item.getQty());
			resp.getItems().add(newItem);
		}
		// Admin details
		resp.setIsAccrued(po.getIsAccrued());
		if (po.getIsEmergency() != null) {
			resp.setIsEmergency(po.getIsEmergency());
		} else {
			resp.setIsEmergency(false);
		}

		if (po.getAdminDetails() != null) {
			PurchaseOrderAdminDetails details = po.getAdminDetails();
			resp.setIsEmailed(po.getIsEmailed());
			resp.setAdminDetailId(details.getId());
			resp.setHandledBy(details.getHandledBy());
			if (details.getIsAlreadyPurchased() == null && po.getIsEmergency()) {
				resp.setAlreadyPurchased("Emergency");
			} else {
				resp.setAlreadyPurchased(details.getIsAlreadyPurchased() ? "Yes" : "No");
			}
			if (details.getOrderPlacedDate() != null) {
				resp.setOrderPlacedDate(sdf.format(details.getOrderPlacedDate()));
			}
			resp.setSupplierNameAndCode(details.getSupplierNameCode());
			resp.setPaymentTerms(details.getPaymentTerms());
			resp.setPaymentType(details.getPaymentType());
			resp.setIsProcessFollowed(details.getIsProcessFollowed());
			resp.setExpenseTypeAndCode(details.getExpenseType());
			resp.setSavings(details.getSavings());
			resp.setIsCompanyAsset(details.getIsCompanyAsset());
			resp.setCapexCompelete(details.getCapexComplete());
			
			List<Object[]> deptToChargeList = poItemsRepo.findItemsByDeptToCharge(po);
			//for (PurchaseOrderDeptsToCharge deptToCharge : po.getDeptsToCharge()) {
			for(Object[] deptToCharge :deptToChargeList ){
				DepartmentToCharge newDeptTocharge = new DepartmentToCharge();
				//newDeptTocharge.setId(deptToCharge[0]);
				//newDeptTocharge.setDeptId(deptToCharge.getDeptId());
				newDeptTocharge.setDeptName(deptToCharge[2].toString());
				newDeptTocharge.setExpenseCode(deptToCharge[3].toString());
				newDeptTocharge.setValue(new Float(deptToCharge[0].toString()));
				resp.getDeptToChargeList().add(newDeptTocharge);
			}
			resp.setPurchasingComments(details.getPurchasingComments());
			resp.setFinanceNotes(details.getFinanceNotes());
		}
		if (po.getStatus().equals(PurchaseOrderStatusEnum._COMPLETED.name())) {
			// Check if we have delivery notes
			List<DeliveryNote> deliveryNotes = poDeliveryNotesRepo.findByPurchaseOrder(po);
			List<DeliveryNoteModel> respDeliveryNotes = new ArrayList<DeliveryNoteModel>();

			if (deliveryNotes != null) {
				for (DeliveryNote note : deliveryNotes) {
					DeliveryNoteModel newNote = new DeliveryNoteModel();
					newNote.setDeliveryDateString(sdf.format(note.getDeliveryDate()));
					newNote.setDeliveryNoteNumber(note.getDeliveryNoteNumber());
					newNote.setDeliveryNoteDocument(note.getDeliveryNoteDocument());
					newNote.setTotalAmount(note.getTotalAmount());
					newNote.setId(note.getId());
					for (DeliveryNoteDetails noteDetail : note.getDeliveredItems()) {
						Item newItem = new Item();
						if (outstandingDelivery.containsKey(noteDetail.getItemId())) {
							Integer orderedQty = outstandingDelivery.get(noteDetail.getItemId());
							outstandingDelivery.put(noteDetail.getItemId(), orderedQty - noteDetail.getDeliveredQty());
						}
						newItem.setItemId(noteDetail.getItemId());
						newItem.setItem(noteDetail.getItemName());
						newItem.setPrice(noteDetail.getDeliveredPrice());
						newItem.setQuantity(noteDetail.getDeliveredQty());
						newNote.getDeliveredItems().add(newItem);

					}
					respDeliveryNotes.add(newNote);
				}
				resp.setDeliveryNotes(respDeliveryNotes);
				resp.setOutstandingDelivery(outstandingDelivery);
				for (Integer val : outstandingDelivery.values()) {
					if (val > 0) {
						resp.setDeliveryComplete(false);
					}
				}
			}

			// Check if PO has invoices
			List<Invoice> invoices = poInvoiceRepo.findByPurchaseOrder(po);
			Float outstandingInvoiceAmount = po.getGrand_total();
			if (invoices != null) {
				
				for (Invoice tempInv : invoices) {
					resp.setHasInvoices(true);
					InvoiceModel newInv = new InvoiceModel();
					newInv.setId(tempInv.getId());
					newInv.setInvoiceAmount(BigDecimal.valueOf(tempInv.getInvoiceAmount()).setScale(4, BigDecimal.ROUND_HALF_UP).floatValue());
					newInv.setInvoiceDocument(tempInv.getInvoiceDocument());
					newInv.setShippingCost(tempInv.getShippingCost());
					newInv.setInvoiceNumber(tempInv.getInvoiceNumber());
					newInv.setInvoiceDateString(sdf.format(tempInv.getInvoiceDate()));
					for (InvoiceDetails invoiceDetail : tempInv.getInvoicedItems()) {
						Item newItem = new Item();
						if (outstandingInvoiceQty.containsKey(invoiceDetail.getItemId())) {
							Integer orderedQty = outstandingInvoiceQty.get(invoiceDetail.getItemId());
							outstandingInvoiceQty.put(invoiceDetail.getItemId(),
									orderedQty - invoiceDetail.getInvoicedQty());
						}
						PurchaseOrderItems poItem =    poItemsRepo.findOne(invoiceDetail.getItemId());
						newItem.setDeptToCharge(poItem.getDeptToCharge());
						newItem.setExpenseCode(poItem.getExpenseCode());
						newItem.setItemId(invoiceDetail.getItemId());
						newItem.setItem(invoiceDetail.getItemName());
						newItem.setPrice(invoiceDetail.getInvoicedPrice());
						newItem.setQuantity(invoiceDetail.getInvoicedQty());
						//newItem.setTotal(invoiceDetail.getInvoicedPrice() * invoiceDetail.getInvoicedQty());
						newItem.setTotal(BigDecimal.valueOf(invoiceDetail.getInvoicedQty()* invoiceDetail.getInvoicedPrice())
								.setScale(4, BigDecimal.ROUND_HALF_UP).floatValue());

						newInv.getInvoicedItems().add(newItem);
						
						

					}
					List<Object[]> deptToChargeList = poInvoiceDetailsRepo.findDeptChargedInInvoice(tempInv, po);
					//for (PurchaseOrderDeptsToCharge deptToCharge : po.getDeptsToCharge()) {
					for(Object[] deptToCharge :deptToChargeList ){
						DepartmentToCharge newDeptTocharge = new DepartmentToCharge();
						//newDeptTocharge.setId(deptToCharge[0]);
						//newDeptTocharge.setDeptId(deptToCharge.getDeptId());
						newDeptTocharge.setDeptName(deptToCharge[1].toString());
						newDeptTocharge.setExpenseCode(deptToCharge[2].toString());
						newDeptTocharge.setValue(new Float(deptToCharge[0].toString()));
						newInv.getDeptToChargeList().add(newDeptTocharge);
					}
					resp.getInvoices().add(newInv);
					outstandingInvoiceAmount = outstandingInvoiceAmount - newInv.getInvoiceAmount();
				}
				if (!po.getIsFinalized()) {
					resp.setOutstandingInvoiceQty(outstandingInvoiceQty);
					resp.setOutstandingInvoiceAmount(BigDecimal.valueOf(outstandingInvoiceAmount).setScale(4, BigDecimal.ROUND_HALF_UP).floatValue());
				} else {
					resp.setOutstandingInvoiceAmount(0.0F);

				}
			}
			
			// Check if PO has credit Notes
		       List<CreditNote> creditNotes = poCreditNoteRepo.findByPurchaseOrder(po);
		       Float totalCreditAmt = 0F;
		       if(creditNotes!=null){
		    	   for(CreditNote creditNote : creditNotes){
		    		   CreditNoteModel newNote = new CreditNoteModel();
		    		   newNote.setId(creditNote.getId());
		    		   newNote.setCreditAmount(creditNote.getCreditAmount());
		    		   newNote.setCreditNoteDate(creditNote.getCreditNoteDate());
		    		   newNote.setCreditNoteNumber(creditNote.getCreditNoteNumber());
		    		   newNote.setCreditNoteDocument(creditNote.getCreditNoteDoc());
		    		   newNote.setInvoiceId(creditNote.getInvoice().getId());
		    		   newNote.setCreditNoteDateString(sdf.format(creditNote.getCreditNoteDate()));
		    		   totalCreditAmt += creditNote.getCreditAmount();
		    		   resp.getCreditNotes().add(newNote);
		    	   }
		    	   resp.setOutstandingCreditAmount(outstandingInvoiceAmount - totalCreditAmt);
		       }
		       

		}
		// General DATA
		resp.setCreateAt(sdf.format(po.getCreateAt()));
		resp.setCreatedBy(po.getCreatedBy());
		resp.setLastUpdatedBy(po.getLastUpdatedBy());
		resp.setUpdatedAt(sdf.format(po.getUpdatedAt()));

		// STATUS
		resp.setStatus(po.getStatus());

		// USER NAME
		Users user = userRepo.findById(po.getRequestorId());
		resp.setRequestorId(po.getRequestorId());
		resp.setRequestorUserName(user.getUserName());
		resp.setRequestorName(user.getFirstName() + " " + user.getLastName());
		resp.setCreatedBy(po.getCreatedBy());
		return resp;
	}

	/**
	 * Update purchase order in case of APPROVE---REJECT----CLOSE
	 */
	public String updatePurchaseOrderStatus(UpdateStatusRequest statusReq) {
		String status = "";
		String response = "OK";

		Users user = userRepo.findById(statusReq.getApproverId());
		PurchaseOrder orderToUpdate = purchaseOrderRepo.findOne(statusReq.getPurchaseOrderId());
		orderToUpdate.setUpdatedAt(new Date());
		orderToUpdate.setLastUpdatedBy(user.getUserName());
		boolean updateBudget = false;
		if (statusReq.getAction().equals(PurchaseOrderActionEnum.REJECT.name())) {
			// This is stage 1 reject - first go REJECT in same level
			status = statusReq.getStatus().replace(PurchaseOrderStatusEnum._PENDING.name(),
					PurchaseOrderStatusEnum._REJECTED.name());
			updateBudget = true;
		} else if (statusReq.getAction().equals(PurchaseOrderActionEnum.APPROVE.name())
				|| statusReq.getAction().equals(PurchaseOrderActionEnum.APPROVEFINAL.name())) {
			orderToUpdate.setCurrApproverId(statusReq.getNextApproverId());
			status = statusReq.getStatus() + PurchaseOrderActionEnum.APPROVE.name();
		} else if (statusReq.getAction().equals(PurchaseOrderActionEnum.CLOSE.name())) {
			updateBudget = true;
			status = PurchaseOrderStatusEnum._CLOSED.name();
		} else if (statusReq.getAction().equals(PurchaseOrderActionEnum.CHANGEAPPROVER.name())) {

			status = statusReq.getNextLevel() + PurchaseOrderStatusEnum._PENDING.name();
			orderToUpdate.setCurrApproverId(statusReq.getNextApproverId());

		}
		orderToUpdate.setStatus(status);
		PurchaseOrder savedPO = purchaseOrderRepo.save(orderToUpdate);
		PurchaseOrderHistory newHist = new PurchaseOrderHistory();
		newHist.setPurchaseOrderId(statusReq.getPurchaseOrderId());
		newHist.setStatus(helper.getStatus(savedPO.getStatus()));
		newHist.setLevel(helper.getLevel(savedPO.getStatus()));
		newHist.setLastUpdatedBy(user.getUserName());
		newHist.setUpdatedAt(new Date());
		newHist.setComments(statusReq.getComments());
		newHist.setUpdatedByUserId(statusReq.getApproverId());
		poHistRepo.save(newHist);

		// If action is approve move to next level
		// IF its ApproveFinal move to stage 2 purchasing level
		if (statusReq.getAction().equals(PurchaseOrderActionEnum.APPROVE.name())
				|| statusReq.getAction().equals(PurchaseOrderActionEnum.APPROVEFINAL.name())) {
			savedPO.setStatus(statusReq.getNextLevel() + PurchaseOrderStatusEnum._PENDING.name());
			savedPO.setLastUpdatedBy(user.getUserName());
			savedPO.setUpdatedAt(new Date());
			purchaseOrderRepo.save(savedPO);

			PurchaseOrderHistory tempHist = new PurchaseOrderHistory();
			tempHist.setPurchaseOrderId(statusReq.getPurchaseOrderId());
			tempHist.setStatus(helper.getStatus(savedPO.getStatus()));
			tempHist.setLevel(helper.getLevel(savedPO.getStatus()));
			tempHist.setLastUpdatedBy(user.getUserName());
			tempHist.setUpdatedAt(new Date());
			tempHist.setComments("Moved to Next Level Pending");
			tempHist.setUpdatedByUserId(statusReq.getApproverId());
			poHistRepo.save(tempHist);
		}
		if(updateBudget){
			BudgetBody budgetBody = budgetBodyRepository.findById(savedPO.getBudgetBodyId());
			budgetService.updateBudgetForPO(budgetBody, savedPO.getRequestedDate(), -savedPO.getGrand_total().doubleValue(), savedPO.getLastUpdatedBy());
		}

		return response;
	}

	/**
	 * 
	 * GEt list of approvers
	 */
	public ApproversResponse getApprovers(Long divId, String currStatus, Long requestorId, Float amount) {
		ApproversResponse response = new ApproversResponse();
		String[] splitArr = currStatus.split("_");
		String level = splitArr[0];
		String status = splitArr[1];
		Users requestor = userRepo.findById(requestorId);
		String reqLevel = ""; 
		if(requestor.getIsApprover() ){
			Approvers reqAppr = approversRepo.findByUserIdAndDivisionIdForLLevel(requestorId, divId);
			if(reqAppr!=null)
				reqLevel = reqAppr.getLevel();
			logger.info("User is approver and level is "+reqLevel);
		}
		List<String> levelsToFetch = new ArrayList<String>();
		if (status.equals("PURCHASING")) {
			// The requestor approver is the last approver Find the next
			// purchasing approver

			levelsToFetch.addAll(approverDao.findStage2NextLevelsForDiv(divId, level));// approverDao.findPurchasingFirstLevel(divId);
			logger.info("I am here inPURCHASING FLOW" + levelsToFetch.size());

		} else if (status.equals("PURCHASINGALL")) {
			// The requestor is P0 admin show all approvers in Purchasing
			// approvers
			levelsToFetch.addAll(approverDao.findAllStage2NextLevelsForDiv(divId, level));
			logger.info("I am here inPURCHASING ALL FLOW" + levelsToFetch.size());

		} else if (status.equals("CHANGE")) {
			// Include the current level as well
			levelsToFetch.addAll(approverDao.findStage1LevelsForDiv(divId, level));
			logger.info("I am change and and all general caess" + levelsToFetch.size());

		} else if (status.equals("CHANGEPURCHASING")) {
			// Include the current level as well
			levelsToFetch.addAll(approverDao.findStage2LevelsForDiv(divId, level));
			logger.info("I am change for purchasing" + levelsToFetch.size());

		} else {
			// All general cases - Status == NEW (Normal users)
			// nextLevel = approverDao.findDistinctNextLevel(divId, level);
			if(reqLevel!=""){
				// this is the case where requestor is approver
				levelsToFetch.addAll(approverDao.findStage1NextLevelsForDiv(divId, reqLevel));
			}
			else{
				levelsToFetch.addAll(approverDao.findStage1NextLevelsForDiv(divId, level));
			}
			logger.info("Div is "+divId + " - level is "+level);
			logger.info("I am here in NEW and and all general caess" + levelsToFetch.size());
		}
		if (levelsToFetch.isEmpty()) {
			levelsToFetch.add(Stage2ApproverLevels.P1.toString());
		}
		List<Object[]> result = approverDao.getApprovers(divId, levelsToFetch, requestorId);
		for (Object[] row : result) {
			User user = new User();
			String tmpLevel = helper.getDescriptionForLevel(row[3].toString());
			user.setFirstName(row[1].toString());
			user.setLastName(row[2].toString());
			user.setUserId(Integer.parseInt(row[0].toString()));
			response.addUser(tmpLevel, user);
		}
		return response;
	}

	/**
	 * Return list of purchase orders based on 1. Requestor id and/or status 2.
	 * Approver id and /or status
	 */
	public List<PurchaseOrderResponse> getPurchaseOrderList(GetOrderListRequest listReq) {
		List<PurchaseOrder> orderList = new ArrayList<PurchaseOrder>();
		if (listReq.getIsP0() != null && listReq.getIsP0()) {
			orderList.addAll(purchaseOrderRepo.findOrdersForP0());

		} else if (listReq.getApproverId() != null) {
			if (listReq.getStatus() != null) {
				orderList.addAll(
						purchaseOrderRepo.findByCurrApproverAndStatus(listReq.getApproverId(), listReq.getStatus()));
				logger.info("in approver status");
			} else {
				orderList.addAll(purchaseOrderRepo.findByCurrApproverIdOrderByCreateAtDesc(listReq.getApproverId()));
			}
		} else if (listReq.getRequestorId() != null) {
			if (listReq.getStatus() != null) {
				orderList.addAll(
						purchaseOrderRepo.findByRequestorAndStatus(listReq.getRequestorId(), listReq.getStatus()));
			} else {
				orderList.addAll(purchaseOrderRepo.findByRequestorIdOrderByCreateAtDesc(listReq.getRequestorId()));
			}
		} else if (listReq.getStatus() != null) {
			if (listReq.getStatus().equals("PENDING")) {
				orderList.addAll(purchaseOrderRepo.findAllInProcess());
			} else if (listReq.getStatus().equals("COMPLETED")) {
				orderList.addAll(
						purchaseOrderRepo.findByStatusOrderByUpdatedAtDesc(PurchaseOrderStatusEnum._COMPLETED.name()));
			}
		} else if (listReq.getDepartment() != null) {
			long[] dept = Arrays.stream(listReq.getDepartment().split(",")).mapToLong(Long::parseLong).toArray();
			List<Long> departments = Arrays.stream(dept).boxed().collect(Collectors.toList());
			logger.info(listReq.getDepartment());
			orderList.addAll(purchaseOrderRepo.findByDepartments(departments));

		}else if (listReq.getCostCentre() != null) {
			long[] costCentre = Arrays.stream(listReq.getCostCentre().split(",")).mapToLong(Long::parseLong).toArray();
			List<Long> costCentres = Arrays.stream(costCentre).boxed().collect(Collectors.toList());
			logger.info(listReq.getCostCentre());
			orderList.addAll(purchaseOrderRepo.findByCostCentre(costCentres));

		}
		ArrayList<PurchaseOrderResponse> response = new ArrayList<PurchaseOrderResponse>();
		for (PurchaseOrder po : orderList) {
			PurchaseOrderResponse respItem = getPurchaseOrder(po.getPurchaseOrderId());
			response.add(respItem);
		}
		return response;
	}

	/***
	 * Return all divisions with department
	 */
	public List<DivisionResponse> getDivisions() {

		List<DivisionResponse> response = new ArrayList<DivisionResponse>();
		for (com.vantec.epr.entity.Division div : divRepo.findAll()) {
			DivisionResponse newDiv = new DivisionResponse();
			newDiv.setDivId(div.getId());
			newDiv.setDivName(div.getDivName());
			for (Department dept : deptRepo.findByDivisionId(div.getId())) {
				com.vantec.epr.model.DepartmentResponse newDept = new com.vantec.epr.model.DepartmentResponse();
				newDept.setDeptId(dept.getId());
				newDept.setDeptName(dept.getDeptName());
				newDiv.getDeptList().add(newDept);
			}
			response.add(newDiv);
		}
		return response;
	}

	public String saveUser(UserRequest userReq) {
		Users newUser = new Users();
		boolean isEdit = userReq.getUserId() != null ? userReq.getUserId() != 0 : false;
		if (isEdit) {
			newUser = userRepo.findById(userReq.getUserId());
			userDivDeptRepo.delete(newUser.getUserDivDeptList());
			newUser.getUserDivDeptList().clear();
			if (newUser.getIsApprover() == true && userReq.getIsApprover() == false) {
				approversRepo.delete(approversRepo.findByUserId(newUser.getId()));
			}
		}
		newUser.setFirstName(userReq.getFirstName());
		newUser.setEmailId(userReq.getEmailId());
		newUser.setMobileNumber(userReq.getContact());
		newUser.setActive(true);
		newUser.setIsSuperUser(false);
		newUser.setIsApprover(userReq.getIsApprover());
		newUser.setLastName(userReq.getLastName());
		newUser.setPosition(userReq.getPosition());
		newUser.setIsAdmin(userReq.getIsAdmin());
		newUser.setIsP0(userReq.getIsP0());
		newUser.setIsL0(userReq.getIsL0());
		newUser.setIsF0(userReq.getIsF0());
		newUser.setCanRaiseEmergency(userReq.getCanRaiseEmergency());

		Users requestor = userRepo.findById(userReq.getRequestorId());

		if (!isEdit) {
			Long empId = userRepo.findMaxEmployeedId() + 1;
			newUser.setEmployeeId(empId);
			String first = userReq.getFirstName().replaceAll(" ", "");
			newUser.setUserName(first + userReq.getLastName().substring(0, 1).toUpperCase() + empId);
			newUser.setPassword(userReq.getFirstName() + userReq.getLastName().substring(0, 1).toUpperCase() + empId);
			newUser.setCreatedBy(requestor.getUserName());
			newUser.setCreateAt(new Date());
		}

		newUser.setLastUpdatedBy(requestor.getUserName());
		Users savedUser = userRepo.save(newUser);

		// add records in user-div-dept
		for (String id : userReq.getDeptList()) {
			Department dept = deptRepo.findById(Long.valueOf(id));
			UserDivisionDepartment newRec = new UserDivisionDepartment();
			newRec.setDeptId(dept.getId());
			newRec.setDivId(dept.getDivisionId());
			newRec.setUser(savedUser);
			newRec.setCreateAt(new Date());
			newRec.setCreatedBy(requestor.getUserName());
			newRec.setUpdatedAt(new Date());
			newRec.setLastUpdatedBy(requestor.getUserName());
			userDivDeptRepo.save(newRec);
		}
		return "OK-" + savedUser.getUserName();
	}

	/***
	 * 
	 * Create a new Division
	 * 
	 * @param request
	 * @return
	 */
	public String saveDivision(SaveDivDeptRequest request) {
		boolean isEdit = request.getDivId() != null ? request.getDivId() != 0 : false;
		Division newDiv = new Division();
		if (isEdit) {
			newDiv = divRepo.findOne(request.getDivId());
			// DELETE DEPARTMENTS ???
		}
		newDiv.setDivCode(request.getDivCode());
		newDiv.setDivName(request.getDivName());
		Users requestor = userRepo.findById(request.getRequestorId());
		if (!isEdit) {
			newDiv.setCreateAt(new Date());
			newDiv.setCreatedBy(requestor.getUserName());
		}
		newDiv.setLastUpdatedBy(requestor.getUserName());
		newDiv.setUpdatedAt(new Date());
		Division savedDiv = divRepo.save(newDiv);
		if (!isEdit) {
			for (String deptName : request.getDeptList()) {
				Department dept = new Department();
				dept.setDeptName(deptName);
				dept.setDivisionId(savedDiv.getId());
				dept.setCreateAt(new Date());
				dept.setCreatedBy(requestor.getUserName());
				dept.setUpdatedAt(new Date());
				dept.setLastUpdatedBy(requestor.getUserName());
				deptRepo.save(dept);
			}
		}
		return "OK";
	}

	/***
	 * Create / Update a department
	 * 
	 * @param request
	 * @return
	 */
	public String saveDepartment(SaveDivDeptRequest request) {
		boolean isEdit = request.getDeptId() != null ? request.getDeptId() != 0 : false;
		Users requestor = userRepo.findById(request.getRequestorId());
		Department dept = new Department();
		if (isEdit) {
			dept = deptRepo.findById(request.getDeptId());
		}
		dept.setDeptName(request.getDeptName());
		if (!isEdit) {
			dept.setCreateAt(new Date());
			dept.setCreatedBy(requestor.getUserName());
		}
		dept.setUpdatedAt(new Date());
		dept.setLastUpdatedBy(requestor.getUserName());
		deptRepo.save(dept);
		return "OK";
	}

	/***
	 * 
	 * Add a delivery note for completed order
	 * 
	 * @param deliveryNoteReq
	 */
	public String addDeliveryNote(DeliveryNoteModel deliveryNoteReq) {

		DeliveryNote newNote = new DeliveryNote();
		if (deliveryNoteReq.getId() != null) {
			newNote = poDeliveryNotesRepo.findOne(deliveryNoteReq.getId());
		}
		PurchaseOrder po = purchaseOrderRepo.findOne(deliveryNoteReq.getPurchaseOrderId());
		Users requestor = userRepo.findOne(deliveryNoteReq.getRequestorId());
		newNote.setDeliveryNoteNumber(deliveryNoteReq.getDeliveryNoteNumber());
		newNote.setDeliveryDate(deliveryNoteReq.getDeliveryDate());
		newNote.setTotalAmount(deliveryNoteReq.getTotalAmount());
		newNote.setPurchaseOrder(po);

		newNote.setCreateAt(new Date());
		newNote.setCreatedBy(requestor.getUserName());

		newNote.setUpdatedAt(new Date());

		newNote.setLastUpdatedBy(requestor.getUserName());
		DeliveryNote savedDeliveryNote = poDeliveryNotesRepo.save(newNote);

		for (Item item : deliveryNoteReq.getDeliveredItems()) {
			DeliveryNoteDetails noteDetail = new DeliveryNoteDetails();
			noteDetail.setDeliveredQty(item.getQuantity());
			noteDetail.setDeliveredPrice(item.getPrice());
			noteDetail.setItemName(item.getItem());
			noteDetail.setItemId(item.getItemId());
			noteDetail.setCreateAt(new Date());
			noteDetail.setUpdatedAt(new Date());
			noteDetail.setCreatedBy(requestor.getUserName());
			noteDetail.setLastUpdatedBy(requestor.getUserName());
			noteDetail.setDeliveryNote(savedDeliveryNote);
			poDeliveryNotesDetailsRepo.save(noteDetail);
		}
		po.setUpdatedAt(new Date());
		po.setLastUpdatedBy(requestor.getUserName());
		purchaseOrderRepo.save(po);

		PurchaseOrderHistory newHist = new PurchaseOrderHistory();
		newHist.setPurchaseOrderId(po.getPurchaseOrderId());
		newHist.setComments("New Delivery note added " + deliveryNoteReq.getDeliveryNoteNumber());
		newHist.setLastUpdatedBy(requestor.getUserName());
		newHist.setUpdatedAt(new Date());
		newHist.setUpdatedByUserId(requestor.getId());
		poHistRepo.save(newHist);

		return "OK-" + savedDeliveryNote.getId();
	}
	
	@Transactional
	public String deleteInvoice(InvoiceModel invoiceReq) {

		
		Invoice invoice = poInvoiceRepo.findById(invoiceReq.getId());
		Users user = userRepo.findById(invoiceReq.getRequestorId());
		PurchaseOrder order = purchaseOrderRepo.findOne(invoice.getPurchaseOrder().getPurchaseOrderId());
		
		PurchaseOrderHistory newHist = new PurchaseOrderHistory();
		newHist.setPurchaseOrderId(order.getPurchaseOrderId());
		newHist.setComments("Invoice deleted -" + invoice.getInvoiceNumber());
		newHist.setLastUpdatedBy(user.getUserName());
		newHist.setUpdatedAt(new Date());
		newHist.setUpdatedByUserId(user.getId());
		poHistRepo.save(newHist);
		
		order.setIsFinalized(false);
		order.setLastUpdatedBy(user.getUserName());
		order.setUpdatedAt(new Date());
		purchaseOrderRepo.save(order);
		
		List<InvoiceDetails> details = poInvoiceDetailsRepo.findByInvoice(invoice);
		poInvoiceDetailsRepo.delete(details);
		
		poInvoiceRepo.delete(invoice);
		
		//update budget for invoice amount
		
				BudgetBody budgetBody = budgetBodyRepository.findById(order.getBudgetBodyId());
				if (budgetBody != null) {
					budgetService.updateBudgetForInvoice(budgetBody, order.getRequestedDate(),invoice.getInvoiceDate(),
							-invoice.getInvoiceAmount().doubleValue(), order.getLastUpdatedBy());
				}
		
		
		return "OK-true";
	}

	/***
	 * add invoices against PO
	 */
	@Transactional
	public String addInvoice(InvoiceModel invoiceReq) {
		Invoice invoice ;
		PurchaseOrder po = purchaseOrderRepo.findOne(invoiceReq.getPurchaseOrderId());
		if (invoiceReq.getInvoiceNumber() != null) {
			invoice = poInvoiceRepo.findByPurchaseOrderAndInvoiceNumber(po,invoiceReq.getInvoiceNumber());
			if(invoice!=null){
				return "DUPLICATE";
			}
		}
		invoice = new Invoice();
		Users requestor = userRepo.findOne(invoiceReq.getRequestorId());
		invoice.setInvoiceNumber(invoiceReq.getInvoiceNumber());
		invoice.setInvoiceAmount(invoiceReq.getInvoiceAmount());
		invoice.setShippingCost(invoiceReq.getShippingCost());
		invoice.setInvoiceDate(invoiceReq.getInvoiceDate());
		invoice.setCreateAt(new Date());
		invoice.setUpdatedAt(new Date());
		invoice.setCreatedBy(requestor.getUserName());
		invoice.setLastUpdatedBy(requestor.getUserName());
		invoice.setPurchaseOrder(po);
		Invoice savedInvoice = poInvoiceRepo.save(invoice);

		for (Item item : invoiceReq.getInvoicedItems()) {
			InvoiceDetails invoiceDetail = new InvoiceDetails();
			invoiceDetail.setInvoicedQty(item.getQuantity());
			invoiceDetail.setInvoicedPrice(item.getPrice());
			invoiceDetail.setItemName(item.getItem());
			invoiceDetail.setItemId(item.getItemId());
			invoiceDetail.setCreateAt(new Date());
			invoiceDetail.setUpdatedAt(new Date());
			invoiceDetail.setCreatedBy(requestor.getUserName());
			invoiceDetail.setLastUpdatedBy(requestor.getUserName());
			invoiceDetail.setInvoice(savedInvoice);
			poInvoiceDetailsRepo.save(invoiceDetail);
		}
		if (invoiceReq.getIsFinal()) {
			po.setIsFinalized(true);
		}
		po.setUpdatedAt(new Date());
		po.setLastUpdatedBy(requestor.getUserName());
		purchaseOrderRepo.save(po);

		PurchaseOrderHistory newHist = new PurchaseOrderHistory();
		newHist.setPurchaseOrderId(po.getPurchaseOrderId());
		newHist.setComments("New Invoice" + invoiceReq.getInvoiceNumber() + " added.");
		newHist.setLastUpdatedBy(requestor.getUserName());
		newHist.setUpdatedAt(new Date());
		newHist.setUpdatedByUserId(requestor.getId());
		poHistRepo.save(newHist);
		
		//update budget for invoice amount
		
		BudgetBody budgetBody = budgetBodyRepository.findById(po.getBudgetBodyId());
		if (budgetBody != null) {
			budgetService.updateBudgetForInvoice(budgetBody, po.getRequestedDate(),invoice.getInvoiceDate(),
					invoice.getInvoiceAmount().doubleValue(), po.getLastUpdatedBy());
		}
		return "OK-" + savedInvoice.getId();
	}
	@Transactional
	public String finalizeOrder(InvoiceModel invoiceReq) {

		PurchaseOrder po = purchaseOrderRepo.findOne(invoiceReq.getPurchaseOrderId());
		Users requestor = userRepo.findOne(invoiceReq.getRequestorId());
		po.setIsFinalized(true);
		Float origAmount = po.getGrand_total();
		po.setGrand_total(origAmount - invoiceReq.getInvoiceAmount());
		po.setLastUpdatedBy(requestor.getUserName());
		po.setUpdatedAt(new Date());
		purchaseOrderRepo.save(po);
		
		

		PurchaseOrderHistory newHist = new PurchaseOrderHistory();
		newHist.setPurchaseOrderId(po.getPurchaseOrderId());
		newHist.setComments("PO Finalized. Original amount was " + origAmount);
		newHist.setLastUpdatedBy(requestor.getUserName());
		newHist.setUpdatedAt(new Date());
		newHist.setUpdatedByUserId(requestor.getId());
		poHistRepo.save(newHist);
		
		BudgetBody budgetBody = budgetBodyRepository.findById(po.getBudgetBodyId());
		if (budgetBody != null) {
			budgetService.updateBudgetForPO(budgetBody, po.getRequestedDate(),
					-invoiceReq.getInvoiceAmount().doubleValue(), po.getLastUpdatedBy());
		}
		return "OK";

	}

	/* Add approvers for department */
	public String addApprovers(ApproverRequest req) {
		for (ApproverItem item : req.getApprovers()) {
			Approvers newApprover = new Approvers();
			newApprover.setIsActive(true);
			newApprover.setUserId(item.getUserId());
			newApprover.setDivisionId(req.getDivId());
			newApprover.setLevel(item.getLevel());
			newApprover.setCreateAt(new Date());
			newApprover.setCreatedBy(req.getRequestor());
			newApprover.setLastUpdatedBy(req.getRequestor());
			newApprover.setUpdatedAt(new Date());
			newApprover.setMaxAmount(helper.getAmountForLevel(item.getLevel()));
			approversRepo.save(newApprover);
		}
		return "OK";

	}

	public String saveSupplier(SupplierModel req) {
		Supplier newSupp = new Supplier();
		if (req.getId() != 0) {
			newSupp = suppRepo.findOne(req.getId());
			if (req.getSuppName() == null || req.getSuppName().equals("")) {
				suppRepo.delete(req.getId());
				return "OK";
			}
		}

		newSupp.setPaymentDesc(req.getPaymentDesc());
		newSupp.setPaymentTerms(req.getPaymentTerms());
		newSupp.setSuppCode(req.getSuppCode());
		newSupp.setSuppName(req.getSuppName());// .replace("-AND-", "&"));
		suppRepo.save(newSupp);
		return "OK";
	}
	
	/***
	 * add credit note against PO
	 */
	public String addCreditNote(CreditNoteModel creditReq) {
		CreditNote creditNote = new CreditNote();
		if (creditReq.getId() != null) {
			creditNote = poCreditNoteRepo.findOne(creditReq.getId());
		}
		PurchaseOrder po = purchaseOrderRepo.findOne(creditReq.getPurchaseOrderId());
		Invoice invoice = poInvoiceRepo.findOne(creditReq.getInvoiceId());
		Users requestor = userRepo.findOne(creditReq.getRequestorId());
		creditNote.setCreditNoteNumber(creditReq.getCreditNoteNumber());
		creditNote.setCreditAmount(creditReq.getCreditAmount());
		creditNote.setCreditNoteDesc(creditReq.getCreditNoteDesc());
		creditNote.setCreditNoteDate(creditReq.getCreditNoteDate());
		creditNote.setCreateAt(new Date());
		creditNote.setUpdatedAt(new Date());
		creditNote.setCreatedBy(requestor.getUserName());
		creditNote.setLastUpdatedBy(requestor.getUserName());
		creditNote.setPurchaseOrder(po);
		creditNote.setInvoice(invoice);
		CreditNote savedNote = poCreditNoteRepo.save(creditNote);
		
		if(creditReq.getIsFinal()){
			po.setIsFinalized(true);
		}
		po.setUpdatedAt(new Date());
		po.setLastUpdatedBy(requestor.getUserName());
		purchaseOrderRepo.save(po);

		PurchaseOrderHistory newHist = new PurchaseOrderHistory();
		newHist.setPurchaseOrderId(po.getPurchaseOrderId());
		newHist.setComments("New Credit Note " + creditReq.getCreditNoteNumber() + " added.");
		newHist.setLastUpdatedBy(requestor.getUserName());
		newHist.setUpdatedAt(new Date());
		newHist.setUpdatedByUserId(requestor.getId());
		poHistRepo.save(newHist);
		return "OK-" + savedNote.getId();
	}
	
	/***
	 * Save a travel request in DB
	 */
	public String saveTravelRequest(TravelRequestModel request) {
		
		boolean isEdit = request.getAction().equals("EDIT");
		boolean isNew = request.getAction().equals("NEW");
		boolean isApprove = request.getAction().equals("APPROVE");
		boolean isApproveFinal = request.getAction().equals("APPROVEFINAL");
		TravelRequest newTravelRequest = new TravelRequest();
		if(isEdit){
			newTravelRequest = travelReqRepo.findOne(request.getTravelRequestId());
			travelHotelRepo.delete(newTravelRequest.getTravelHotelDetails());
			travelDetailsRepo.delete(newTravelRequest.getTravelDetails());
			newTravelRequest.getTravelHotelDetails().clear();
			newTravelRequest.getTravelDetails().clear();
			
		}
		Users user = userRepo.findOne(request.getRequestorId());
		
		newTravelRequest.setTravellerId(request.getTravellerId());
		newTravelRequest.setTravelRequestedBy(request.getTravelRequestorId());
		newTravelRequest.setDeptIdToCharge(request.getDeptToChargeId());
		newTravelRequest.setTravelReason(request.getTravelReason());
		newTravelRequest.setTravelDate(request.getTravelDate());
		newTravelRequest.setReturnDate(request.getReturnDate());
		newTravelRequest.setIsBudgeted(request.getIsBudgeted());
		newTravelRequest.setIsGDPRFlagged(request.getIsGDPRFlagged());
		newTravelRequest.setIsRecharge(request.getIsRecharge());
		if (newTravelRequest.getIsRecharge()) {
			newTravelRequest.setRechargeRef(request.getRechargeRef());
			newTravelRequest.setRechargeTo(request.getRechargeTo());
			newTravelRequest.setRechargeOthers(request.getRechargeOthers());
		}
		newTravelRequest.setComments(request.getComments());
		newTravelRequest.setCurrApproverId(request.getCurrApproverId());
		newTravelRequest.setStatus(request.getStatus());
		if(isNew){
			
			newTravelRequest.setCreatedBy(user.getUserName());
			newTravelRequest.setCreateAt(new Date());
			newTravelRequest.setLastUpdatedBy(user.getUserName());
			newTravelRequest.setUpdatedAt(new Date());

		}else{
			
			newTravelRequest.setLastUpdatedBy(user.getUserName());
			newTravelRequest.setUpdatedAt(new Date());
		}
		TravelRequest savedTravel = travelReqRepo.save(newTravelRequest);
		logger.info(savedTravel.getTravelRequestId()+"");
		for(TravelModeModel mode : request.getTravelModes()){
			TravelRequestDetails newMode = new TravelRequestDetails();
			newMode.setTravelMode(mode.getTravelMode());
			newMode.setFromLocation(mode.getFromLocation());
			newMode.setToLocation(mode.getToLocation());
			newMode.setOutboundDateTime(mode.getOutDateTime());
			newMode.setInboundDateTime(mode.getInDateTime());
			newMode.setCarParking(mode.getCarPark());
			newMode.setIsHandLuggage(mode.getHandBag());
			newMode.setIsHoldLuggage(mode.getHoldBag());
			newMode.setIsRoundTrip(mode.getRoundTrip());
			newMode.setRoundTripMileage(mode.getMileage());
			newMode.setTravelRequest(savedTravel);
			travelDetailsRepo.save(newMode);
		}
		
		for(TravelAccommodationModel hotel : request.getHotels()){
			TravelAccommodationDetails newHotel = new TravelAccommodationDetails();
			newHotel.setCheckinDate(hotel.getCheckinDate());
			newHotel.setCheckoutDate(hotel.getCheckoutDate());
			newHotel.setIsHotelBreakfast(hotel.getBreakfast());
			newHotel.setHotelEveningMeal(hotel.getEvemeal());
			newHotel.setTravelRequest(savedTravel);
			travelHotelRepo.save(newHotel);
		}
		TravelRequestHistory hist = new TravelRequestHistory();
		hist.setLevel(helper.getLevel(request.getStatus()));
		hist.setTravelRequestId(savedTravel.getTravelRequestId());
		hist.setLastUpdatedBy(user.getUserName());
		hist.setStatus(helper.getStatus(request.getStatus()));
		hist.setUpdatedByUserId(user.getId());
		hist.setUpdatedAt(new Date());
		if(isNew){
			hist.setComments("New travel request created");
		}else if(isEdit){
			hist.setComments("Travel request edited");
		}else if(isApprove){
			hist.setComments("Travel request approved at stage 1");
		}else if(isApproveFinal){
			hist.setComments("Travel request approved at stage 2");
		}
		travelHistRepo.save(hist);
		
		
		return "OK-"+savedTravel.getTravelRequestId();
	}
	
	
	/**
	 * 
	 */
	public TravelRequestModel getTravelRequest(Long travelId){
		
		TravelRequestModel response = new TravelRequestModel();
		TravelRequest dbRecord = travelReqRepo.getOne(travelId);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		response.setTravellerId(travelId);
		response.setTravelReason(dbRecord.getTravelReason());
		response.setStatus(dbRecord.getStatus());
		response.setAddInfo(dbRecord.getAdditionalInfo());
		response.setComments(dbRecord.getComments());
		response.setCurrApproverId(dbRecord.getCurrApproverId());
		Users tmpUser = userRepo.findOne(dbRecord.getCurrApproverId());
		response.setCurrApproverName(tmpUser.getFirstName() + " "+ tmpUser.getLastName());
		
		response.setTravelRequestorId(dbRecord.getTravelRequestedBy());
		tmpUser = userRepo.findOne(dbRecord.getTravelRequestedBy());
		response.setTravelRequestorName(tmpUser.getFirstName() + " "+ tmpUser.getLastName());
		
		response.setTravellerId(dbRecord.getTravellerId());
		tmpUser = userRepo.findOne(dbRecord.getTravellerId());
		response.setTravellerName(tmpUser.getFirstName() + " "+ tmpUser.getLastName());
		
		response.setTravelDate(dbRecord.getTravelDate());
		response.setReturnDate(dbRecord.getReturnDate());
		response.setTravelDateString(sdf.format(dbRecord.getTravelDate()));
		response.setReturnDateString(sdf.format(dbRecord.getReturnDate()));

		Department dept = deptRepo.findOne(dbRecord.getDeptIdToCharge());
		response.setDeptToChargeId(dbRecord.getDeptIdToCharge());
		response.setCostCentre(dept.getDeptName());
		
		response.setIsBudgeted(dbRecord.getIsBudgeted());
		response.setIsGDPRFlagged(dbRecord.getIsGDPRFlagged());
		response.setIsRecharge(dbRecord.getIsRecharge());
		response.setRechargeTo(dbRecord.getRechargeTo());
		response.setRechargeRef(dbRecord.getRechargeRef());
		response.setRechargeOthers(dbRecord.getRechargeOthers());
		
		for(TravelRequestDetails mode: dbRecord.getTravelDetails()){
			TravelModeModel newMode = new TravelModeModel();
			newMode.setTravelMode(mode.getTravelMode());
			newMode.setFromLocation(mode.getFromLocation());
			newMode.setToLocation(mode.getToLocation());
			newMode.setOutDateTime(mode.getOutboundDateTime());
			newMode.setOutDateTimeString(sdf.format(mode.getOutboundDateTime()));
			newMode.setInDateTime(mode.getInboundDateTime());
			if(mode.getInboundDateTime()!=null){
				newMode.setInDateTimeString(sdf.format(mode.getInboundDateTime()));
			}
			newMode.setCarPark(mode.getCarParking());
			newMode.setHandBag(mode.getIsHandLuggage());
			newMode.setHoldBag(mode.getIsHoldLuggage());
			newMode.setMileage(mode.getRoundTripMileage());
			newMode.setId(mode.getId());
			newMode.setSupplierName(mode.getSupplierName());
			newMode.setTotalCharge(mode.getTotalCharge());
			response.getTravelModes().add(newMode);
		}
		
		for(TravelAccommodationDetails hotel: dbRecord.getTravelHotelDetails()){
			TravelAccommodationModel newHotel = new TravelAccommodationModel();
			newHotel.setCheckinDate(hotel.getCheckinDate());
			newHotel.setCheckinDateString(sdf.format(hotel.getCheckinDate()));
			newHotel.setCheckoutDate(hotel.getCheckoutDate());
			newHotel.setCheckoutDateString(sdf.format(hotel.getCheckoutDate()));
			newHotel.setBreakfast(hotel.getIsHotelBreakfast());
			newHotel.setEvemeal(hotel.getHotelEveningMeal());
			newHotel.setBreakfastCharge(hotel.getBreakfastCharge());
			newHotel.setMealCharge(hotel.getMealCharge());
			newHotel.setPerNightCharge(hotel.getPerNightCharge());
			newHotel.setSupplierName(hotel.getSuuplierName());
			newHotel.setId(hotel.getId());
			response.getHotels().add(newHotel);
		}
		
		List<TravelQuotes> travelQuotes = travelQuotesRepo.findByTravelRequestId(travelId);
		for(TravelQuotes quote : travelQuotes){
			Quote newQ = new Quote();
			newQ.setPath(quote.getPath());
			newQ.setQuote(quote.getQuote());
			response.getTravelQuotes().add(newQ);
		}
		return response;
	
	}
	
	/***
	 * 
	 * @param statusReq
	 * @return
	 */
	public String updateTravelRequestStatus(UpdateStatusRequest statusReq) {
		String status = "";
		String response = "OK";

		Users user = userRepo.findById(statusReq.getApproverId());
		TravelRequest requestToUpdate = travelReqRepo.findOne(statusReq.getTravelRequestId());
		requestToUpdate.setUpdatedAt(new Date());
		requestToUpdate.setLastUpdatedBy(user.getUserName());
		if (statusReq.getAction().equals(PurchaseOrderActionEnum.REJECT.name())) {
			// This is stage 1 reject - first go REJECT in same level
			status = statusReq.getStatus().replace(PurchaseOrderStatusEnum._PENDING.name(),
					PurchaseOrderStatusEnum._REJECTED.name());
		} else if (statusReq.getAction().equals(PurchaseOrderActionEnum.APPROVE.name())
				|| statusReq.getAction().equals(PurchaseOrderActionEnum.APPROVEFINAL.name())) {
			status = statusReq.getStatus().replace(PurchaseOrderStatusEnum._PENDING.name(),
					PurchaseOrderStatusEnum._APPROVED.name());
		} else if (statusReq.getAction().equals(PurchaseOrderActionEnum.CLOSE.name())) {
			status = PurchaseOrderStatusEnum._CLOSED.name();
		} else if (statusReq.getAction().equals(PurchaseOrderActionEnum.CHANGEAPPROVER.name())) {
			status = statusReq.getNextLevel() + PurchaseOrderStatusEnum._PENDING.name();
			requestToUpdate.setCurrApproverId(statusReq.getNextApproverId());
		}
		requestToUpdate.setStatus(status);
		TravelRequest savedReq = travelReqRepo.save(requestToUpdate);
		TravelRequestHistory newHist = new TravelRequestHistory();
		newHist.setTravelRequestId(statusReq.getTravelRequestId());
		newHist.setStatus(helper.getStatus(savedReq.getStatus()));
		newHist.setLevel(helper.getLevel(savedReq.getStatus()));
		newHist.setLastUpdatedBy(user.getUserName());
		newHist.setUpdatedAt(new Date());
		newHist.setComments(statusReq.getComments());
		newHist.setUpdatedByUserId(statusReq.getApproverId());
		travelHistRepo.save(newHist);

		// IF its ApproveFinal - Create PO
		if (statusReq.getAction().equals(PurchaseOrderActionEnum.APPROVEFINAL.name())){
			//CREATE PO
			logger.info("All Final. Create orders now");
		}

		return response;
	}
	
	public String updateExpenseCodes(){
		String response = "OK";
		ExpenseCodeExcelFileReader rc = new ExpenseCodeExcelFileReader(); // object of the class
		// reading the value of 2nd row and 2nd column
		  List<DeptExpense> values  = rc.readCellData();
		  deptExpenseRepository.save(values);
		return response;
		
	}

	public Capex saveCapexForm(CapexFormModel capexFormModel) {
		
		Capex capexdetails =  new Capex();
		Long id = capexFormModel.getCapexId();
		if(id != null && id != 0){
			capexdetails = capexRepository.findOne(id);
		}
								
		capexdetails.setPurchaceType(capexFormModel.getPurchaceType());
		capexdetails.setPaymentTermsId(capexFormModel.getPaymentTermsId());
		capexdetails.setDescJustification(capexFormModel.getDescJustification());
		capexdetails.setEffectOnOps(capexFormModel.getEffectOnOps());
		capexdetails.setEquipmentReplacement(capexFormModel.getEquipmentReplacement());
		capexdetails.setFinanceApproveId(capexFormModel.getFinanceApproveId());
		capexdetails.setGmApproveId(capexFormModel.getGmApproveId());
		capexdetails.setMonthsDepreciation(capexFormModel.getMonthsDepreciation());
		capexdetails.setStatus(capexFormModel.getStatus());
		capexdetails.setRequestorId(capexFormModel.getRequestorId());
		capexdetails.setComments(capexFormModel.getComments());
		capexdetails.setCreatedAt(new Date());
		capexdetails.setCreatedBy(capexFormModel.getCurrentUserName());
		
		Capex savedCapex =  capexRepository.save(capexdetails);
		
		return savedCapex ;
	}
	
	public String updateCapexForm(CapexFormModel capexFormModel) {
		
		Capex capexdetails =  new Capex();
		Long id = capexFormModel.getCapexId();
		
		if(id != null && id != 0){
			capexdetails = capexRepository.findOne(id);
			if(capexFormModel.getStatus().equals("CLOSED")) {
				
				PurchaseOrderItems item =  poItemsRepo.findByCapexId(id);
				if(item != null) {
					item.setIsAsset(false);
					item.setUpdatedAt(new Date());
					item.setLastUpdatedBy(capexFormModel.getCurrentUserName());
					pOItemRepository.save(item);
				}
				
			}
		}
		
		capexdetails.setPurchaceType(capexFormModel.getPurchaceType());
		capexdetails.setPaymentTermsId(capexFormModel.getPaymentTermsId());
		capexdetails.setDescJustification(capexFormModel.getDescJustification());
		capexdetails.setEffectOnOps(capexFormModel.getEffectOnOps());
		capexdetails.setEquipmentReplacement(capexFormModel.getEquipmentReplacement());
		capexdetails.setFinanceApproveId(capexFormModel.getFinanceApproveId());
		capexdetails.setGmApproveId(capexFormModel.getGmApproveId());
		capexdetails.setMonthsDepreciation(capexFormModel.getMonthsDepreciation());
		capexdetails.setStatus(capexFormModel.getStatus());
		capexdetails.setRequestorId(capexFormModel.getRequestorId());
		capexdetails.setComments(capexFormModel.getComments());
		capexdetails.setUpdatedAt(new Date());
		capexdetails.setLastUpdatedBy(capexFormModel.getCurrentUserName());
		
		Capex capex = capexRepository.save(capexdetails);

		logger.info("OK-" + capex.getId() + "-" + capex.getStatus());
		return "OK-" + capex.getId() + "-" + capex.getStatus();
	}

	@Override
	public List<CapexResponse> getCapexRequests(long userId) {
		
		List<CapexResponse> capexResList = new ArrayList<CapexResponse>();
		//List<Capex> capexes = capexRepository.findByRequestorId(userId);
		List<Capex> capexes = capexRepository.findByRequestorIdOrderByCreatedAtDesc(userId);
		
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setLenient(false);
		
		for (Capex capex : capexes) {
			
			CapexResponse capexResponse = new CapexResponse();		
			Long capexId = capex.getId();			
		 	PurchaseOrderItems item =  poItemsRepo.findByCapexId(capexId);
		 	
		 	if(item !=  null) {
				capexResponse.setCapexId(capexId);
				capexResponse.setPaymentTermsId(capex.getPaymentTermsId());
				capexResponse.setMonthsDepreciation(capex.getMonthsDepreciation());
				capexResponse.setEquipmentReplacement(capex.getEquipmentReplacement());
				capexResponse.setPurchaceType(capex.getPurchaceType());
				capexResponse.setDescJustification(capex.getDescJustification());
				capexResponse.setEffectOnOps(capex.getEffectOnOps());
				capexResponse.setStatus(capex.getStatus());	
				capexResponse.setItemName(item.getItemName());
				capexResponse.setDeptToCharge(item.getDeptToCharge());
				capexResponse.setExpenseCode(item.getExpenseCode());
				capexResponse.setQty(item.getQty());
				capexResponse.setUnitPrice(item.getUnitPrice());
				capexResponse.setPurchaseOrderId(item.getPurchaseOrder().getPurchaseOrderId());
				if(capex.getCreatedAt() != null) {
					capexResponse.setCreatedAt(sdf.format(capex.getCreatedAt()));
				}
			
				if(capex.getFinanceApproveId() !=null ) {
					capexResponse.setFinanceApproveId(capex.getFinanceApproveId());
					String apprName = userRepo.getFullNamebyId(capex.getFinanceApproveId());
					capexResponse.setFinanceApproverName(apprName);
				}
				if(capex.getGmApproveId() != null) {
					capexResponse.setGmApproveId(capex.getGmApproveId());
					String gMapprName = userRepo.getFullNamebyId(capex.getGmApproveId());
					capexResponse.setGmApproverName(gMapprName);
				}
				capexResList.add(capexResponse);
		 	}
		}
		
		return capexResList;
	}

	@Override
	public List<CapexResponse> getAllCapexRequests(String status) {
		List<CapexResponse> capexResList = new ArrayList<CapexResponse>();
		List<Capex> capexes = new ArrayList<>();
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		sdf.setLenient(false);
		
		if(status.equalsIgnoreCase("PENDING")) {
			capexes = capexRepository.findAllPendingRequests();
		}else if(status.equalsIgnoreCase("ALL")) {
			capexes = capexRepository.findAll();
		}
		
		for (Capex capex : capexes) {
			
			CapexResponse capexResponse = new CapexResponse();		
			Long capexId = capex.getId();			
		 	PurchaseOrderItems item =  poItemsRepo.findByCapexId(capexId);
		 	
		 	if(item !=  null) {
				capexResponse.setCapexId(capexId);
				capexResponse.setPaymentTermsId(capex.getPaymentTermsId());
				capexResponse.setMonthsDepreciation(capex.getMonthsDepreciation());
				capexResponse.setEquipmentReplacement(capex.getEquipmentReplacement());
				capexResponse.setPurchaceType(capex.getPurchaceType());
				capexResponse.setDescJustification(capex.getDescJustification());
				capexResponse.setEffectOnOps(capex.getEffectOnOps());
				capexResponse.setStatus(capex.getStatus());	
				capexResponse.setItemName(item.getItemName());
				capexResponse.setDeptToCharge(item.getDeptToCharge());
				capexResponse.setExpenseCode(item.getExpenseCode());
				capexResponse.setQty(item.getQty());
				capexResponse.setUnitPrice(item.getUnitPrice());
				capexResponse.setPurchaseOrderId(item.getPurchaseOrder().getPurchaseOrderId());
				if(capex.getCreatedAt() != null) {
					capexResponse.setCreatedAt(sdf.format(capex.getCreatedAt()));
				}
				
				if(capex.getFinanceApproveId() !=null ) {
					capexResponse.setFinanceApproveId(capex.getFinanceApproveId());
					String apprName = userRepo.getFullNamebyId(capex.getFinanceApproveId());
					capexResponse.setFinanceApproverName(apprName);
				}
				if(capex.getGmApproveId() != null) {
					capexResponse.setGmApproveId(capex.getGmApproveId());
					String gMapprName = userRepo.getFullNamebyId(capex.getGmApproveId());
					capexResponse.setGmApproverName(gMapprName);
				}
				if(capex.getRequestorId() != null) {
					capexResponse.setRequestorId(capex.getRequestorId());
					capexResponse.setRequestorName(userRepo.getFullNamebyId(capex.getRequestorId()));
				}
				capexResList.add(capexResponse);
		 	}
		}
		
		return capexResList;
	}
	
	
}
