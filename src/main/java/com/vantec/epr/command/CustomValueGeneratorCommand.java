package com.vantec.epr.command;

public interface CustomValueGeneratorCommand<T> {

	public T generate(Long id,String name);
}
