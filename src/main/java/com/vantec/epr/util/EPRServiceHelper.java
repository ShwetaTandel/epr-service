package com.vantec.epr.util;

import java.text.SimpleDateFormat;

import com.vantec.epr.entity.BudgetBody;
import com.vantec.epr.entity.PurchaseOrder;
import com.vantec.epr.model.PurchaseOrderRequest;

public final class EPRServiceHelper {

	public void createPurchaseOrderObjectFromRequest(PurchaseOrderRequest orderReq, PurchaseOrder purchaseOrder) {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		// Section 1 Data
		purchaseOrder.setTitle(orderReq.getTitle());
		if(orderReq.getIsEmergency()!=null && orderReq.getIsEmergency()){
			purchaseOrder.setIsEmergency(true);
		}
		
		purchaseOrder.setRequestedDate(orderReq.getRequestedDate());
		purchaseOrder.setDeliveryDate(orderReq.getDeliveryDate());
		purchaseOrder.setRequestorDeptId(orderReq.getRequestorDeptId());
		purchaseOrder.setPurchaseType(orderReq.getPurchaseType());
		purchaseOrder.setProjectName(orderReq.getProjectName());
		purchaseOrder.setReasonType(orderReq.getReasonType());
		purchaseOrder.setReasonDescription(orderReq.getReasonDescription());
		purchaseOrder.setDeptToChargeId(orderReq.getDeptToChargeId());
		purchaseOrder.setIsBudgeted(orderReq.getIsBudgeted());
		purchaseOrder.setIsRecharge(orderReq.getIsRecharge());
		if (purchaseOrder.getIsRecharge()) {
			purchaseOrder.setRechargeRef(orderReq.getRechargeRef());
			purchaseOrder.setRechargeTo(orderReq.getRechargeTo());
			purchaseOrder.setRechargeOthers(orderReq.getRechargeOthers());

		}
		purchaseOrder.setIsGDPRFlagged(orderReq.getIsGDPRFlagged());


		// Section 2 Data
		purchaseOrder.setPreferred_supplier(orderReq.getPreferred_supplier());
		purchaseOrder.setReasonOfChoice(orderReq.getReasonOfChoice());
		purchaseOrder.setSupplierContactDetails(orderReq.getSupplierContactDetails());
		purchaseOrder.setDoaApprovalRequired(orderReq.getDoaApprovalRequired());
		purchaseOrder.setNumberOfQuotes(orderReq.getNumberOfQuotes());
		purchaseOrder.setSpecialRequirements(orderReq.getSpecialRequirements());
		purchaseOrder.setDiscountsAchieved(orderReq.getDiscountsAchieved());
		if (purchaseOrder.getPurchaseType().equals("Contract")) {
			purchaseOrder.setContractFromDate(orderReq.getContractFromDate());
			purchaseOrder.setContractToDate(orderReq.getContractToDate());
		}
		purchaseOrder.setCarrierCharges(orderReq.getCarrierCharges());
		purchaseOrder.setDiscount(orderReq.getDiscount());
		purchaseOrder.setGrand_total(orderReq.getGrand_total());
		purchaseOrder.setDeliveryAddress(orderReq.getDeliveryAddress());
	

	}
	/*UTILITY method**/
	public String getLevel(String status){
		String level = "";
		if(status!=null){
			String [] temp = status.split("_");
			if(temp!=null & temp.length > 1){
				level = temp[0];
			}
		}
		return level;
	}
	/*UTILITY method**/
	public String getStatus(String wholeStatus){
		String status = "";
		if(wholeStatus!=null){
			String [] temp = wholeStatus.split("_");
			if(temp!=null & temp.length > 1){
				status = temp[1];
			}
		}
		return status;
	}
	
	/*UTILITY METHOD*/
	public String getDescriptionForLevel(String level){
		
		switch(level){
		case "L1": return "Job approval - L1";
		case "L2": return "Job approval - L2";
		case "L3": return "Job approval - L3";
		case "L4": return "Job approval - L4";
		case "L5": return "Job approval - L5";
		case "P1": return "Purchasing approval - P1";
		case "P2": return "Purchasing approval - P2";
		case "P3": return "Purchasing approval - P3";
		default : return "Level";
		}
		
	}
	
	public Float getAmountForLevel(String level){
		
		switch(level){
		case "L1": return 150F;
		case "L2": return 250F;
		case "L3": return 500F;
		case "L4": return 50000000F;
		case "L5": return 50000000F;
		case "P1": return 500F;
		case "P2": return 2500F;
		case "P3": return 50000000F;
		default : return 0F;
		}
		
	}


}
