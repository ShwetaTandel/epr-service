package com.vantec.epr.util;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;

import com.vantec.epr.entity.DeptExpense;
import com.vantec.epr.repository.DeptExpenseRepository;


public class ExpenseCodeExcelFileReader {
	
	
	public static void main(String[] args) {
		ExpenseCodeExcelFileReader rc = new ExpenseCodeExcelFileReader(); // object of the class
		// reading the value of 2nd row and 2nd column
		rc.readCellData();
	}

	// method defined for reading a cell
	public List<DeptExpense> readCellData() {
		  List<DeptExpense> response = new ArrayList<DeptExpense>();
		 try
	        {
	            FileInputStream file = new FileInputStream("C:\\Users\\standel\\Downloads\\GL code and dept to charge.xlsx");
	          
	            
	 
	            //Create Workbook instance holding reference to .xlsx file
	            XSSFWorkbook workbook = new XSSFWorkbook(file);
	 
	            //Get first/desired sheet from the workbook
	            XSSFSheet sheet = workbook.getSheetAt(0);
	 
	            
	            for(int i=2; i<sheet.getRow(sheet.getTopRow()).getLastCellNum(); i++){
	                Iterator<Row> rowIterator=sheet.rowIterator();
	                int j =0;
	                String deptCode = "" , expenseCode= "";
	                while(rowIterator.hasNext()){
	                	
	                	if(j== 0){
	                		deptCode = rowIterator.next().getCell(i).toString();
	                	}else{
	                		 expenseCode= "";
	                		 expenseCode = rowIterator.next().getCell(i)+"";
	                		
	                			
	                	}
	                  
	                    j++;
	                    if(!expenseCode.isEmpty() && !expenseCode.equals("null")){
		                    DeptExpense value = new DeptExpense();
		                    value.setDeptCode(deptCode);
		                    int iend = expenseCode.indexOf("."); //this finds the first occurrence of "." 
            	       		String subString = expenseCode;
		            		if (iend != -1) 
		            		{
		            		    subString= expenseCode.substring(0 , iend); //this will give abc
		            		}
		                    value.setExpenseCode(subString);
		                    response.add(value);
		                    System.out.println(value.getDeptCode() + "   "+value.getExpenseCode()); //do what you want
	                    }
	                

	                }

	            }
	            
	            
	           /* //Iterate through each rows one by one
	            Iterator<Row> rowIterator = sheet.iterator();
	            while (rowIterator.hasNext()) 
	            {
	                Row row = rowIterator.next();
	                //For each row, iterate through all the columns
	                Iterator<Cell> cellIterator = row.cellIterator();
	                 
	                while (cellIterator.hasNext()) 
	                {
	                    Cell cell = cellIterator.next();
	                    //Check the cell type and format accordingly
	                    switch (cell.getCellType()) 
	                    {
	                        case NUMERIC:
	                            System.out.print(cell.getNumericCellValue() + "t");
	                            break;
	                        case STRING:
	                            System.out.print(cell.getStringCellValue() + "t");
	                            break;
						default:
							break;
	                    }
	                }
	                System.out.println("");
	            }*/
	            workbook.close();
	            file.close();
	        } 
	        catch (Exception e) 
	        {
	            e.printStackTrace();
	        }
		 
		 return response ;
	}
}