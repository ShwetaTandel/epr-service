package com.vantec.epr.util;

public enum PurchaseOrderStatusEnum {
	_PENDING, _APPROVED, _REJECTED, _CLOSED, _COMPLETED
}
