package com.vantec.epr.util;

public interface AddressConstants {

	  public static final String CHERRY_BLOSSOM_ADDR= "Cherry Blossom Way, Sunderland SR5 3QZ";
	  public static final String HILLTHORN_ADDR= "3 Infiniti Drive, Hillthorn Business Park, Washington, Tyne & Wear, NE37 3HG";
	  public static final String TURBINE_ADDR= "Turbine Business Park, Sunderland";
	  public static final String CHERRY_BLOSSOM= "Cherry Blossom";
	  public static final String HILLTHORN= "Hillthorn";
	  public static final String TURBINE= "Turbine";
	  
}
