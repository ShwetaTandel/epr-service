package com.vantec.epr.model;


import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class ApproversResponse {
	
	private List<ApproverDetails> approvers = new ArrayList<>();
	public List<ApproverDetails> getApprovers() {
		return approvers;
	}
	public void setApprovers(List<ApproverDetails> approvers) {
		this.approvers = approvers;
	}
	/***
	 * Check if level already exists in the Response
	 * @param level
	 * @return
	 */
	public void addUser(String level,User user){
		
		boolean isAdded = false;
		for(ApproverDetails tmp : this.approvers){
			if(tmp.getLevel().equals(level)){
				tmp.getUsers().add(user);
				isAdded = true;
			}
		}
		if(isAdded == false){
			ApproverDetails newObj = new ApproverDetails();
			newObj.setLevel(level);
			newObj.getUsers().add(user);
			this.approvers.add(newObj);
		}
	}
}
