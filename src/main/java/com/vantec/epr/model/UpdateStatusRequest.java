package com.vantec.epr.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class UpdateStatusRequest {
	
	
	private Long purchaseOrderId;
	private Long travelRequestId;
	private Long approverId;
	private String status;
	private String action;
	private String comments;
	private String rejectReason;
	private String nextLevel;
	private Long nextApproverId;
	
	
	public Long getTravelRequestId() {
		return travelRequestId;
	}
	public void setTravelRequestId(Long travelRequestId) {
		this.travelRequestId = travelRequestId;
	}
	public String getNextLevel() {
		return nextLevel;
	}
	public void setNextLevel(String nextLevel) {
		this.nextLevel = nextLevel;
	}

	public Long getNextApproverId() {
		return nextApproverId;
	}
	public void setNextApproverId(Long nextApproverId) {
		this.nextApproverId = nextApproverId;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getRejectReason() {
		return rejectReason;
	}
	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}
	public Long getPurchaseOrderId() {
		return purchaseOrderId;
	}
	public void setPurchaseOrderId(Long purchaseOrderId) {
		this.purchaseOrderId = purchaseOrderId;
	}
	public Long getApproverId() {
		return approverId;
	}
	public void setApproverId(Long requestorId) {
		this.approverId = requestorId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
}
