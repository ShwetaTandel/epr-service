package com.vantec.epr.model;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class TravelRequestModel {
	
	private Long travelRequestId;
	private Long requestorId;
	private Long travellerId;
	private Long travelRequestorId;
	
	private String travelReason;
	private Date travelDate;
	private Date returnDate;
	
	private String travelDateString;
	private String returnDateString;


	private Long deptToChargeId;
	private String isBudgeted;
	private String addInfo;
	private Boolean isRecharge;
	private String rechargeRef;
	private String rechargeTo;
	private String rechargeOthers;
	private String comments;
	private String status;
	private Long currApproverId;
	private List<TravelModeModel> travelModes = new ArrayList<TravelModeModel>();
	private List<TravelAccommodationModel> hotels = new ArrayList<TravelAccommodationModel>();
	private List<Quote> travelQuotes = new ArrayList<Quote>();
	private String action;
	private Boolean isGDPRFlagged;
	//**Fields use for response**//
	
	private String curApproverName;
	private String travellerName;
	private String travelRequestorName;
	private String costCentre;
	private Float totalTravelCost = 0.0F;
	
	
	
	public Float getTotalTravelCost() {
		return totalTravelCost;
	}
	public void setTotalTravelCost(Float totalTravelCost) {
		this.totalTravelCost = totalTravelCost;
	}
	public List<Quote> getTravelQuotes() {
		return travelQuotes;
	}
	public void setTravelQuotes(List<Quote> travelQuotes) {
		this.travelQuotes = travelQuotes;
	}
	public String getCurrApproverName() {
		return curApproverName;
	}
	public void setCurrApproverName(String cuurApproverName) {
		this.curApproverName = cuurApproverName;
	}
	public String getTravellerName() {
		return travellerName;
	}
	public void setTravellerName(String travellerName) {
		this.travellerName = travellerName;
	}
	public String getTravelRequestorName() {
		return travelRequestorName;
	}
	public void setTravelRequestorName(String travelRequestorName) {
		this.travelRequestorName = travelRequestorName;
	}
	public String getCostCentre() {
		return costCentre;
	}
	public void setCostCentre(String costCentre) {
		this.costCentre = costCentre;
	}
	
	public String getTravelDateString() {
		return travelDateString;
	}
	public void setTravelDateString(String travelDateString) {
		this.travelDateString = travelDateString;
	}
	public String getReturnDateString() {
		return returnDateString;
	}
	public void setReturnDateString(String returnDateString) {
		this.returnDateString = returnDateString;
	}
	public String getCurApproverName() {
		return curApproverName;
	}
	public void setCurApproverName(String curApproverName) {
		this.curApproverName = curApproverName;
	}
	public Long getTravelRequestId() {
		return travelRequestId;
	}
	public void setTravelRequestId(Long travelRequestId) {
		this.travelRequestId = travelRequestId;
	}
	public Long getRequestorId() {
		return requestorId;
	}
	public void setRequestorId(Long requestorId) {
		this.requestorId = requestorId;
	}
	public Long getTravellerId() {
		return travellerId;
	}
	public void setTravellerId(Long travellerId) {
		this.travellerId = travellerId;
	}
	public Long getTravelRequestorId() {
		return travelRequestorId;
	}
	public void setTravelRequestorId(Long travelRequestorId) {
		this.travelRequestorId = travelRequestorId;
	}
	public String getTravelReason() {
		return travelReason;
	}
	public void setTravelReason(String travelReason) {
		this.travelReason = travelReason;
	}
	public Date getTravelDate() {
		return travelDate;
	}
	public void setTravelDate(Date travelDate) {
		this.travelDate = travelDate;
	}
	public Date getReturnDate() {
		return returnDate;
	}
	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}
	public Long getDeptToChargeId() {
		return deptToChargeId;
	}
	public void setDeptToChargeId(Long deptToChargeId) {
		this.deptToChargeId = deptToChargeId;
	}
	public String getIsBudgeted() {
		return isBudgeted;
	}
	public void setIsBudgeted(String isBudgeted) {
		this.isBudgeted = isBudgeted;
	}
	public String getAddInfo() {
		return addInfo;
	}
	public void setAddInfo(String addInfo) {
		this.addInfo = addInfo;
	}
	public Boolean getIsRecharge() {
		return isRecharge;
	}
	public void setIsRecharge(Boolean isRecharge) {
		this.isRecharge = isRecharge;
	}
	public String getRechargeRef() {
		return rechargeRef;
	}
	public void setRechargeRef(String rechargeRef) {
		this.rechargeRef = rechargeRef;
	}
	public String getRechargeTo() {
		return rechargeTo;
	}
	public void setRechargeTo(String rechargeTo) {
		this.rechargeTo = rechargeTo;
	}
	public String getRechargeOthers() {
		return rechargeOthers;
	}
	public void setRechargeOthers(String rechargeOthers) {
		this.rechargeOthers = rechargeOthers;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Long getCurrApproverId() {
		return currApproverId;
	}
	public void setCurrApproverId(Long currApproverId) {
		this.currApproverId = currApproverId;
	}
	public List<TravelModeModel> getTravelModes() {
		return travelModes;
	}
	public void setTravelModes(List<TravelModeModel> travelModes) {
		this.travelModes = travelModes;
	}
	public List<TravelAccommodationModel> getHotels() {
		return hotels;
	}
	public void setHotels(List<TravelAccommodationModel> hotels) {
		this.hotels = hotels;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public Boolean getIsGDPRFlagged() {
		return isGDPRFlagged;
	}
	public void setIsGDPRFlagged(Boolean isGDPRFlagged) {
		this.isGDPRFlagged = isGDPRFlagged;
	}
	

}
