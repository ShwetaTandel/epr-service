package com.vantec.epr.model;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class PurchaseOrderResponse {
	
	private Long purchaseOrderId;
	private Long purchaseOrderNumber;
	private String requestorName;
	private String requestorUserName;
	private String currApproverName;
	private Long requestorId;
	private String title;
	private String requestedDate;
	private String deliveryDate;
	private String requestorDeptName;
	private Long requestorDivId;
	private Long requestorDeptId;
	private String purchaseType;
	private String projectName;
	private String reasonType;
	private String reasonDescription;
	private String deptToChargeName;
	private Long deptToChargeId;
	private Long deptToChargeDivId;
	private String isBudgeted;
	private Boolean isEmergency;
	private Boolean isAccrued;
	private Boolean isRecharge;
	private Boolean isGDPRFlagged;
	private String rechargeRef;
	private String rechargeTo;
	private String rechargeOthers;
	private String quoteSuppliedBy;
	private String preferredSupplier;
	private String reasonOfChoice;
	private String supplierContactDetails;
	private Boolean doaApprovalRequired;
	private Boolean isEmailed;
	private Integer numberOfQuotes;
	private String specialRequirements;
	private String discountsAchieved;
	private String contractToDate;
	private String contractFromDate;
	private Long adminDetailId = 0L;
	private Float discount;
	private Float carrierCharges;
	private Float grand_total;
	private String deliveryAddress;
	private String status;
	
	private Long currApproverId;
	
	//Admin details
	private String handledBy;
	private String alreadyPurchased;
	private String orderPlacedDate;
	private String supplierNameAndCode;
	private String expenseTypeAndCode;
	private Float savings;
	private Boolean isCompanyAsset;
	private Boolean capexCompelete;
	private List<DepartmentToCharge> deptToChargeList = new ArrayList<DepartmentToCharge>();
	private String purchasingComments;
	private String financeNotes;
	private String isProcessFollowed;
	private Boolean hasInvoices = false;
	private Float outstandingInvoiceAmount;
	private String paymentTerms;
	private String paymentType;
	private Boolean deliveryComplete  = true;
	private List<InvoiceModel> invoices = new ArrayList<InvoiceModel>();
	private List<CreditNoteModel> creditNotes = new ArrayList<CreditNoteModel>();
	private List<Item> totalOfInvoicedItems = new ArrayList<Item>();
	private List<DeliveryNoteModel> deliveryNotes = new ArrayList<DeliveryNoteModel>();
	private Map<Long, Integer> outstandingDelivery = new HashMap<Long, Integer>();
	private Map<Long, Integer> outstandingInvoiceQty = new HashMap<Long, Integer>();
	private String approvalDate;
	private Long budgetBodyId;
	private String budgetBodyItem;
	private String budgetCategory;
	private String budgetCostCentre;
	private Boolean overBudget = false;
	private Float outstandingCreditAmount;
	private String financialYear;
	
	public String getBudgetCategory() {
		return budgetCategory;
	}

	public void setBudgetCategory(String budgetCategory) {
		this.budgetCategory = budgetCategory;
	}

	public String getBudgetCostCentre() {
		return budgetCostCentre;
	}

	public void setBudgetCostCentre(String budgetCostCentre) {
		this.budgetCostCentre = budgetCostCentre;
	}

	public Long getRequestorDeptId() {
	return requestorDeptId;
}

public Boolean getOverBudget() {
		return overBudget;
	}

	public void setOverBudget(Boolean overBudget) {
		this.overBudget = overBudget;
	}

public Long getBudgetBodyId() {
		return budgetBodyId;
	}

	public void setBudgetBodyId(Long budgetBodyId) {
		this.budgetBodyId = budgetBodyId;
	}

	public String getBudgetBodyItem() {
		return budgetBodyItem;
	}

	public void setBudgetBodyItem(String budgetBodyItem) {
		this.budgetBodyItem = budgetBodyItem;
	}

public void setRequestorDeptId(Long requestorDeptId) {
	this.requestorDeptId = requestorDeptId;
}

	public Float getOutstandingCreditAmount() {
		return outstandingCreditAmount;
	}

	public void setOutstandingCreditAmount(Float outstandingCreditAmount) {
		this.outstandingCreditAmount = outstandingCreditAmount;
	}

	public List<CreditNoteModel> getCreditNotes() {
		return creditNotes;
	}
	public void setCreditNotes(List<CreditNoteModel> creditNotes) {
		this.creditNotes = creditNotes;
	}
	public String getRequestorUserName() {
		return requestorUserName;
	}
	public void setRequestorUserName(String requestorUserName) {
		this.requestorUserName = requestorUserName;
	}
	public String getApprovalDate() {
		return approvalDate;
	}
	public void setApprovalDate(String approvalDate) {
		this.approvalDate = approvalDate;
	}


	private Boolean isFinalized;
	
	public Boolean getIsFinalized() {
		return isFinalized;
	}
	public void setIsFinalized(Boolean isFinalized) {
		this.isFinalized = isFinalized;
	}
	public Boolean getIsGDPRFlagged() {
		return isGDPRFlagged;
	}
	public void setIsGDPRFlagged(Boolean isGDPRFlagged) {
		this.isGDPRFlagged = isGDPRFlagged;
	}
	public Boolean getIsEmailed() {
		return isEmailed;
	}
	public void setIsEmailed(Boolean isEmailed) {
		this.isEmailed = isEmailed;
	}
	public Boolean getIsEmergency() {
		return isEmergency;
	}
	public void setIsEmergency(Boolean isEmergency) {
		this.isEmergency = isEmergency;
	}
	public Boolean getIsAccrued() {
		return isAccrued;
	}
	public void setIsAccrued(Boolean isAccrued) {
		this.isAccrued = isAccrued;
	}
	public Map<Long, Integer> getOutstandingInvoiceQty() {
		return outstandingInvoiceQty;
	}
	public void setOutstandingInvoiceQty(Map<Long, Integer> outstandingInvoice) {
		this.outstandingInvoiceQty = outstandingInvoice;
	}
	public List<Item> getTotalOfInvoicedItems() {
		return totalOfInvoicedItems;
	}
	public void setTotalOfInvoicedItems(List<Item> totalOfInvoicedItems) {
		this.totalOfInvoicedItems = totalOfInvoicedItems;
	}
	public String getCurrApproverName() {
		return currApproverName;
	}
	public void setCurrApproverName(String currApproverName) {
		this.currApproverName = currApproverName;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public List<InvoiceModel> getInvoices() {
		return invoices;
	}
	public void setInvoices(List<InvoiceModel> invoices) {
		this.invoices = invoices;
	}

	public Boolean getHasInvoices() {
		return hasInvoices;
	}

	public void setHasInvoices(Boolean hasInvoices) {
		this.hasInvoices = hasInvoices;
	}

	public Float getOutstandingInvoiceAmount() {
		return outstandingInvoiceAmount;
	}
	public void setOutstandingInvoiceAmount(Float outstandingInvoiceAmount) {
		this.outstandingInvoiceAmount = outstandingInvoiceAmount;
	}
	public String getPaymentTerms() {
		return paymentTerms;
	}
	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}
	public Long getDeptToChargeId() {
		return deptToChargeId;
	}
	public void setDeptToChargeId(Long deptToChargeId) {
		this.deptToChargeId = deptToChargeId;
	}
	public Long getDeptToChargeDivId() {
		return deptToChargeDivId;
	}
	public void setDeptToChargeDivId(Long deptToChargeDivId) {
		this.deptToChargeDivId = deptToChargeDivId;
	}



	public Boolean getDeliveryComplete() {
		return deliveryComplete;
	}
	public void setDeliveryComplete(Boolean deliveryComplete) {
		this.deliveryComplete = deliveryComplete;
	}
	public Map<Long, Integer> getOutstandingDelivery() {
		return outstandingDelivery;
	}
	public void setOutstandingDelivery(Map<Long, Integer> outstanding) {
		this.outstandingDelivery = outstanding;
	}
	
	public List<DeliveryNoteModel> getDeliveryNotes() {
		return deliveryNotes;
	}
	public void setDeliveryNotes(List<DeliveryNoteModel> deliveryNotes) {
		this.deliveryNotes = deliveryNotes;
	}
	public Long getRequestorId() {
		return requestorId;
	}
	public void setRequestorId(Long requestorId) {
		this.requestorId = requestorId;
	}

	public String getIsBudgeted() {
		return isBudgeted;
	}
	public void setIsBudgeted(String isBudgeted) {
		this.isBudgeted = isBudgeted;
	}
	
	public String getRechargeOthers() {
		return rechargeOthers;
	}
	public void setRechargeOthers(String rechargeOthers) {
		this.rechargeOthers = rechargeOthers;
	}
	public Long getPurchaseOrderNumber() {
		return purchaseOrderNumber;
	}
	public void setPurchaseOrderNumber(Long purchaseOrderNumber) {
		this.purchaseOrderNumber = purchaseOrderNumber;
	}

	public Long getAdminDetailId() {
		return adminDetailId;
	}
	public void setAdminDetailId(Long adminDetailId) {
		this.adminDetailId = adminDetailId;
	}

	public String getIsProcessFollowed() {
		return isProcessFollowed;
	}
	public void setIsProcessFollowed(String isProcessFollowed) {
		this.isProcessFollowed = isProcessFollowed;
	}
	public String getHandledBy() {
		return handledBy;
	}
	public void setHandledBy(String handledBy) {
		this.handledBy = handledBy;
	}
	public String getAlreadyPurchased() {
		return alreadyPurchased;
	}
	public void setAlreadyPurchased(String alreadyPurchased) {
		this.alreadyPurchased = alreadyPurchased;
	}
	public String getOrderPlacedDate() {
		return orderPlacedDate;
	}
	public void setOrderPlacedDate(String orderPlacedDate) {
		this.orderPlacedDate = orderPlacedDate;
	}
	public String getSupplierNameAndCode() {
		return supplierNameAndCode;
	}
	public void setSupplierNameAndCode(String supplierNameAndCode) {
		this.supplierNameAndCode = supplierNameAndCode;
	}
	public String getExpenseTypeAndCode() {
		return expenseTypeAndCode;
	}
	public void setExpenseTypeAndCode(String expenseTypeAndCode) {
		this.expenseTypeAndCode = expenseTypeAndCode;
	}
	public Float getSavings() {
		return savings;
	}
	public void setSavings(Float savings) {
		this.savings = savings;
	}
	public Boolean getIsCompanyAsset() {
		return isCompanyAsset;
	}
	public void setIsCompanyAsset(Boolean isCompanyAsset) {
		this.isCompanyAsset = isCompanyAsset;
	}
	public Boolean getCapexCompelete() {
		return capexCompelete;
	}
	public void setCapexCompelete(Boolean capexCompelete) {
		this.capexCompelete = capexCompelete;
	}
	public List<DepartmentToCharge> getDeptToChargeList() {
		return deptToChargeList;
	}
	public void setDeptToChargeList(List<DepartmentToCharge> deptToChargeList) {
		this.deptToChargeList = deptToChargeList;
	}
	public String getPurchasingComments() {
		return purchasingComments;
	}
	public void setPurchasingComments(String purchasingComments) {
		this.purchasingComments = purchasingComments;
	}
	public String getFinanceNotes() {
		return financeNotes;
	}
	public void setFinanceNotes(String financeNotes) {
		this.financeNotes = financeNotes;
	}


	private String createAt;
	private String createdBy;
	private String updatedAt;
	private String lastUpdatedBy;	
	
	public Long getRequestorDivId() {
		return requestorDivId;
	}
	public void setRequestorDivId(Long requestorDivId) {
		this.requestorDivId = requestorDivId;
	}

	 
	private List<Item>items = new ArrayList<Item>();
	private List<Quote>quotes = new ArrayList<Quote>();
	public List<Quote> getQuotes() {
		return quotes;
	}
	public void setQuotes(List<Quote> quotes) {
		this.quotes = quotes;
	}
	public Long getPurchaseOrderId() {
		return purchaseOrderId;
	}
	public void setPurchaseOrderId(Long purchaseOrderId) {
		this.purchaseOrderId = purchaseOrderId;
	}
	public String getRequestorName() {
		return requestorName;
	}
	public void setRequestorName(String requestorName) {
		this.requestorName = requestorName;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getRequestedDate() {
		return requestedDate;
	}
	public void setRequestedDate(String requestedDate) {
		this.requestedDate = requestedDate;
	}
	public String getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public String getRequestorDeptName() {
		return requestorDeptName;
	}
	public void setRequestorDeptName(String requestorDeptName) {
		this.requestorDeptName = requestorDeptName;
	}
	public String getPurchaseType() {
		return purchaseType;
	}
	public void setPurchaseType(String purchaseType) {
		this.purchaseType = purchaseType;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getReasonType() {
		return reasonType;
	}
	public void setReasonType(String reasonType) {
		this.reasonType = reasonType;
	}
	public String getReasonDescription() {
		return reasonDescription;
	}
	public void setReasonDescription(String reasonDescription) {
		this.reasonDescription = reasonDescription;
	}
	public String getDeptToChargeName() {
		return deptToChargeName;
	}
	public void setDeptToChargeName(String deptToChargeName) {
		this.deptToChargeName = deptToChargeName;
	}
	public Boolean getIsRecharge() {
		return isRecharge;
	}
	public void setIsRecharge(Boolean isRecharge) {
		this.isRecharge = isRecharge;
	}
	public String getRechargeRef() {
		return rechargeRef;
	}
	public void setRechargeRef(String rechargeRef) {
		this.rechargeRef = rechargeRef;
	}
	public String getRechargeTo() {
		return rechargeTo;
	}
	public void setRechargeTo(String rechargeTo) {
		this.rechargeTo = rechargeTo;
	}
	public String getQuoteSuppliedBy() {
		return quoteSuppliedBy;
	}
	public void setQuoteSuppliedBy(String quoteSuppliedBy) {
		this.quoteSuppliedBy = quoteSuppliedBy;
	}
	public String getPreferredSupplier() {
		return preferredSupplier;
	}
	public void setPreferredSupplier(String preferredSupplier) {
		this.preferredSupplier = preferredSupplier;
	}
	public String getReasonOfChoice() {
		return reasonOfChoice;
	}
	public void setReasonOfChoice(String reasonOfChoice) {
		this.reasonOfChoice = reasonOfChoice;
	}
	public String getSupplierContactDetails() {
		return supplierContactDetails;
	}
	public void setSupplierContactDetails(String supplierContactDetails) {
		this.supplierContactDetails = supplierContactDetails;
	}
	public Boolean getDoaApprovalRequired() {
		return doaApprovalRequired;
	}
	public void setDoaApprovalRequired(Boolean doaApprovalRequired) {
		this.doaApprovalRequired = doaApprovalRequired;
	}
	public Integer getNumberOfQuotes() {
		return numberOfQuotes;
	}
	public void setNumberOfQuotes(Integer numberOfQuotes) {
		this.numberOfQuotes = numberOfQuotes;
	}
	public String getSpecialRequirements() {
		return specialRequirements;
	}
	public void setSpecialRequirements(String specialRequirements) {
		this.specialRequirements = specialRequirements;
	}
	public String getDiscountsAchieved() {
		return discountsAchieved;
	}
	public void setDiscountsAchieved(String discountsAchieved) {
		this.discountsAchieved = discountsAchieved;
	}
	public String getContractToDate() {
		return contractToDate;
	}
	public void setContractToDate(String contractToDate) {
		this.contractToDate = contractToDate;
	}
	public String getContractFromDate() {
		return contractFromDate;
	}
	public void setContractFromDate(String contractFromDate) {
		this.contractFromDate = contractFromDate;
	}
	public Float getDiscount() {
		return discount;
	}
	public void setDiscount(Float discount) {
		this.discount = discount;
	}
	public Float getCarrierCharges() {
		return carrierCharges;
	}
	public void setCarrierCharges(Float carrierCharges) {
		this.carrierCharges = carrierCharges;
	}
	public Float getGrand_total() {
		return grand_total;
	}
	public void setGrand_total(Float grand_total) {
		this.grand_total = grand_total;
	}
	public String getDeliveryAddress() {
		return deliveryAddress;
	}
	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Long getCurrApproverId() {
		return currApproverId;
	}
	public void setCurrApproverId(Long currApproverId) {
		this.currApproverId = currApproverId;
	}
	public String getCreateAt() {
		return createAt;
	}
	public void setCreateAt(String createAt) {
		this.createAt = createAt;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public List<Item> getItems() {
		return items;
	}
	public void setItems(List<Item> items) {
		this.items = items;
	}
	
	public String getFinancialYear() {
		return financialYear;
	}
	
	public void setFinancialYear(String financialYear) {
		this.financialYear = financialYear;
	}

}
