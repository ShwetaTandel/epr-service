package com.vantec.epr.model;

import java.util.ArrayList;
import java.util.List;

public class DivisionResponse {
	
	private String divName;
	private Long divId;
	private List<DepartmentResponse> deptList = new ArrayList<DepartmentResponse>();
	public String getDivName() {
		return divName;
	}
	public void setDivName(String divName) {
		this.divName = divName;
	}
	public Long getDivId() {
		return divId;
	}
	public void setDivId(Long divId) {
		this.divId = divId;
	}
	public List<DepartmentResponse> getDeptList() {
		return deptList;
	}
	public void setDeptList(List<DepartmentResponse> deptList) {
		this.deptList = deptList;
	} 

}
