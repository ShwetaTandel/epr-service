package com.vantec.epr.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown=true)
public class CapexResponse {
	
	private Long capexId;
	private Long paymentTermsId;
	private Integer monthsDepreciation;
	private Boolean equipmentReplacement;
	private String purchaceType;
	private String descJustification;
	private String effectOnOps;
	private Long gmApproveId;
	private String gmApproverName;
	private Long financeApproveId;
	private String financeApproverName;
	private String status;
	private Long requestorId;
	private String requestorName;
	private String createdAt;
	//ItemDetails
	private String itemName;
	private Long PurchaseOrderId;
	private String deptToCharge;
	private String expenseCode;
	private Integer qty;
	private Float unitPrice;
	
	
	public String getRequestorName() {
		return requestorName;
	}
	public void setRequestorName(String requestorName) {
		this.requestorName = requestorName;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	
	public Long getCapexId() {
		return capexId;
	}
	public void setCapexId(Long capexId) {
		this.capexId = capexId;
	}
	public Long getRequestorId() {
		return requestorId;
	}
	public void setRequestorId(Long requestorId) {
		this.requestorId = requestorId;
	}
	public Long getPurchaseOrderId() {
		return PurchaseOrderId;
	}
	public void setPurchaseOrderId(Long purchaseOrderId) {
		PurchaseOrderId = purchaseOrderId;
	}
	public Long getPaymentTermsId() {
		return paymentTermsId;
	}
	public void setPaymentTermsId(Long paymentTermsId) {
		this.paymentTermsId = paymentTermsId;
	}
	public Integer getMonthsDepreciation() {
		return monthsDepreciation;
	}
	public void setMonthsDepreciation(Integer monthsDepreciation) {
		this.monthsDepreciation = monthsDepreciation;
	}
	public Boolean getEquipmentReplacement() {
		return equipmentReplacement;
	}
	public void setEquipmentReplacement(Boolean equipmentReplacement) {
		this.equipmentReplacement = equipmentReplacement;
	}
	public String getPurchaceType() {
		return purchaceType;
	}
	public void setPurchaceType(String purchaceType) {
		this.purchaceType = purchaceType;
	}
	public String getDescJustification() {
		return descJustification;
	}
	public void setDescJustification(String descJustification) {
		this.descJustification = descJustification;
	}
	public String getEffectOnOps() {
		return effectOnOps;
	}
	public void setEffectOnOps(String effectOnOps) {
		this.effectOnOps = effectOnOps;
	}
	public Long getGmApproveId() {
		return gmApproveId;
	}
	public void setGmApproveId(Long gmApproveId) {
		this.gmApproveId = gmApproveId;
	}
	public Long getFinanceApproveId() {
		return financeApproveId;
	}
	public void setFinanceApproveId(Long financeApproveId) {
		this.financeApproveId = financeApproveId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getDeptToCharge() {
		return deptToCharge;
	}
	public void setDeptToCharge(String deptToCharge) {
		this.deptToCharge = deptToCharge;
	}
	public String getExpenseCode() {
		return expenseCode;
	}
	public void setExpenseCode(String expenseCode) {
		this.expenseCode = expenseCode;
	}
	public Integer getQty() {
		return qty;
	}
	public void setQty(Integer qty) {
		this.qty = qty;
	}
	public Float getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(Float unitPrice) {
		this.unitPrice = unitPrice;
	}

	public String getGmApproverName() {
		return gmApproverName;
	}
	public void setGmApproverName(String gmApproverName) {
		this.gmApproverName = gmApproverName;
	}
	public String getFinanceApproverName() {
		return financeApproverName;
	}
	public void setFinanceApproverName(String financeApproverName) {
		this.financeApproverName = financeApproverName;
	}
	


}
