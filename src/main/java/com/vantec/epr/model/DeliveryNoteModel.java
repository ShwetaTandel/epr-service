package com.vantec.epr.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DeliveryNoteModel {
	
	private Long id;
	private Long purchaseOrderId;
	private Long requestorId;
	private String deliveryNoteNumber;
	private Date deliveryDate;
	private String deliveryDateString;
	private Float totalAmount;
	private List<Item> deliveredItems = new ArrayList<Item>();
	private String deliveryNoteDocument;
	
	public String getDeliveryNoteDocument() {
		return deliveryNoteDocument;
	}
	public void setDeliveryNoteDocument(String deliveryNoteDocument) {
		this.deliveryNoteDocument = deliveryNoteDocument;
	}
	public String getDeliveryDateString() {
		return deliveryDateString;
	}
	public void setDeliveryDateString(String deliveryDateString) {
		this.deliveryDateString = deliveryDateString;
	}
	
	public Long getRequestorId() {
		return requestorId;
	}
	public void setRequestorId(Long requestorId) {
		this.requestorId = requestorId;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getDeliveryNoteNumber() {
		return deliveryNoteNumber;
	}
	public void setDeliveryNoteNumber(String deliveryNoteNumber) {
		this.deliveryNoteNumber = deliveryNoteNumber;
	}
	public Long getPurchaseOrderId() {
		return purchaseOrderId;
	}
	public void setPurchaseOrderId(Long purchaseOrderId) {
		this.purchaseOrderId = purchaseOrderId;
	}
	public Date getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public Float getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(Float totalAmount) {
		this.totalAmount = totalAmount;
	}
	public List<Item> getDeliveredItems() {
		return deliveredItems;
	}
	public void setDeliveredItems(List<Item> deliveredItems) {
		this.deliveredItems = deliveredItems;
	} 
}
