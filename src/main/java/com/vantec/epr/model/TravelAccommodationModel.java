package com.vantec.epr.model;


import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class TravelAccommodationModel {
	
	private Long id;

	private Date checkinDate;
	private Date checkoutDate;
	private String checkinDateString;
	private String checkoutDateString;

	private Boolean breakfast;
	private Boolean evemeal;
	private Float perNightCharge;
	private Float breakfastCharge;
	private Float mealCharge;
	private String supplierName;
	
	
	public String getCheckinDateString() {
		return checkinDateString;
	}
	public void setCheckinDateString(String checkinDateString) {
		this.checkinDateString = checkinDateString;
	}
	public String getCheckoutDateString() {
		return checkoutDateString;
	}
	public void setCheckoutDateString(String checkoutDateString) {
		this.checkoutDateString = checkoutDateString;
	}
	public Float getPerNightCharge() {
		return perNightCharge;
	}
	public void setPerNightCharge(Float perNightCharge) {
		this.perNightCharge = perNightCharge;
	}
	public Float getBreakfastCharge() {
		return breakfastCharge;
	}
	public void setBreakfastCharge(Float breakfastCharge) {
		this.breakfastCharge = breakfastCharge;
	}
	public Float getMealCharge() {
		return mealCharge;
	}
	public void setMealCharge(Float mealCharge) {
		this.mealCharge = mealCharge;
	}
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getCheckinDate() {
		return checkinDate;
	}
	public void setCheckinDate(Date checkinDate) {
		this.checkinDate = checkinDate;
	}
	public Date getCheckoutDate() {
		return checkoutDate;
	}
	public void setCheckoutDate(Date checkoutDate) {
		this.checkoutDate = checkoutDate;
	}
	public Boolean getBreakfast() {
		return breakfast;
	}
	public void setBreakfast(Boolean breakfast) {
		this.breakfast = breakfast;
	}
	public Boolean getEvemeal() {
		return evemeal;
	}
	public void setEvemeal(Boolean evemeal) {
		this.evemeal = evemeal;
	}
}
