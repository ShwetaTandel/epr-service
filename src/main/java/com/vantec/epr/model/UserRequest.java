package com.vantec.epr.model;

import java.util.ArrayList;
import java.util.List;

public class UserRequest {

	private Long userId;
	private String firstName;
	private String lastName;
	private Long empId;
	private String emailId;
	private String contact;
	private String position;
	private Boolean isApprover;
	private Boolean isAdmin;
	private Boolean isP0;
	private Boolean isL0;
	private Boolean isF0;
	private Boolean canRaiseEmergency;
	private List<String> divList = new ArrayList<String>();
	private List<String> deptList = new ArrayList<String>();
	
	private Long requestorId;

	public Boolean getIsF0() {
		return isF0;
	}
	public void setIsF0(Boolean isF0) {
		this.isF0 = isF0;
	}
	public Boolean getIsL0() {
		return isL0;
	}
	public void setIsL0(Boolean isL0) {
		this.isL0 = isL0;
	}
	public Boolean getIsP0() {
		return isP0;
	}
	public void setIsP0(Boolean isP0) {
		this.isP0 = isP0;
	}
	public Boolean getCanRaiseEmergency() {
		return canRaiseEmergency;
	}
	public void setCanRaiseEmergency(Boolean canRaiseEmergency) {
		this.canRaiseEmergency = canRaiseEmergency;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public Boolean getIsApprover() {
		return isApprover;
	}
	public void setIsApprover(Boolean isApprover) {
		this.isApprover = isApprover;
	}
	public List<String> getDivList() {
		return divList;
	}
	public void setDivList(List<String> divList) {
		this.divList = divList;
	}
	public List<String> getDeptList() {
		return deptList;
	}
	public void setDeptList(List<String> deptList) {
		this.deptList = deptList;
	}
	public Boolean getIsAdmin() {
		return isAdmin;
	}
	public void setIsAdmin(Boolean isAdmin) {
		this.isAdmin = isAdmin;
	}
	public Long getRequestorId() {
		return requestorId;
	}
	public void setRequestorId(Long requestorId) {
		this.requestorId = requestorId;
	}
	

}
