package com.vantec.epr.model;

import java.util.ArrayList;
import java.util.List;

public class ApproverRequest {
	
	private Long divId;
	private String requestor;
	public String getRequestor() {
		return requestor;
	}

	public void setRequestor(String requestor) {
		this.requestor = requestor;
	}

	public Long getDivId() {
		return divId;
	}

	public void setDivId(Long divId) {
		this.divId = divId;
	}

	private List<ApproverItem> approvers = new ArrayList<ApproverItem>();

	public List<ApproverItem> getApprovers() {
		return approvers;
	}

	public void setApprovers(List<ApproverItem> approvers) {
		this.approvers = approvers;
	}
	
}


