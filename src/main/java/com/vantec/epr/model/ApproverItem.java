package com.vantec.epr.model;

public class ApproverItem {
	
		private Long userId;
		private String level;
		public Long getUserId() {
			return userId;
		}
		public void setUserId(Long userId) {
			this.userId = userId;
		}
		public String getLevel() {
			return level;
		}
		public void setLevel(String level) {
			this.level = level;
		}
		

}
