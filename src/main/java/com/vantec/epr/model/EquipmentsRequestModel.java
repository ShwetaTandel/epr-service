package com.vantec.epr.model;

public class EquipmentsRequestModel {
	
	private Integer number_of_units;
	private String manufacturer;
	private String model_num;
	
	public Integer getNumber_of_units() {
		return number_of_units;
	}
	public void setNumber_of_units(Integer number_of_units) {
		this.number_of_units = number_of_units;
	}
	public String getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public String getModel_num() {
		return model_num;
	}
	public void setModel_num(String model_num) {
		this.model_num = model_num;
	}
	
	
	
	

}
