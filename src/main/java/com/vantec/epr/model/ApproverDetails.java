package com.vantec.epr.model;

import java.util.ArrayList;
import java.util.List;

public class ApproverDetails {
	
	private String level;
	private List<User> users = new ArrayList<User>();
	public List<User> getUsers() {
		return users;
	}
	public void setUsers(List<User> users) {
		this.users = users;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	


}


