package com.vantec.epr.model;


import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class TravelModeModel {
	
	private Long id;
	private String travelMode;
	private Date inDateTime;
	private Date outDateTime;
	private String inDateTimeString;
	private String outDateTimeString;

	private Float mileage;
	private String fromLocation;
	private String toLocation;
	private Boolean roundTrip;
	private Boolean holdBag;
	private Boolean handBag;
	private Boolean carPark;
	private String supplierName;
	private Float totalCharge;
	
	
	public String getInDateTimeString() {
		return inDateTimeString;
	}
	public void setInDateTimeString(String inDateTimeString) {
		this.inDateTimeString = inDateTimeString;
	}
	public String getOutDateTimeString() {
		return outDateTimeString;
	}
	public void setOutDateTimeString(String outDateTimeString) {
		this.outDateTimeString = outDateTimeString;
	}
	public String getSupplierName() {
		return supplierName;
	}
	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}
	public Float getTotalCharge() {
		return totalCharge;
	}
	public void setTotalCharge(Float totalCharge) {
		this.totalCharge = totalCharge;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTravelMode() {
		return travelMode;
	}
	public void setTravelMode(String travelMode) {
		this.travelMode = travelMode;
	}
	public Date getInDateTime() {
		return inDateTime;
	}
	public void setInDateTime(Date inDateTime) {
		this.inDateTime = inDateTime;
	}
	public Date getOutDateTime() {
		return outDateTime;
	}
	public void setOutDateTime(Date outDateTime) {
		this.outDateTime = outDateTime;
	}
	public Float getMileage() {
		return mileage;
	}
	public void setMileage(Float mileage) {
		this.mileage = mileage;
	}
	public String getFromLocation() {
		return fromLocation;
	}
	public void setFromLocation(String fromLocation) {
		this.fromLocation = fromLocation;
	}
	public String getToLocation() {
		return toLocation;
	}
	public void setToLocation(String toLocation) {
		this.toLocation = toLocation;
	}
	public Boolean getRoundTrip() {
		return roundTrip;
	}
	public void setRoundTrip(Boolean roundTrip) {
		this.roundTrip = roundTrip;
	}
	public Boolean getHoldBag() {
		return holdBag;
	}
	public void setHoldBag(Boolean holdBag) {
		this.holdBag = holdBag;
	}
	public Boolean getHandBag() {
		return handBag;
	}
	public void setHandBag(Boolean handBag) {
		this.handBag = handBag;
	}
	public Boolean getCarPark() {
		return carPark;
	}
	public void setCarPark(Boolean carPark) {
		this.carPark = carPark;
	}
	

}
