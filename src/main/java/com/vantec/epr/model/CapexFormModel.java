package com.vantec.epr.model;

import java.util.List;


public class CapexFormModel {
	
	public CapexFormModel() {
		super();
	}
	private Long capexId;
	private Long paymentTermsId;
	private Integer monthsDepreciation;
	private Boolean equipmentReplacement;
	private String purchaceType;
	private String descJustification;
	private String effectOnOps;
	private Long gmApproveId;
	private Long financeApproveId;
	private List<EquipmentsRequestModel> equipmentsRequest;
	private String status;
	private Long requestorId;
	private String comments;
	private String currentUserName;
	
	public String getCurrentUserName() {
		return currentUserName;
	}
	public void setCurrentUserName(String currentUserName) {
		this.currentUserName = currentUserName;
	}
	public String getComments() {
		return comments;
	}
	public Long getCapexId() {
		return capexId;
	}
	public void setCapexId(Long capexId) {
		this.capexId = capexId;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public Long getRequestorId() {
		return requestorId;
	}
	public void setRequestorId(Long requestorId) {
		this.requestorId = requestorId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Long getPaymentTermsId() {
		return paymentTermsId;
	}
	public void setPaymentTermsId(Long paymentTermsId) {
		this.paymentTermsId = paymentTermsId;
	}
	public Integer getMonthsDepreciation() {
		return monthsDepreciation;
	}
	public void setMonthsDepreciation(Integer monthsDepreciation) {
		this.monthsDepreciation = monthsDepreciation;
	}
	public Boolean getEquipmentReplacement() {
		return equipmentReplacement;
	}
	public void setEquipmentReplacement(Boolean equipmentReplacement) {
		this.equipmentReplacement = equipmentReplacement;
	}
	public String getPurchaceType() {
		return purchaceType;
	}
	public void setPurchaceType(String purchace_type) {
		this.purchaceType = purchace_type;
	}
	public String getDescJustification() {
		return descJustification;
	}
	public void setDescJustification(String descJustification) {
		this.descJustification = descJustification;
	}
	public String getEffectOnOps() {
		return effectOnOps;
	}
	public void setEffectOnOps(String effectOnOps) {
		this.effectOnOps = effectOnOps;
	}
	public Long getGmApproveId() {
		return gmApproveId;
	}
	public void setGmApproveId(Long gmApproveId) {
		this.gmApproveId = gmApproveId;
	}
	public Long getFinanceApproveId() {
		return financeApproveId;
	}
	public void setFinanceApproveId(Long financeApproveId) {
		this.financeApproveId = financeApproveId;
	}
	public List<EquipmentsRequestModel> getEquipmentsRequest() {
		return equipmentsRequest;
	}
	public void setEquipmentsRequest(List<EquipmentsRequestModel> equipmentsRequest) {
		this.equipmentsRequest = equipmentsRequest;
	}
	
	

}
