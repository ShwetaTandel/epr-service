package com.vantec.epr.model;


import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class PurchaseOrderRequest {
	
	private Long purchaseOrderId;
	private Long requestorId;
	private String title;
	private Date requestedDate;
	private Date deliveryDate;
	private Long requestorDeptId;
	private String purchaseType;
	private String projectName;
	private String reasonType;
	private String reasonDescription;
	private Long deptToChargeId;
	private String isBudgeted;
	private String nextLevel;
	private Boolean isAccrued;
	private Boolean isRecharge;
	private Boolean isEmergency;
	private String rechargeRef;
	private String rechargeTo;
	private String rechargeOthers;
	private String quoteSuppliedBy;
	private String preferred_supplier;
	private String reasonOfChoice;
	private String supplierContactDetails;
	private Boolean doaApprovalRequired;
	private Integer numberOfQuotes;
	private String specialRequirements;
	private String discountsAchieved;
	private Date contractToDate;
	private Date contractFromDate;
	private Float discount;
	private Float carrierCharges;
	private Float grand_total;
	private String deliveryAddress;
	private String status;
	private Long currApproverId;
	private Date approvalDate;
	private Date createAt;
	private String createdBy;
	private Date updatedAt;
	private String lastUpdatedBy;	
	private List<Item>items;
	private String action;
	private AdminDetails adminDetails;
	private Boolean isFreeOfCharge;
	private Boolean isGDPRFlagged;
	private Long budgetBodyId;
	
	public Date getApprovalDate() {
		return approvalDate;
	}
	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}
	public Boolean getIsGDPRFlagged() {
		return isGDPRFlagged;
	}
	public void setIsGDPRFlagged(Boolean isGDPRFlagged) {
		this.isGDPRFlagged = isGDPRFlagged;
	}
	public Boolean getIsFreeOfCharge() {
		return isFreeOfCharge;
	}
	public void setIsFreeOfCharge(Boolean isFreeofCharge) {
		this.isFreeOfCharge = isFreeofCharge;
	}
	public Boolean getIsAccrued() {
		return isAccrued;
	}
	public void setIsAccrued(Boolean isAccrued) {
		this.isAccrued = isAccrued;
	}
	public Boolean getIsEmergency() {
		return isEmergency;
	}
	public void setIsEmergency(Boolean isEmergency) {
		this.isEmergency = isEmergency;
	}
	public String getRechargeOthers() {
		return rechargeOthers;
	}
	public void setRechargeOthers(String rechargeOthers) {
		this.rechargeOthers = rechargeOthers;
	}
	public String getNextLevel() {
		return nextLevel;
	}
	public void setNextLevel(String nextLevel) {
		this.nextLevel = nextLevel;
	}
	public String getIsBudgeted() {
		return isBudgeted;
	}
	public void setIsBudgeted(String isBudgeted) {
		this.isBudgeted = isBudgeted;
	}

	public AdminDetails getAdminDetails() {
		return adminDetails;
	}
	public void setAdminDetails(AdminDetails adminDetails) {
		this.adminDetails = adminDetails;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public List<Item> getItems() {
		return items;
	}
	public void setItems(List<Item> items) {
		this.items = items;
	}
	public Date getContractFromDate() {
		return contractFromDate;
	}
	public void setContractFromDate(Date contractFromDate) {
		this.contractFromDate = contractFromDate;
	}
	public Long getPurchaseOrderId() {
		return purchaseOrderId;
	}
	public void setPurchaseOrderId(Long purchaseOrderId) {
		this.purchaseOrderId = purchaseOrderId;
	}
	public Long getRequestorId() {
		return requestorId;
	}
	public void setRequestorId(Long requestorId) {
		this.requestorId = requestorId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Date getRequestedDate() {
		return requestedDate;
	}
	public void setRequestedDate(Date requestedDate) {
		this.requestedDate = requestedDate;
	}
	public Date getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public Long getRequestorDeptId() {
		return requestorDeptId;
	}
	public void setRequestorDeptId(Long requestorDeptId) {
		this.requestorDeptId = requestorDeptId;
	}
	public String getPurchaseType() {
		return purchaseType;
	}
	public void setPurchaseType(String purchaseType) {
		this.purchaseType = purchaseType;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getReasonType() {
		return reasonType;
	}
	public void setReasonType(String reasonType) {
		this.reasonType = reasonType;
	}
	public String getReasonDescription() {
		return reasonDescription;
	}
	public void setReasonDescription(String reasonDescription) {
		this.reasonDescription = reasonDescription;
	}
	public Long getDeptToChargeId() {
		return deptToChargeId;
	}
	public void setDeptToChargeId(Long deptToChargeId) {
		this.deptToChargeId = deptToChargeId;
	}
	public Boolean getIsRecharge() {
		return isRecharge;
	}
	public void setIsRecharge(Boolean isRecharge) {
		this.isRecharge = isRecharge;
	}
	public String getRechargeRef() {
		return rechargeRef;
	}
	public void setRechargeRef(String rechargeRef) {
		this.rechargeRef = rechargeRef;
	}
	public String getRechargeTo() {
		return rechargeTo;
	}
	public void setRechargeTo(String rechargeTo) {
		this.rechargeTo = rechargeTo;
	}
	public String getQuoteSuppliedBy() {
		return quoteSuppliedBy;
	}
	public void setQuoteSuppliedBy(String quoteSuppliedBy) {
		this.quoteSuppliedBy = quoteSuppliedBy;
	}
	public String getPreferred_supplier() {
		return preferred_supplier;
	}
	public void setPreferred_supplier(String preferred_supplier) {
		this.preferred_supplier = preferred_supplier;
	}
	public String getReasonOfChoice() {
		return reasonOfChoice;
	}
	public void setReasonOfChoice(String reasonOfChoice) {
		this.reasonOfChoice = reasonOfChoice;
	}
	public String getSupplierContactDetails() {
		return supplierContactDetails;
	}
	public void setSupplierContactDetails(String supplierContactDetails) {
		this.supplierContactDetails = supplierContactDetails;
	}
	public Boolean getDoaApprovalRequired() {
		return doaApprovalRequired;
	}
	public void setDoaApprovalRequired(Boolean doaApprovalRequired) {
		this.doaApprovalRequired = doaApprovalRequired;
	}
	public Integer getNumberOfQuotes() {
		return numberOfQuotes;
	}
	public void setNumberOfQuotes(Integer numberOfQuotes) {
		this.numberOfQuotes = numberOfQuotes;
	}
	public String getSpecialRequirements() {
		return specialRequirements;
	}
	public void setSpecialRequirements(String specialRequirements) {
		this.specialRequirements = specialRequirements;
	}
	public String getDiscountsAchieved() {
		return discountsAchieved;
	}
	public void setDiscountsAchieved(String discountsAchieved) {
		this.discountsAchieved = discountsAchieved;
	}
	public Date getContractToDate() {
		return contractToDate;
	}
	public void setContractToDate(Date contractToDate) {
		this.contractToDate = contractToDate;
	}
	public Float getDiscount() {
		return discount;
	}
	public void setDiscount(Float discount) {
		this.discount = discount;
	}
	public Float getCarrierCharges() {
		return carrierCharges;
	}
	public void setCarrierCharges(Float carrierCharges) {
		this.carrierCharges = carrierCharges;
	}
	public Float getGrand_total() {
		return grand_total;
	}
	public void setGrand_total(Float grand_total) {
		this.grand_total = grand_total;
	}
	public String getDeliveryAddress() {
		return deliveryAddress;
	}
	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Long getCurrApproverId() {
		return currApproverId;
	}
	public void setCurrApproverId(Long currApproverId) {
		this.currApproverId = currApproverId;
	}
	public Date getCreateAt() {
		return createAt;
	}
	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public Long getBudgetBodyId() {
		return budgetBodyId;
	}
	public void setBudgetBodyId(Long budgetBodyId) {
		this.budgetBodyId = budgetBodyId;
	}
    
}
