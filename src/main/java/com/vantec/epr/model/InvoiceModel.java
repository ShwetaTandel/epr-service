package com.vantec.epr.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class InvoiceModel {
	
	private Long id;
	private Long purchaseOrderId;
	private Long requestorId;
	private String invoiceNumber;
	private Date invoiceDate;
	private String invoiceDateString;
	private String invoiceDocument;
	private Float invoiceAmount;
	private Float shippingCost;
	private Boolean isFinal;
	private List<DepartmentToCharge> deptToChargeList = new ArrayList<DepartmentToCharge>();
	
	public List<DepartmentToCharge> getDeptToChargeList() {
		return deptToChargeList;
	}
	public void setDeptToChargeList(List<DepartmentToCharge> deptToChargeList) {
		this.deptToChargeList = deptToChargeList;
	}
	public Boolean getIsFinal() {
		return isFinal;
	}
	public void setIsFinal(Boolean isFinal) {
		this.isFinal = isFinal;
	}
	public String getInvoiceDocument() {
		return invoiceDocument;
	}
	public void setInvoiceDocument(String invoiceDocument) {
		this.invoiceDocument = invoiceDocument;
	}
	public Float getShippingCost() {
		return shippingCost;
	}
	public void setShippingCost(Float shippingCost) {
		this.shippingCost = shippingCost;
	}
	private List<Item> invoicedItems = new ArrayList<Item>();
	public List<Item> getInvoicedItems() {
		return invoicedItems;
	}
	public void setInvoicedItems(List<Item> invoicedItems) {
		this.invoicedItems = invoicedItems;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getPurchaseOrderId() {
		return purchaseOrderId;
	}
	public void setPurchaseOrderId(Long purchaseOrderId) {
		this.purchaseOrderId = purchaseOrderId;
	}
	public Long getRequestorId() {
		return requestorId;
	}
	public void setRequestorId(Long requestorId) {
		this.requestorId = requestorId;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	
	public Date getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public String getInvoiceDateString() {
		return invoiceDateString;
	}
	public void setInvoiceDateString(String invoiceDateString) {
		this.invoiceDateString = invoiceDateString;
	}
	public Float getInvoiceAmount() {
		return invoiceAmount;
	}
	public void setInvoiceAmount(Float invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}
	

}
