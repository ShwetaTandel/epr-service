package com.vantec.epr.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class BudgetRequest {
	
	
	
	private String fy;
	private String deptName;
	
	private String item;
	private String categoryName;
	private String costCentreName;
	private String sageCode;
	private Double planned;
	private Double spend;
	private Double accrued;
	
	private String year;
	private String month;
	private String conMonth;
	private String[] months;
	private String[] consecutiveMonths;
	private Double[] monthsValue;
	private String[] monthsSelected;
	private Integer noOfMonths;
	private Double monthlyValue;
	private Double bodyMonthlyValue;
	private Integer noOfMonthsForThatYear;
	
	private String paymentFrequency;
	private Double unitPrice;
	private Integer noOfItems;
	private String createdBy;
	private Long userId;
	private Long budgetBodyId;
	
	
	
	public Long getBudgetBodyId() {
		return budgetBodyId;
	}
	public void setBudgetBodyId(Long budgetBodyId) {
		this.budgetBodyId = budgetBodyId;
	}
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public Double getPlanned() {
		return planned;
	}
	public void setPlanned(Double planned) {
		this.planned = planned;
	}
	public Double getSpend() {
		return spend;
	}
	public void setSpend(Double spend) {
		this.spend = spend;
	}
	public Double getAccrued() {
		return accrued;
	}
	public void setAccrued(Double accrued) {
		this.accrued = accrued;
	}
	public String getCostCentreName() {
		return costCentreName;
	}
	public void setCostCentreName(String costCentreName) {
		this.costCentreName = costCentreName;
	}
	public String getSageCode() {
		return sageCode;
	}
	public void setSageCode(String sageCode) {
		this.sageCode = sageCode;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getFy() {
		return fy;
	}
	public void setFy(String fy) {
		this.fy = fy;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String[] getMonths() {
		return months;
	}
	public void setMonths(String[] months) {
		this.months = months;
	}
	public Integer getNoOfMonths() {
		return noOfMonths;
	}
	public void setNoOfMonths(Integer noOfMonths) {
		this.noOfMonths = noOfMonths;
	}
	public Double getMonthlyValue() {
		return monthlyValue;
	}
	public void setMonthlyValue(Double monthlyValue) {
		this.monthlyValue = monthlyValue;
	}
	public Double[] getMonthsValue() {
		return monthsValue;
	}
	public void setMonthsValue(Double[] monthsValue) {
		this.monthsValue = monthsValue;
	}
	public String[] getMonthsSelected() {
		return monthsSelected;
	}
	public void setMonthsSelected(String[] monthsSelected) {
		this.monthsSelected = monthsSelected;
	}
	public String[] getConsecutiveMonths() {
		return consecutiveMonths;
	}
	public void setConsecutiveMonths(String[] consecutiveMonths) {
		this.consecutiveMonths = consecutiveMonths;
	}
	public String getDeptName() {
		return deptName;
	}
	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getConMonth() {
		return conMonth;
	}
	public void setConMonth(String conMonth) {
		this.conMonth = conMonth;
	}
	public Double getBodyMonthlyValue() {
		return bodyMonthlyValue;
	}
	public void setBodyMonthlyValue(Double bodyMonthlyValue) {
		this.bodyMonthlyValue = bodyMonthlyValue;
	}
	public Integer getNoOfMonthsForThatYear() {
		return noOfMonthsForThatYear;
	}
	public void setNoOfMonthsForThatYear(Integer noOfMonthsForThatYear) {
		this.noOfMonthsForThatYear = noOfMonthsForThatYear;
	}
	public String getPaymentFrequency() {
		return paymentFrequency;
	}
	public void setPaymentFrequency(String paymentFrequency) {
		this.paymentFrequency = paymentFrequency;
	}
	public Double getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}
	public Integer getNoOfItems() {
		return noOfItems;
	}
	public void setNoOfItems(Integer noOfItems) {
		this.noOfItems = noOfItems;
	}
	
	
	


}
