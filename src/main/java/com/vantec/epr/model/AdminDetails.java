package com.vantec.epr.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AdminDetails {
	
	private long adminDetailId;
	public long getAdminDetailId() {
		return adminDetailId;
	}
	public void setAdminDetailId(long adminDetailId) {
		this.adminDetailId = adminDetailId;
	}
	private String handledBy;
	private String alreadyPurchased;
	private Date orderPlacedDate;
	private String supplierNameAndCode;
	private String paymentTerms;
	private String paymentType;
	private String processFollowed;
	private String expenseTypeAndCode;
	private Float savings;
	private Boolean isCompanyAsset;
	private Boolean capexCompelete;
	private List<DepartmentToCharge> deptToChargeList = new ArrayList<DepartmentToCharge>();
	private String purchasingComments;
	private String financeNotes;
	private String isProcessFollowed;
	private String comments;
	
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getPaymentTerms() {
		return paymentTerms;
	}
	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public String getIsProcessFollowed() {
		return isProcessFollowed;
	}
	public void setIsProcessFollowed(String isProcessFollowed) {
		this.isProcessFollowed = isProcessFollowed;
	}
	public String getHandledBy() {
		return handledBy;
	}
	public void setHandledBy(String handledBy) {
		this.handledBy = handledBy;
	}
	public String getAlreadyPurchased() {
		return alreadyPurchased;
	}
	public void setAlreadyPurchased(String alreadyPurchased) {
		this.alreadyPurchased = alreadyPurchased;
	}
	public Date getOrderPlacedDate() {
		return orderPlacedDate;
	}
	public void setOrderPlacedDate(Date orderPlacesDate) {
		this.orderPlacedDate = orderPlacesDate;
	}
	public String getSupplierNameAndCode() {
		return supplierNameAndCode;
	}
	public void setSupplierNameAndCode(String supplierNameAndCode) {
		this.supplierNameAndCode = supplierNameAndCode;
	}
	public String getProcessFollowed() {
		return processFollowed;
	}
	public void setProcessFollowed(String processFollowed) {
		this.processFollowed = processFollowed;
	}
	public String getExpenseTypeAndCode() {
		return expenseTypeAndCode;
	}
	public void setExpenseTypeAndCode(String expenseTypeAndCode) {
		this.expenseTypeAndCode = expenseTypeAndCode;
	}
	public Float getSavings() {
		return savings;
	}
	public void setSavings(Float savings) {
		this.savings = savings;
	}
	public Boolean getIsCompanyAsset() {
		return isCompanyAsset;
	}
	public void setIsCompanyAsset(Boolean isCompanyAsset) {
		this.isCompanyAsset = isCompanyAsset;
	}
	public Boolean getCapexCompelete() {
		return capexCompelete;
	}
	public void setCapexCompelete(Boolean capexCompelete) {
		this.capexCompelete = capexCompelete;
	}
	public List<DepartmentToCharge> getDeptToChargeList() {
		return deptToChargeList;
	}
	public void setDeptToChargeList(List<DepartmentToCharge> deptToChargeList) {
		this.deptToChargeList = deptToChargeList;
	}
	public String getPurchasingComments() {
		return purchasingComments;
	}
	public void setPurchasingComments(String purchasingComments) {
		this.purchasingComments = purchasingComments;
	}
	public String getFinanceNotes() {
		return financeNotes;
	}
	public void setFinanceNotes(String financeNotes) {
		this.financeNotes = financeNotes;
	}
			
	
	

}
