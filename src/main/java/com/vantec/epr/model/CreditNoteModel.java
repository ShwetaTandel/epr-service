package com.vantec.epr.model;

import java.util.Date;

public class CreditNoteModel {
	
	private Long id;
	private Long invoiceId;
	private Long purchaseOrderId;
	private Long requestorId;
	private String creditNoteNumber;
	private String creditNoteDesc;
	private Date creditNoteDate;
	private String creditNoteDateString;
	private String creditNoteDocument;
	private Float creditAmount;
	private Boolean isFinal;
	
	public Boolean getIsFinal() {
		return isFinal;
	}

	public void setIsFinal(Boolean isFinal) {
		this.isFinal = isFinal;
	}

	public String getCreditNoteDesc() {
		return creditNoteDesc;
	}

	public void setCreditNoteDesc(String creditNoteDesc) {
		this.creditNoteDesc = creditNoteDesc;
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	public Long getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(Long invoiceId) {
		this.invoiceId = invoiceId;
	}
	public Long getPurchaseOrderId() {
		return purchaseOrderId;
	}
	public void setPurchaseOrderId(Long purchaseOrderId) {
		this.purchaseOrderId = purchaseOrderId;
	}
	public Long getRequestorId() {
		return requestorId;
	}
	public void setRequestorId(Long requestorId) {
		this.requestorId = requestorId;
	}
	public String getCreditNoteNumber() {
		return creditNoteNumber;
	}
	public void setCreditNoteNumber(String creditNoteNumber) {
		this.creditNoteNumber = creditNoteNumber;
	}
	public Date getCreditNoteDate() {
		return creditNoteDate;
	}
	public void setCreditNoteDate(Date creditNoteDate) {
		this.creditNoteDate = creditNoteDate;
	}
	public String getCreditNoteDateString() {
		return creditNoteDateString;
	}
	public void setCreditNoteDateString(String creditNoteDateString) {
		this.creditNoteDateString = creditNoteDateString;
	}
	public String getCreditNoteDocument() {
		return creditNoteDocument;
	}
	public void setCreditNoteDocument(String creditNoteDocument) {
		this.creditNoteDocument = creditNoteDocument;
	}
	public Float getCreditAmount() {
		return creditAmount;
	}
	public void setCreditAmount(Float creditAmount) {
		this.creditAmount = creditAmount;
	}
	
		
	

}
