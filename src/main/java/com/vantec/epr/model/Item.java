package com.vantec.epr.model;

public class Item {
	
	private long itemId;
	private Integer quantity;
	private Float price;
	private Float total;
	private Integer outstandingQty;
	private String item;
	private String deptToCharge = "";
	private String expenseCode = "";
	private Boolean isAsset;
	private Long capexId;
	
	public Long getCapexId() {
		return capexId;
	}
	public void setCapexId(Long capexId) {
		this.capexId = capexId;
	}
	private CapexFormModel capexDetails;

	public CapexFormModel getCapexDetails() {
		return capexDetails;
	}
	public void setCapexDetails(CapexFormModel capexDetails) {
		this.capexDetails = capexDetails;
	}
	public Boolean getIsAsset() {
		return isAsset;
	}
	public void setIsAsset(Boolean isAsset) {
		this.isAsset = isAsset;
	}
	public String getDeptToCharge() {
		return deptToCharge;
	}
	public void setDeptToCharge(String deptToCharge) {
		this.deptToCharge = deptToCharge;
	}
	public String getExpenseCode() {
		return expenseCode;
	}
	public void setExpenseCode(String expenseCode) {
		this.expenseCode = expenseCode;
	}
	public Integer getOutstandingQty() {
		return outstandingQty;
	}
	public void setOutstandingQty(Integer outstandingQty) {
		this.outstandingQty = outstandingQty;
	}
	public long getItemId() {
		return itemId;
	}
	public void setItemId(long itemId) {
		this.itemId = itemId;
	}
	
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer qty) {
		this.quantity = qty;
	}
	public Float getPrice() {
		return price;
	}
	public void setPrice(Float price) {
		this.price = price;
	}
	public Float getTotal() {
		return total;
	}
	public void setTotal(Float total) {
		this.total = total;
	}
}
