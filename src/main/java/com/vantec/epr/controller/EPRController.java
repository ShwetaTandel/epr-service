package com.vantec.epr.controller;

import java.text.ParseException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vantec.epr.model.ApproverRequest;
import com.vantec.epr.model.ApproversResponse;
import com.vantec.epr.model.CapexFormModel;
import com.vantec.epr.model.CapexResponse;
import com.vantec.epr.model.CreditNoteModel;
import com.vantec.epr.model.DeliveryNoteModel;
import com.vantec.epr.model.DivisionResponse;
import com.vantec.epr.model.GetOrderListRequest;
import com.vantec.epr.model.InvoiceModel;
import com.vantec.epr.model.PurchaseOrderRequest;
import com.vantec.epr.model.PurchaseOrderResponse;
import com.vantec.epr.model.QuotesRequest;
import com.vantec.epr.model.SaveDivDeptRequest;
import com.vantec.epr.model.SupplierModel;
import com.vantec.epr.model.TravelRequestModel;
import com.vantec.epr.model.UpdateStatusRequest;
import com.vantec.epr.model.UserRequest;
import com.vantec.epr.service.EPRService;

@RestController
@RequestMapping("/")
public class EPRController {
	
	
	@Autowired
	EPRService eprService;
	private static Logger logger = LoggerFactory.getLogger(EPRController.class);
    @RequestMapping(value = "/testService", method = RequestMethod.GET)
	public ResponseEntity<String> testService() throws ParseException {
			return new ResponseEntity<String>(eprService.testService(), HttpStatus.OK);
	}
    
    @RequestMapping(value = "/savePurchaseOrder", method = RequestMethod.POST)
    public ResponseEntity<String> savePurchaseOrder(@RequestBody PurchaseOrderRequest purchaseOrderReq) throws ParseException {
    	logger.info("savePurchaseOrder called..");
		return new ResponseEntity<String>(eprService.savePurchaseOrder(purchaseOrderReq), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/addPurchaseOrderQuotes", method = RequestMethod.POST)
    public ResponseEntity<String> addPurchaseOrderQuotes(@RequestBody QuotesRequest quotesReq) throws ParseException {
    	logger.info("addPurchaseOrderQuotes called..");
		return new ResponseEntity<String>(eprService.addPurchaseOrderQuotes(quotesReq), HttpStatus.OK);
    }
    @RequestMapping(value = "/getPurchaseOrder", method = RequestMethod.GET)
    public ResponseEntity<PurchaseOrderResponse> getPurchaseOrder(@RequestParam Long orderId) throws ParseException {
    	logger.info("getPurchaseOrder called..");
    	logger.info("getPurchaseOrder called for orderID"+ orderId);
		return new ResponseEntity<PurchaseOrderResponse>(eprService.getPurchaseOrder(orderId), HttpStatus.OK);
    }

    @RequestMapping(value = "/getApprovers", method = RequestMethod.GET)
    public ResponseEntity<ApproversResponse> getApprovers(@RequestParam Long divId,@RequestParam String currStatus, @RequestParam Long requestorId, Float amount) throws ParseException {
    	logger.info("getApprovers called..");
		return new ResponseEntity<ApproversResponse>(eprService.getApprovers(divId,currStatus,requestorId,amount), HttpStatus.OK);
    }
    @RequestMapping(value = "/updatePurchaseOrderStatus", method = RequestMethod.POST)
    public ResponseEntity<String> updatePurchaseOrderStatus(@RequestBody UpdateStatusRequest statusReq) throws ParseException {
    	logger.info("updatePurchaseOrderStatus called..");
		return new ResponseEntity<String>(eprService.updatePurchaseOrderStatus(statusReq), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/getPurchaseOrderList", method = RequestMethod.POST)
    public ResponseEntity<List<PurchaseOrderResponse>> getPurchaseOrderList(@RequestBody GetOrderListRequest orderListReq) throws ParseException {
    	logger.info("getPurchaseOrderList called..");
		return new ResponseEntity<List<PurchaseOrderResponse>>(eprService.getPurchaseOrderList(orderListReq), HttpStatus.OK);
    }

    @RequestMapping(value = "/getDivisions", method = RequestMethod.GET)
    public ResponseEntity<List<DivisionResponse>> getDivisions() throws ParseException {
    	logger.info("getDivisions called..");
		return new ResponseEntity<List<DivisionResponse>>(eprService.getDivisions(), HttpStatus.OK);
    }

    @RequestMapping(value = "/saveUser", method = RequestMethod.POST)
    public ResponseEntity<String> saveUser(@RequestBody UserRequest userReq) throws ParseException {
    	logger.info("saveUser called..");
		return new ResponseEntity<String>(eprService.saveUser(userReq), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/saveDivision", method = RequestMethod.POST)
    public ResponseEntity<String> saveDivision(@RequestBody SaveDivDeptRequest request) throws ParseException {
    	logger.info("saveDivision called..");
		return new ResponseEntity<String>(eprService.saveDivision(request), HttpStatus.OK);
    }

    
    @RequestMapping(value = "/saveDepartment", method = RequestMethod.POST)
    public ResponseEntity<String> saveDepartment(@RequestBody SaveDivDeptRequest request) throws ParseException {
    	logger.info("saveDepartment called..");
		return new ResponseEntity<String>(eprService.saveDepartment(request), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/addDeliveryNote", method = RequestMethod.POST)
    public ResponseEntity<String> addDeliveryNote (@RequestBody DeliveryNoteModel deliveryNoteReq) throws ParseException {
    	logger.info("addDeliveryNote called..");
		return new ResponseEntity<String>(eprService.addDeliveryNote(deliveryNoteReq), HttpStatus.OK);
    }

    @RequestMapping(value = "/addInvoice", method = RequestMethod.POST)
    public ResponseEntity<String> addInvoice (@RequestBody InvoiceModel invoiceReq) throws ParseException {
    	logger.info("addInvoice called..");
		return new ResponseEntity<String>(eprService.addInvoice(invoiceReq), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/deleteInvoice", method = RequestMethod.POST)
    public ResponseEntity<String> deleteInvoice (@RequestBody InvoiceModel invoiceReq) throws ParseException {
    	logger.info("deleteInvoice called..");
		return new ResponseEntity<String>(eprService.deleteInvoice(invoiceReq), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/addCreditNote", method = RequestMethod.POST)
    public ResponseEntity<String> addCreditNote (@RequestBody CreditNoteModel creditReq) throws ParseException {
    	logger.info("addCreditNote called..");
		return new ResponseEntity<String>(eprService.addCreditNote(creditReq), HttpStatus.OK);
    }

    
    @RequestMapping(value = "/finalizeOrder", method = RequestMethod.POST)
    public ResponseEntity<String> finalizeOrder (@RequestBody InvoiceModel invoiceReq) throws ParseException {
    	logger.info("finalizeOrder called..");
		return new ResponseEntity<String>(eprService.finalizeOrder(invoiceReq), HttpStatus.OK);
    }

    @RequestMapping(value = "/addApprovers", method = RequestMethod.POST)
    public ResponseEntity<String> addApprovers (@RequestBody ApproverRequest request) throws ParseException {
    	logger.info("addApprovers called..");
		return new ResponseEntity<String>(eprService.addApprovers(request), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/saveSupplier", method = RequestMethod.POST)
    public ResponseEntity<String> saveSupplier (@RequestBody SupplierModel request) throws ParseException {
    	logger.info("saveSupplier called..");
		return new ResponseEntity<String>(eprService.saveSupplier(request), HttpStatus.OK);
    }

    @RequestMapping(value = "/saveTravelRequest", method = RequestMethod.POST)
    public ResponseEntity<String> saveTravelRequest (@RequestBody TravelRequestModel request) throws ParseException {
    	logger.info("saveTravelRequest called..");
		return new ResponseEntity<String>(eprService.saveTravelRequest(request), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/getTravelRequest", method = RequestMethod.GET)
    public ResponseEntity<TravelRequestModel> getTravelRequest(@RequestParam Long travelId) throws ParseException {
    	logger.info("getTravelRequest called..");
		return new ResponseEntity<TravelRequestModel>(eprService.getTravelRequest(travelId), HttpStatus.OK);
    }

    @RequestMapping(value = "/updateTravelRequestStatus", method = RequestMethod.POST)
    public ResponseEntity<String> updateTravelRequestStatus(@RequestBody UpdateStatusRequest statusReq) throws ParseException {
    	logger.info("updateTravelRequestStatus called..");
		return new ResponseEntity<String>(eprService.updateTravelRequestStatus(statusReq), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/updateExpenseCodes", method = RequestMethod.GET)
    public ResponseEntity<String> updateExpenseCodes() throws ParseException {
    	logger.info("updateExpenseCodes called..");
		return new ResponseEntity<String>(eprService.updateExpenseCodes(), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/updateCapex", method = RequestMethod.POST)
    public ResponseEntity<String> saveCapexForm(@RequestBody CapexFormModel capexFormModel) throws ParseException {
    	logger.info("updateCapex called..");
    	String response = eprService.updateCapexForm(capexFormModel);
    	
    	
    	return new ResponseEntity<String>(response, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/getCapexRequests", method = RequestMethod.GET)
    public ResponseEntity<List<CapexResponse>> getCapexRequests(@RequestParam long userId) throws ParseException {
    	logger.info("getCapexRequests called..");
		return new ResponseEntity<List<CapexResponse>>(eprService.getCapexRequests(userId), HttpStatus.OK);
    }  
    
    @RequestMapping(value = "/getAllCapexRequests", method = RequestMethod.GET)
    public ResponseEntity<List<CapexResponse>> getAllCapexRequests(@RequestParam String status) throws ParseException {
    	logger.info("getAllCapexRequests called..");
		return new ResponseEntity<List<CapexResponse>>(eprService.getAllCapexRequests(status), HttpStatus.OK);
    }

}
