package com.vantec.epr.controller;

import java.text.ParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.vantec.epr.model.BudgetRequest;
import com.vantec.epr.service.NewBudgetService;

@RestController
@RequestMapping("/")
public class BudgetController {
	
	
//	@Autowired
//	BudgetService budgetService;
	@Autowired
	NewBudgetService budgetService;
	
	private static Logger logger = LoggerFactory.getLogger(BudgetController.class);
	
//    @RequestMapping(value = "/saveBudget", method = RequestMethod.POST)
//    public ResponseEntity<String> saveBudget(@RequestBody BudgetRequest[] budgetRequest) throws ParseException {
//    	logger.info("saveBudget called......");
//		return new ResponseEntity<String>(budgetService.saveBudget(budgetRequest), HttpStatus.OK);
//    }
	
	
	@RequestMapping(value = "/saveBudget", method = RequestMethod.POST)
    public ResponseEntity<String> saveBudget(@RequestBody BudgetRequest[] budgetRequest) throws ParseException {
    	logger.info("saveBudget called......");
		return new ResponseEntity<String>(budgetService.saveBudgets(budgetRequest), HttpStatus.OK);
    }
    
    @RequestMapping(value = "/deleteBudget", method = RequestMethod.POST)
    public ResponseEntity<String> deleteBudget(@RequestBody Long[] budgetbodyIds) throws ParseException {
    	logger.info("deleteBudget called......");
		return new ResponseEntity<String>(budgetService.deleteBudgets(budgetbodyIds), HttpStatus.OK);
    }
    
//    @RequestMapping(value = "/updateDetail", method = RequestMethod.GET)
//   	public ResponseEntity<String> testService() throws ParseException {
//   			return new ResponseEntity<String>(budgetService.updateDetail(), HttpStatus.OK);
//   	}
    
}
