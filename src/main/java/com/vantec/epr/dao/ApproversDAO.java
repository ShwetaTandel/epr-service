package com.vantec.epr.dao;

import java.util.List;

import org.springframework.stereotype.Component;


@Component
public interface ApproversDAO {
	
	public String findDistinctNextLevel(long divId, String level);
	public List<String> findStage1NextLevelsForDivAndAmount(long divId, String level, float amount);
	public List<String> findStage1NextLevelsForDiv(long divId, String level);
	public List<String> findStage1LevelsForDiv(long divId, String level);
	public List<String> findStage2NextLevelsForDiv(long divId, String level);
	public List<String> findAllStage2NextLevelsForDiv(long divId, String level);
	public List<String> findStage2LevelsForDiv(long divId, String level);
	public String findPurchasingFirstLevel(long divId);
	public String findStage1FirstLevel(long divId);
	public List<Object[]> getApprovers(long divId, List<String> levels,long reqUserId);
	public String getApproverLevelStage1(long reqUserId,long divId);
	public String getApproverLevelStage2(long reqUserId);
	public Object[] getPreviousApproverLevel(long purchaseOrderId,String level);

}
