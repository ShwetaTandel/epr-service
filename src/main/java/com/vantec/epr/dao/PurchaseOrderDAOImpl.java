package com.vantec.epr.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.vantec.epr.entity.PurchaseOrder;
import com.vantec.epr.util.PurchaseOrderActionEnum;
import com.vantec.epr.util.PurchaseOrderStatusEnum;

@Transactional
@Repository
public class PurchaseOrderDAOImpl implements PurchaseOrderDAO {

	private static Logger LOGGER = LoggerFactory.getLogger(PurchaseOrderDAOImpl.class);
	/// THE Flow goes as below - 
	//L(x)_NEW -> L(x+1)_PENDING -> L(x+1)_REJECTED/APPROVED ->  ... -> P1_PENDING -> P1_APPROVED/REJECTED -> P2_PENDING -> P2_APPROVED/REJECTED -> FINAL
	@PersistenceContext
	private EntityManager entityManager;
	public String findStatusByID(long id){
		PurchaseOrder po = entityManager.find(PurchaseOrder.class, id);
		return po.getStatus();
	}

	@Override
	public void createPurchaseOrder(PurchaseOrder purchaseOrder) {
		entityManager.persist(purchaseOrder);
	}
	
	@Override
	public Boolean updatePurchaseOrderStatus(long purchaseOrderId ,String status) {
		PurchaseOrder purchaseOrder = new PurchaseOrder();
		purchaseOrder.setPurchaseOrderId(purchaseOrderId);
		purchaseOrder.setStatus(status);
		purchaseOrder.setLastUpdatedBy("auto-code");
		entityManager.persist(purchaseOrder);

		return true;
	}
	
	@Override
	public String findNextStatusForPurchaseOrder(String currStatus, Float orderAmount, Long divisionId,PurchaseOrderActionEnum action){

		String newStatus = "";
		int iend = currStatus.indexOf("_"); 
		String level = "";
		if (iend != -1) 
		{
			level = currStatus.substring(0 , iend); 
		}
		String sql , results = "";
			sql =   "SELECT distinct(level) FROM approvers where is_active = true and division_id = :divisionId and level > :level order by level asc Limit 1";
			results = (String)entityManager.createNativeQuery(sql)
	                .setParameter("divisionId", divisionId)
	                .setParameter("level", level).getSingleResult();
		switch (action) {
		// Find next Level Approver and move to pending
		case NEW: newStatus 	  = results + PurchaseOrderStatusEnum._PENDING; break;
		//Send back to same approver for approval
		case EDIT: newStatus 	  = level + PurchaseOrderStatusEnum._PENDING; break;
		//Curr Level and move to approved
		case APPROVE: newStatus = level + PurchaseOrderStatusEnum._APPROVED;break;
		//Move to next level with pending status
		case MOVELEVELUP: newStatus = results + PurchaseOrderStatusEnum._PENDING;break;
		//Reject in the same level
		case REJECT: newStatus  = level + PurchaseOrderStatusEnum._REJECTED;break;
		//Close in the same level
		case CLOSE: newStatus   = level + PurchaseOrderStatusEnum._CLOSED;break;
		default:
			break;
		}
		System.out.println("curr status:"+currStatus + " Order Amount : " + orderAmount + "  div id " + divisionId + " actiiomn " + action.name() + " new status " +newStatus);
		return newStatus;
	}

}
