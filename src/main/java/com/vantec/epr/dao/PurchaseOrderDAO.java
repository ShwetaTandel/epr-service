package com.vantec.epr.dao;

import org.springframework.stereotype.Component;

import com.vantec.epr.entity.PurchaseOrder;
import com.vantec.epr.util.PurchaseOrderActionEnum;


@Component
public interface PurchaseOrderDAO {
	
	public Boolean updatePurchaseOrderStatus(long purchaseOrderId, String status);

	public void createPurchaseOrder(PurchaseOrder purchaseOrder);
	
	public String findNextStatusForPurchaseOrder(String currStatus, Float orderAmount, Long divisionId,PurchaseOrderActionEnum action);

}
