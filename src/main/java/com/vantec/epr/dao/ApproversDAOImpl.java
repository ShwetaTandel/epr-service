package com.vantec.epr.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.vantec.epr.entity.PurchaseOrder;

@Transactional
@Repository
public class ApproversDAOImpl implements ApproversDAO {

	private static Logger LOGGER = LoggerFactory.getLogger(ApproversDAOImpl.class);

	@PersistenceContext
	private EntityManager entityManager;
	public String findStatusByID(long id){
		PurchaseOrder po = entityManager.find(PurchaseOrder.class, id);
		return po.getStatus();
	}

	@Override
	public List<String> findStage1NextLevelsForDivAndAmount(long divId, String level, float amount){
		
		String sql =   "SELECT distinct(level) FROM approvers where is_active = true and division_id = :divisionId and level > :level and level like 'L%' and max_amount >=:amount order by level asc ";
		@SuppressWarnings("unchecked")
		List<String> results = (List<String>) entityManager.createNativeQuery(sql)
                  .setParameter("divisionId", divId)
                  .setParameter("level", level)
                  .setParameter("amount", amount).getResultList();
		return results;
	}
	
	@Override
	public List<String> findStage1NextLevelsForDiv(long divId, String level){
		
		String sql =   "SELECT distinct(level) FROM approvers where is_active = true and division_id = :divisionId and level > :level and level like 'L%' order by level asc ";
		@SuppressWarnings("unchecked")
		List<String> results = (List<String>) entityManager.createNativeQuery(sql)
                  .setParameter("divisionId", divId)
                  .setParameter("level", level)
                  .getResultList();
		return results;
	}
	@Override
	public List<String> findStage1LevelsForDiv(long divId, String level){
		
		String sql =   "SELECT distinct(level) FROM approvers where is_active = true and division_id = :divisionId and level >= :level and level like 'L%' order by level asc ";
		@SuppressWarnings("unchecked")
		List<String> results = (List<String>) entityManager.createNativeQuery(sql)
                  .setParameter("divisionId", divId)
                  .setParameter("level", level)
                  .getResultList();
		return results;
	}


	@Override
	public List<String> findStage2NextLevelsForDiv(long divId, String level){
		
		String sql =   "SELECT distinct(level) FROM approvers where is_active = true and division_id = :divisionId and level > :level order by level asc limit 1";
		@SuppressWarnings("unchecked")
		List<String> results = (List<String>) entityManager.createNativeQuery(sql)
                  .setParameter("divisionId", divId)
                  .setParameter("level", level)
                  .getResultList();
		return results;
	}
	
	@Override
	public List<String> findAllStage2NextLevelsForDiv(long divId, String level){
		
		String sql =   "SELECT distinct(level) FROM approvers where is_active = true and division_id = :divisionId and level > :level order by level asc";
		@SuppressWarnings("unchecked")
		List<String> results = (List<String>) entityManager.createNativeQuery(sql)
                  .setParameter("divisionId", divId)
                  .setParameter("level", level)
                  .getResultList();
		return results;
	}

	
	@Override
	public List<String> findStage2LevelsForDiv(long divId, String level){
		
		String sql =   "SELECT distinct(level) FROM approvers where is_active = true and division_id = :divisionId and level >= :level order by level asc limit 1";
		@SuppressWarnings("unchecked")
		List<String> results = (List<String>) entityManager.createNativeQuery(sql)
                  .setParameter("divisionId", divId)
                  .setParameter("level", level)
                  .getResultList();
		return results;
	}

	
	@Override
	public String findDistinctNextLevel(long divId, String level){
		
		String sql =   "SELECT distinct(level) FROM approvers where is_active = true and division_id = :divisionId and level > :level order by level asc Limit 1";
		String results = (String)entityManager.createNativeQuery(sql)
                  .setParameter("divisionId", divId)
                  .setParameter("level", level).getSingleResult();
		return results;
	}

	@Override
	public String findPurchasingFirstLevel(long divId){
		
		String sql =   "SELECT distinct(level) FROM approvers where is_active = true and division_id = :divisionId and level = 'P1' order by level asc Limit 1";
		String results = (String)entityManager.createNativeQuery(sql)
                  .setParameter("divisionId", divId)
                  .getSingleResult();
		return results;
	}
	
	@Override
	public String findStage1FirstLevel(long divId){
		
		String sql =   "SELECT distinct(level) FROM approvers where is_active = true and division_id = :divisionId and level like 'L%' order by level asc Limit 1";
		String results = (String)entityManager.createNativeQuery(sql)
                  .setParameter("divisionId", divId)
                  .getSingleResult();
		return results;
	}

	@Override
	public List<Object[]> getApprovers(long divId, List<String> levels, long reqUserId){
		String sql =   "SELECT users.id,first_name, last_name,level FROM approvers,users where approvers.user_id = users.id and  level in :levels and division_id=:divisionId and approvers.user_id!=:reqUserId order by level asc";
		@SuppressWarnings("unchecked")
		List<Object[]> result = (List<Object[]>)entityManager.createNativeQuery(sql)
                  .setParameter("divisionId", divId)
                  .setParameter("levels", levels)
                  .setParameter("reqUserId", reqUserId)
                  .getResultList();

        return result;
	}
	
	@Override
	public String getApproverLevelStage1(long reqUserId, long divId){
		String sql =   "SELECT distinct(level) FROM approvers where approvers.user_id= :reqUserId and division_id=:divId and level like 'L%'";
		@SuppressWarnings("unchecked")
		String result = (String)entityManager.createNativeQuery(sql)
                  .setParameter("reqUserId", reqUserId)
                  .setParameter("divId", divId)
                  .getSingleResult();

        return result;
	}
	
	@Override
	public String getApproverLevelStage2(long reqUserId){
		String sql =   "SELECT distinct(level) FROM approvers where approvers.user_id= :reqUserId and level like 'P%'";
		@SuppressWarnings("unchecked")
		String result = (String)entityManager.createNativeQuery(sql)
                  .setParameter("reqUserId", reqUserId)
                  .getSingleResult();

        return result;
	}
	
	
	@Override
	public Object[] getPreviousApproverLevel(long purchaseOrderId, String level){
		String sql =   "SELECT level,status,updated_by_user_id  FROM purchase_order_history where purchase_order_id =:orderId and status ='APPROVED' and level <:level order by level desc LIMIT 1;";
		@SuppressWarnings("unchecked")
		Object[] result = (Object[])entityManager.createNativeQuery(sql)
                  .setParameter("orderId", purchaseOrderId)
                  .setParameter("level", level)
                  .getSingleResult();

        return result;
	}


}
