package com.vantec.epr.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.vantec.epr.entity.DeliveryNote;
import com.vantec.epr.entity.PurchaseOrder;


@Component
public interface PurchaseOrderDeliveryNotesRepository extends JpaRepository<DeliveryNote, Serializable>{
	List<DeliveryNote> findByPurchaseOrder(PurchaseOrder po);
}
