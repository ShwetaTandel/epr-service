package com.vantec.epr.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import com.vantec.epr.entity.Invoice;
import com.vantec.epr.entity.InvoiceDetails;
import com.vantec.epr.entity.PurchaseOrder;


@Component
public interface PurchaseOrderInvoiceDetailsRepository extends JpaRepository<InvoiceDetails, Serializable>{
	
	List<InvoiceDetails> findByInvoiceAndItemName(Invoice inv, String itemName);
	List<InvoiceDetails> findByInvoice(Invoice inv);
	
	
	@Query("SELECT sum(a.invoicedQty*a.invoicedPrice), i.deptToCharge, i.expenseCode FROM InvoiceDetails a join a.item i where a.invoice=:invoice and  i.purchaseOrder=:porder group by i.deptToCharge,i.expenseCode ")
	//@Query("SELECT sum(a.invoicedQty*a.invoicedPrice), i.deptToCharge, i.expenseCode FROM InvoiceDetails a join PurchaseOrderItems i where i = a.item and  a.invoice=:invoice and  i.purchaseOrder=:porder group by i.deptToCharge,i.expenseCode ")
	List<Object[]> findDeptChargedInInvoice(@Param("invoice") Invoice inv, @Param("porder") PurchaseOrder po);
	
	
	
	
	
}
