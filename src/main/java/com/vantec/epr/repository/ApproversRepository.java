package com.vantec.epr.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import com.vantec.epr.entity.Approvers;


@Component
public interface ApproversRepository extends JpaRepository<Approvers, Serializable>{

	@Query("SELECT a FROM Approvers a where a.userId =:userId and a.level like 'L%'")
	Approvers findStage1ApproversByUserId(@Param("userId") Long userId);
	
	List<Approvers> findByUserId(@Param("userId") Long userId);
	
	@Query("SELECT a FROM Approvers a where a.userId =:userId and a.divisionId=:divisionId and a.level like 'L%'")
	public Approvers findByUserIdAndDivisionIdForLLevel(@Param("userId") Long userId, @Param("divisionId") Long divisionId);
	
	
	@Query("SELECT count(a) FROM Approvers a WHERE a.divisionId=:divId and a.userId =:userId and a.level like 'L%'")
    public Integer approverExistsByDivId(@Param("userId") Long userId,@Param("divId") Long divId);
	
}
