package com.vantec.epr.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.vantec.epr.entity.PurchaseOrderHistory;


@Component
public interface PurchaseOrderHisrtoryRepository extends JpaRepository<PurchaseOrderHistory, Serializable>{

	PurchaseOrderHistory findById(Long id);

}
