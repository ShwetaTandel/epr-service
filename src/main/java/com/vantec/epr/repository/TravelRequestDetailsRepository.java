package com.vantec.epr.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.vantec.epr.entity.TravelRequestDetails;


@Component
public interface TravelRequestDetailsRepository extends JpaRepository<TravelRequestDetails, Serializable>{

	

}
