package com.vantec.epr.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import com.vantec.epr.entity.PurchaseOrder;




@Component
public interface PurchaseOrderRepository extends JpaRepository<PurchaseOrder, Serializable>{

	
	@Query("SELECT po FROM PurchaseOrder po where po.currApproverId =:approverId and SUBSTRING(po.status,LOCATE('_', po.status)) =:status and po.grand_total != 0 order by po.createAt desc") 
    List<PurchaseOrder> findByCurrApproverAndStatus(@Param("approverId") Long approverId,@Param("status") String status);

	@Query("SELECT po FROM PurchaseOrder po where po.status = 'P1_PENDING' and po.grand_total != 0 order by po.createAt desc") 
    List<PurchaseOrder> findOrdersForP0();

	
	@Query("SELECT po FROM PurchaseOrder po where po.requestorId =:requestorId and SUBSTRING(po.status,LOCATE('_', po.status)) =:status order by po.createAt desc") 
	List<PurchaseOrder> findByRequestorAndStatus(@Param("requestorId") Long requestorId,@Param("status") String status);
	
	List<PurchaseOrder> findByCurrApproverIdOrderByCreateAtDesc(Long approverId);
	List<PurchaseOrder> findByRequestorIdOrderByCreateAtDesc(Long requestorId);
	
	@Query("SELECT po FROM PurchaseOrder po where po.requestorDeptId in :departments order by po.updatedAt desc") 
    List<PurchaseOrder> findByDepartments(@Param("departments") List<Long> departments);

	@Query("SELECT po FROM PurchaseOrder po where po.deptToChargeId in :costCentre order by po.updatedAt desc") 
    List<PurchaseOrder> findByCostCentre(@Param("costCentre") List<Long> costCentre);

	
	@Query("SELECT po FROM PurchaseOrder po where SUBSTRING(po.status,LOCATE('_', po.status)) in ('_PENDING','_REJECTED') order by po.updatedAt desc")
	List<PurchaseOrder> findAllInProcess();
	
	List<PurchaseOrder> findByStatusOrderByUpdatedAtDesc(String status);
	
}
