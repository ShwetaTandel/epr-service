package com.vantec.epr.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.vantec.epr.entity.Invoice;
import com.vantec.epr.entity.PurchaseOrder;


@Component
public interface PurchaseOrderInvoiceRepository extends JpaRepository<Invoice, Serializable>{
	List<Invoice> findByPurchaseOrder(PurchaseOrder po);
	Invoice findByPurchaseOrderAndInvoiceNumber(PurchaseOrder po, String invoiceNumber);
	Invoice findById(Long id);
	
}
