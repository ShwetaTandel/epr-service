package com.vantec.epr.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.vantec.epr.entity.BudgetHeader;
import com.vantec.epr.entity.Users;




@Component
public interface BudgetHeaderRepository extends JpaRepository<BudgetHeader, Serializable>{
	
	
	BudgetHeader findByFinancialYearAndDepartmentName(String year,String deptName);

	
}
