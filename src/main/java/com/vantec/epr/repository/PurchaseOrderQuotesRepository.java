package com.vantec.epr.repository;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import com.vantec.epr.entity.PurchaseOrderQuotes;




@Component
public interface PurchaseOrderQuotesRepository extends JpaRepository<PurchaseOrderQuotes, Serializable>{

	@Modifying
	@Query(value = "delete FROM PurchaseOrderQuotes q WHERE q.purchaseOrderId= :orderId and q.quote NOT IN :quotes")
	void deleteByQuoteNamesAndOrderId(@Param("orderId") Long orderId,@Param("quotes") Collection<String> quotes);
	
	List<PurchaseOrderQuotes> findByPurchaseOrderId(Long poId);
	
	@Modifying
	void deleteByPurchaseOrderId(Long orderId);

}
