package com.vantec.epr.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import com.vantec.epr.entity.Department;


@Component
public interface DepartmentRepository extends JpaRepository<Department, Serializable>{

	Department findById(Long deptId);
	
	@Query("SELECT a FROM Department a where a.divisionId =:divisionId and a.isActive =true")
	List<Department> findByDivisionId( @Param("divisionId") Long divisionId);

}
