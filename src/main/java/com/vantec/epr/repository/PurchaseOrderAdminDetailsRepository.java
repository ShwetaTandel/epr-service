package com.vantec.epr.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.vantec.epr.entity.PurchaseOrder;
import com.vantec.epr.entity.PurchaseOrderAdminDetails;


@Component
public interface PurchaseOrderAdminDetailsRepository extends JpaRepository<PurchaseOrderAdminDetails, Serializable>{
	
	PurchaseOrderAdminDetails findByPurchaseOrder(PurchaseOrder order);
}
