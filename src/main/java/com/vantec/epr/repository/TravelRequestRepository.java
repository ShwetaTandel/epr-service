package com.vantec.epr.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.vantec.epr.entity.TravelRequest;


@Component
public interface TravelRequestRepository extends JpaRepository<TravelRequest, Serializable>{

	

}
