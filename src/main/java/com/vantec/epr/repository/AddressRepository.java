package com.vantec.epr.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import com.vantec.epr.entity.Address;


@Component
public interface AddressRepository extends JpaRepository<Address, Serializable>{
	
	Address findByCode(@Param("code") String code);

}
