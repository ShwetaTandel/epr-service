package com.vantec.epr.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.vantec.epr.entity.DeptExpense;


@Component
public interface DeptExpenseRepository extends JpaRepository<DeptExpense, Serializable>{

	
	
}
