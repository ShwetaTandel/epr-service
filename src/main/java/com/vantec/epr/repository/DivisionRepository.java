package com.vantec.epr.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.vantec.epr.entity.Department;
import com.vantec.epr.entity.Division;


@Component
public interface DivisionRepository extends JpaRepository<Division, Serializable>{

	Department findById(Long divId);

}
