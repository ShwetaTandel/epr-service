package com.vantec.epr.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.vantec.epr.entity.CreditNote;
import com.vantec.epr.entity.PurchaseOrder;


@Component
public interface PurchaseOrderCreditNoteRepository extends JpaRepository<CreditNote, Serializable>{
	List<CreditNote> findByPurchaseOrder(PurchaseOrder po);
	
}
