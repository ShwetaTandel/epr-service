package com.vantec.epr.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.vantec.epr.entity.TravelAccommodationDetails;


@Component
public interface TravelRequestAccommodationRepository extends JpaRepository<TravelAccommodationDetails, Serializable>{

	

}
