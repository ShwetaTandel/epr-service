package com.vantec.epr.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import com.vantec.epr.entity.Approvers;
import com.vantec.epr.entity.Supplier;


@Component
public interface SupplierRepository extends JpaRepository<Supplier, Serializable>{

	
}
