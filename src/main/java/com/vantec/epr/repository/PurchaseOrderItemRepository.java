package com.vantec.epr.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import com.vantec.epr.entity.PurchaseOrder;
import com.vantec.epr.entity.PurchaseOrderItems;




@Component
public interface PurchaseOrderItemRepository extends JpaRepository<PurchaseOrderItems, Serializable>{

		//void deleteByPurchaseOrder(PurchaseOrder order);
	@Query("SELECT Sum(c.qty * c.unitPrice),c.itemName,c.deptToCharge,c.expenseCode  FROM PurchaseOrderItems c where c.purchaseOrder =:purchaseOrder GROUP BY c.deptToCharge, c.expenseCode")
	List<Object[]>  findItemsByDeptToCharge(@Param("purchaseOrder")PurchaseOrder purchaseOrder);
	
	@Query("SELECT p  FROM PurchaseOrderItems p where id =:itemId")
	PurchaseOrderItems findById(@Param("itemId")Long itemId);
	
	@Query("SELECT p  FROM PurchaseOrderItems p where capexId =:capexId")
	PurchaseOrderItems findByCapexId(@Param("capexId")Long capexId);
	

}
