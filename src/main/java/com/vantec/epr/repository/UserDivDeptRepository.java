package com.vantec.epr.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.vantec.epr.entity.UserDivisionDepartment;
import com.vantec.epr.entity.Users;


@Component
public interface UserDivDeptRepository extends JpaRepository<UserDivisionDepartment, Serializable>{


	UserDivisionDepartment findByUser(Users user);
}
