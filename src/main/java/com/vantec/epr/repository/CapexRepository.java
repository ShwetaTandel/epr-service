package com.vantec.epr.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.vantec.epr.entity.Capex;

@Repository
public interface CapexRepository extends JpaRepository<Capex, Serializable>{
	
	public List<Capex> findByRequestorId(Long reqId);
	public List<Capex> findByRequestorIdOrderByCreatedAtDesc(Long reqId);
	
	public List<Capex> findByStatusNot(String status);
	
	@Query("SELECT c FROM Capex c where c.status <> 'APPROVED' and c.status <> 'CLOSED' order by c.createdAt desc") 
	List<Capex> findAllPendingRequests();

}
