package com.vantec.epr.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;

import com.vantec.epr.entity.Users;


@Component
public interface UsersRepository extends JpaRepository<Users, Serializable>{

	Users findById(Long userId);
	
	Users findByFirstName(String firstName);
	
	@Query("SELECT max(u.employeeId) FROM Users u")
	Long findMaxEmployeedId();
	
	@Query("SELECT concat(u.firstName,' ',u.lastName) as fullName FROM Users u where id =:userId")
	String getFullNamebyId(@Param("userId")Long userId);


}
