package com.vantec.epr.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.vantec.epr.entity.BudgetDetail;




@Component
public interface BudgetDetailRepository extends JpaRepository<BudgetDetail, Serializable>{

	
	BudgetDetail findByBudgetBodyIdAndMonth(Long id,String month);
	
	List<BudgetDetail> findAllByBudgetBodyIdOrderByIdAsc(Long id);
	
	@Query(value = "select * from budget_detail where budget_body_id =:bodyId order by id asc limit 1", nativeQuery = true)
	BudgetDetail findFirstDetail(@Param("bodyId")Long id);
	
	@Modifying
	@Transactional
	@Query(value = "delete b.* from budget_detail b where b.budget_body_id =:bodyId", nativeQuery = true)
	void deleteAllByBudgetBodyId(@Param("bodyId")Long id);
	
}
