package com.vantec.epr.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vantec.epr.entity.EquipmentRequests;

@Repository
public interface EquipmentsReqRepository extends JpaRepository<EquipmentRequests, Serializable> {

}
