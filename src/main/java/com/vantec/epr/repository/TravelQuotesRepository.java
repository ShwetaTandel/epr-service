package com.vantec.epr.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.vantec.epr.entity.TravelQuotes;

@Component
public interface TravelQuotesRepository extends JpaRepository<TravelQuotes, Serializable>{

	List<TravelQuotes> findByTravelRequestId(Long travelId);
	
}
