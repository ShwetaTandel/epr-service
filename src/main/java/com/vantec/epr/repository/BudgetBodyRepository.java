package com.vantec.epr.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.vantec.epr.entity.BudgetBody;




@Component
public interface BudgetBodyRepository extends JpaRepository<BudgetBody, Serializable>{

	
	BudgetBody findById(Long id);
	
	List<BudgetBody> findAll();

	
}
