package com.vantec.epr.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;




@Entity
@Table(name = "purchase_order")
public class PurchaseOrder implements Serializable {

	public String getRechargeOthers() {
		return rechargeOthers;
	}

	public void setRechargeOthers(String rechargeOthers) {
		this.rechargeOthers = rechargeOthers;
	}

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "purchase_order_id")
	private Long purchaseOrderId;
	
	
	@Column(name = "purchase_order_number")
	private Long purchaseOrderNumber;


	
	@Column(name = "requestor_id")
	private Long requestorId;

	@Column(name = "title")
	private String title;

	@Column(name = "requested_date")
	private Date requestedDate;
	
	@Column(name = "delivery_date")
	private Date deliveryDate;
	
	@Column (name= "requestor_dept_id")
	private Long requestorDeptId;
	
	@Column (name= "budget_body_id")
	private Long budgetBodyId;
	
	@Column (name= "is_over_budget")
	private Boolean isOverBudget;
	
	@Column(name = "purchase_type")
	private String purchaseType;
	
	@Column(name = "project_name")
	private String projectName;

	@Column(name = "reason_type")
	private String reasonType;
	
	@Column(name = "reason_description")
	private String reasonDescription;
	
	@Column(name = "dept_id_to_charge")
	private Long deptToChargeId;
	
	@Column(name = "is_budgeted")
	private String isBudgeted;

	@Column(name = "is_recharge")
	private Boolean isRecharge;
	
	@Column(name = "is_accrued")
	private Boolean isAccrued;

	
	@Column(name = "is_emergency")
	private Boolean isEmergency;

	@Column(name = "is_emailed")
	private Boolean isEmailed;

	@Column(name = "is_finalized")
	private Boolean isFinalized;


	@Column(name = "recharge_ref")
	private String rechargeRef;
	
	@Column(name = "recharge_to")
	private String rechargeTo;
	
	@Column(name = "recharge_others")
	private String rechargeOthers;

	@Column(name = "quote_supplied_by")
	private String quoteSuppliedBy;
	
	
	@Column(name = "preferred_supplier")
	private String preferred_supplier;
	
	@Column(name = "reason_of_choice")
	private String reasonOfChoice;
	
	
	@Column(name = "supplier_contact_details")
	private String supplierContactDetails;
	
	@Column(name = "doa_approval_req")
	private Boolean doaApprovalRequired;
	
	@Column(name = "is_gdprflagged")
	private Boolean isGDPRFlagged;

	
	@Column(name = "number_of_quotes")
	private Integer numberOfQuotes;
	
	@Column(name = "special_requirements")
	private String specialRequirements;
	@Column(name = "discounts_achieved")
	private String discountsAchieved;
	
	
	@Column(name = "contract_to_date")
	private Date contractToDate;
	

	@Column(name = "contract_from_date")
	private Date contractFromDate;
	
	
	@Column(name = "discount")
	private Float discount;
	

	@Column(name = "carrier_charges")
	private Float carrierCharges;
	

	@Column(name = "grand_total")
	private Float grand_total;
	
	
	@Column(name = "delivery_address")
	private String deliveryAddress;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "curr_approver_id")
	private Long currApproverId;
	
	@Column(name = "approval_date")
	private Date approvalDate;
	
	public Date getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(Date approvalDate) {
		this.approvalDate = approvalDate;
	}

	public Boolean getIsFinalized() {
		return isFinalized;
	}

	public void setIsFinalized(Boolean isFinalized) {
		this.isFinalized = isFinalized;
	}

	public Boolean getIsGDPRFlagged() {
		return isGDPRFlagged;
	}

	public void setIsGDPRFlagged(Boolean isGDPRFlagged) {
		this.isGDPRFlagged = isGDPRFlagged;
	}

	public Boolean getIsAccrued() {
		return isAccrued;
	}

	public void setIsAccrued(Boolean isAccrued) {
		this.isAccrued = isAccrued;
	}

	public Long getPurchaseOrderNumber() {
		return purchaseOrderNumber;
	}

	public void setPurchaseOrderNumber(Long purchaseOrderNumber) {
		this.purchaseOrderNumber = purchaseOrderNumber;
	}

	
	public Boolean getIsEmailed() {
		return isEmailed;
	}

	public void setIsEmailed(Boolean isEmailed) {
		this.isEmailed = isEmailed;
	}

	public Boolean getIsEmergency() {
		return isEmergency;
	}

	public void setIsEmergency(Boolean isEmergency) {
		this.isEmergency = isEmergency;
	}

	public Long getCurrApproverId() {
		return currApproverId;
	}

	public void setCurrApproverId(Long currApproverId) {
		this.currApproverId = currApproverId;
	}
	@OneToOne(mappedBy = "purchaseOrder")
	private PurchaseOrderAdminDetails adminDetails;

	@OneToMany(mappedBy = "purchaseOrder")
	private List<PurchaseOrderDeptsToCharge> deptsToCharge = new ArrayList<PurchaseOrderDeptsToCharge>();

	public List<PurchaseOrderDeptsToCharge> getDeptsToCharge() {
		return deptsToCharge;
	}

	public void setDeptsToCharge(List<PurchaseOrderDeptsToCharge> deptsToCharge) {
		this.deptsToCharge = deptsToCharge;
	}

	public PurchaseOrderAdminDetails getAdminDetails() {
		return adminDetails;
	}

	public void setAdminDetails(PurchaseOrderAdminDetails adminDetails) {
		this.adminDetails = adminDetails;
	}

	@OneToMany(mappedBy = "purchaseOrder")
	private List<PurchaseOrderItems> items = new ArrayList<PurchaseOrderItems>();
	public List<PurchaseOrderItems> getItems() {
		return items;
	}
	public void setItems(List<PurchaseOrderItems> items) {
		this.items = items;
	}
	@Column(name = "created_at")
	private Date createAt;
	
	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "updated_at")
	private Date updatedAt;

	@Column(name = "last_updated_by")
	private String lastUpdatedBy;

	public PurchaseOrder() {
	}
	

	public String getIsBudgeted() {
		return isBudgeted;
	}

	public void setIsBudgeted(String isBudgeted) {
		this.isBudgeted = isBudgeted;
	}

	public Long getPurchaseOrderId() {
		return purchaseOrderId;
	}

	public void setPurchaseOrderId(Long purchaseOrderId) {
		this.purchaseOrderId = purchaseOrderId;
	}

	public Long getRequestorId() {
		return requestorId;
	}

	public void setRequestorId(Long requestorId) {
		this.requestorId = requestorId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getRequestedDate() {
		return requestedDate;
	}

	public void setRequestedDate(Date requestedDate) {
		this.requestedDate = requestedDate;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public Long getRequestorDeptId() {
		return requestorDeptId;
	}

	public void setRequestorDeptId(Long requestorDeptId) {
		this.requestorDeptId = requestorDeptId;
	}

	public String getPurchaseType() {
		return purchaseType;
	}

	public void setPurchaseType(String purchaseType) {
		this.purchaseType = purchaseType;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getReasonType() {
		return reasonType;
	}

	public void setReasonType(String reasonType) {
		this.reasonType = reasonType;
	}

	public String getReasonDescription() {
		return reasonDescription;
	}

	public void setReasonDescription(String reasonDescription) {
		this.reasonDescription = reasonDescription;
	}

	public Long getDeptToChargeId() {
		return deptToChargeId;
	}

	public void setDeptToChargeId(Long deptToChargeId) {
		this.deptToChargeId = deptToChargeId;
	}

	public Boolean getIsRecharge() {
		return isRecharge;
	}

	public void setIsRecharge(Boolean isRecharge) {
		this.isRecharge = isRecharge;
	}

	public String getRechargeRef() {
		return rechargeRef;
	}

	public void setRechargeRef(String rechargeRef) {
		this.rechargeRef = rechargeRef;
	}

	public String getRechargeTo() {
		return rechargeTo;
	}

	public void setRechargeTo(String rechargeTo) {
		this.rechargeTo = rechargeTo;
	}

	public String getQuoteSuppliedBy() {
		return quoteSuppliedBy;
	}

	public void setQuoteSuppliedBy(String quoteSuppliedBy) {
		this.quoteSuppliedBy = quoteSuppliedBy;
	}

	public String getPreferred_supplier() {
		return preferred_supplier;
	}

	public void setPreferred_supplier(String preferred_supplier) {
		this.preferred_supplier = preferred_supplier;
	}

	public String getReasonOfChoice() {
		return reasonOfChoice;
	}

	public void setReasonOfChoice(String reasonOfChoice) {
		this.reasonOfChoice = reasonOfChoice;
	}

	public String getSupplierContactDetails() {
		return supplierContactDetails;
	}

	public void setSupplierContactDetails(String supplierContactDetails) {
		this.supplierContactDetails = supplierContactDetails;
	}

	public Boolean getDoaApprovalRequired() {
		return doaApprovalRequired;
	}

	public void setDoaApprovalRequired(Boolean doaApprovalRequired) {
		this.doaApprovalRequired = doaApprovalRequired;
	}

	public Integer getNumberOfQuotes() {
		return numberOfQuotes;
	}

	public void setNumberOfQuotes(Integer numberOfQuotes) {
		this.numberOfQuotes = numberOfQuotes;
	}

	public String getSpecialRequirements() {
		return specialRequirements;
	}

	public void setSpecialRequirements(String specialRequirements) {
		this.specialRequirements = specialRequirements;
	}

	public String getDiscountsAchieved() {
		return discountsAchieved;
	}

	public void setDiscountsAchieved(String discountsAchieved) {
		this.discountsAchieved = discountsAchieved;
	}

	public Date getContractToDate() {
		return contractToDate;
	}

	public void setContractToDate(Date contractToDate) {
		this.contractToDate = contractToDate;
	}

	public Date getContractFromDate() {
		return contractFromDate;
	}

	public void setContractFromDate(Date contractFromDate) {
		this.contractFromDate = contractFromDate;
	}

	public Float getDiscount() {
		return discount;
	}

	public void setDiscount(Float discount) {
		this.discount = discount;
	}

	public Float getCarrierCharges() {
		return carrierCharges;
	}

	public void setCarrierCharges(Float carrierCharges) {
		this.carrierCharges = carrierCharges;
	}

	public Float getGrand_total() {
		return grand_total;
	}

	public void setGrand_total(Float grand_total) {
		this.grand_total = grand_total;
	}

	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Long getBudgetBodyId() {
		return budgetBodyId;
	}

	public void setBudgetBodyId(Long budgetBodyId) {
		this.budgetBodyId = budgetBodyId;
	}

	public Boolean getIsOverBudget() {
		return isOverBudget;
	}

	public void setIsOverBudget(Boolean isOverBudget) {
		this.isOverBudget = isOverBudget;
	}
    


}
