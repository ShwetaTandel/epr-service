package com.vantec.epr.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;




@Entity
@Table(name = "travel_request")
public class TravelRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "travel_request_id")
	private Long travelRequestId;
	
	@Column(name = "travel_requested_by")
	private Long travelRequestedBy;
	
	@Column(name = "traveller_id")
	private Long travellerId;
	
	@Column(name = "dept_id_to_charge")
	private Long deptIdToCharge;


	@Column(name = "travel_reason")
	private String travelReason;

	@Column(name = "additional_info")
	private String additionalInfo;
	
	@Column(name = "travel_date")
	private Date travelDate;
	
	@Column(name = "return_date")
	private Date returnDate;
	
	@Column(name = "is_budgeted")
	private String isBudgeted;

	@Column(name = "is_recharge")
	private Boolean isRecharge;
	

	@Column(name = "recharge_ref")
	private String rechargeRef;
	
	@Column(name = "recharge_to")
	private String rechargeTo;
	
	@Column(name = "recharge_others")
	private String rechargeOthers;

	@Column(name = "is_gdprflagged")
	private Boolean isGDPRFlagged;

	@Column(name = "comments")
	private String comments;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "curr_approver_id")
	private Long currApproverId;
	
	@OneToMany(mappedBy = "travelRequest")
	private List<TravelRequestDetails> travelDetails = new ArrayList<TravelRequestDetails>();
	
	@OneToMany(mappedBy = "travelRequest")
	private List<TravelAccommodationDetails> travelHotelDetails = new ArrayList<TravelAccommodationDetails>();

	
	@Column(name = "created_at")
	private Date createAt;
	
	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "updated_at")
	private Date updatedAt;

	@Column(name = "last_updated_by")
	private String lastUpdatedBy;

	public TravelRequest() {
	}

	public Long getTravelRequestId() {
		return travelRequestId;
	}

	public void setTravelRequestId(Long travelRequestId) {
		this.travelRequestId = travelRequestId;
	}

	public Long getTravelRequestedBy() {
		return travelRequestedBy;
	}

	public void setTravelRequestedBy(Long travelRequestedBy) {
		this.travelRequestedBy = travelRequestedBy;
	}

	public Long getTravellerId() {
		return travellerId;
	}

	public void setTravellerId(Long travellerId) {
		this.travellerId = travellerId;
	}

	public String getTravelReason() {
		return travelReason;
	}

	public void setTravelReason(String travelReason) {
		this.travelReason = travelReason;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public Date getTravelDate() {
		return travelDate;
	}

	public void setTravelDate(Date travelDate) {
		this.travelDate = travelDate;
	}

	public Date getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}

	public String getIsBudgeted() {
		return isBudgeted;
	}

	public void setIsBudgeted(String isBudgeted) {
		this.isBudgeted = isBudgeted;
	}

	public Boolean getIsRecharge() {
		return isRecharge;
	}

	public void setIsRecharge(Boolean isRecharge) {
		this.isRecharge = isRecharge;
	}

	public String getRechargeRef() {
		return rechargeRef;
	}

	public void setRechargeRef(String rechargeRef) {
		this.rechargeRef = rechargeRef;
	}

	public String getRechargeTo() {
		return rechargeTo;
	}

	public void setRechargeTo(String rechargeTo) {
		this.rechargeTo = rechargeTo;
	}

	public String getRechargeOthers() {
		return rechargeOthers;
	}

	public void setRechargeOthers(String rechargeOthers) {
		this.rechargeOthers = rechargeOthers;
	}

	public Boolean getIsGDPRFlagged() {
		return isGDPRFlagged;
	}

	public void setIsGDPRFlagged(Boolean isGDPRFlagged) {
		this.isGDPRFlagged = isGDPRFlagged;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getCurrApproverId() {
		return currApproverId;
	}

	public void setCurrApproverId(Long currApproverId) {
		this.currApproverId = currApproverId;
	}

	public List<TravelRequestDetails> getTravelDetails() {
		return travelDetails;
	}

	public void setTravelDetails(List<TravelRequestDetails> travelDetails) {
		this.travelDetails = travelDetails;
	}

	public List<TravelAccommodationDetails> getTravelHotelDetails() {
		return travelHotelDetails;
	}

	public void setTravelHotelDetails(List<TravelAccommodationDetails> travelHotelDetails) {
		this.travelHotelDetails = travelHotelDetails;
	}

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Long getDeptIdToCharge() {
		return deptIdToCharge;
	}

	public void setDeptIdToCharge(Long deptIdToCharge) {
		this.deptIdToCharge = deptIdToCharge;
	}

}
