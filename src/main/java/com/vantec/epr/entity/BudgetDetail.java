package com.vantec.epr.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;




@Entity
@Table(name = "budget_detail")
public class BudgetDetail implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	
	@Column(name = "item")
	private String item;

	
	@Column(name = "category_name")
	private String categoryName;
	
	@Column(name = "cost_center_name")
	private String costCentreName;
	
	@Column(name = "budget_body_id")
	private Long budgetBodyId;
	
	@Column(name = "month")
	private String month;
	
	@Column(name = "year")
	private String year;

	
	@Column(name = "planned")
	private Double planned;

	@Column(name = "spend")
	private Double spend;


	
	@Column(name = "available")
	private Double available;
	
	@Column(name = "commited")
	private Double commited;
	
	@Column(name = "date_created")
	private Date dateCreated;
	
	@Column(name = "created_by")
	private String createdBy;

	@Column(name = "last_updated")
	private Date dateUpdated;

	@Column(name = "last_updated_by")
	private String updatedBy;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCostCentreName() {
		return costCentreName;
	}

	public void setCostCentreName(String costCentreName) {
		this.costCentreName = costCentreName;
	}



	public Double getPlanned() {
		return planned;
	}

	public void setPlanned(Double planned) {
		this.planned = planned;
	}

	public Double getSpend() {
		return spend;
	}

	public void setSpend(Double spend) {
		this.spend = spend;
	}



	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Long getBudgetBodyId() {
		return budgetBodyId;
	}

	public void setBudgetBodyId(Long budgetBodyId) {
		this.budgetBodyId = budgetBodyId;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public Double getCommited() {
		return commited;
	}

	public void setCommited(Double commited) {
		this.commited = commited;
	}

	public Double getAvailable() {
		return available;
	}

	public void setAvailable(Double available) {
		this.available = available;
	}
    
    




}
