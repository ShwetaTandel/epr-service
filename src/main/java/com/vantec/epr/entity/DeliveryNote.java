package com.vantec.epr.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "purchase_order_delivery_note")
public class DeliveryNote implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "purchase_order_id", referencedColumnName = "purchase_order_id")
	private PurchaseOrder purchaseOrder;

	@Column(name = "delivery_note_number")
	private String deliveryNoteNumber;
	
	@Column(name = "delivery_date")
	private Date deliveryDate;
	
	@Column(name = "total_amount")
	private Float totalAmount;
	
	@Column(name = "delivery_note_document")
	private String deliveryNoteDocument;
	
	

	@OneToMany(mappedBy = "deliveryNote")
	private List<DeliveryNoteDetails> deliveredItems = new ArrayList<DeliveryNoteDetails>();

	@Column(name = "created_at")
	private Date createAt;
	
	@Column(name = "created_by")
	private String createdBy;

	@Column(name = "updated_at")
	private Date updatedAt;

	
	public String getDeliveryNoteDocument() {
		return deliveryNoteDocument;
	}

	public void setDeliveryNoteDocument(String deliveryNoteDocument) {
		this.deliveryNoteDocument = deliveryNoteDocument;
	}

	public List<DeliveryNoteDetails> getDeliveredItems() {
		return deliveredItems;
	}

	public void setDeliveredItems(List<DeliveryNoteDetails> deliveredItems) {
		this.deliveredItems = deliveredItems;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PurchaseOrder getPurchaseOrder() {
		return purchaseOrder;
	}

	public void setPurchaseOrder(PurchaseOrder purchaseOrder) {
		this.purchaseOrder = purchaseOrder;
	}

	public String getDeliveryNoteNumber() {
		return deliveryNoteNumber;
	}

	public void setDeliveryNoteNumber(String deliveryNoteNumber) {
		this.deliveryNoteNumber = deliveryNoteNumber;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public Float getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Float totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	@Column(name = "last_updated_by")
	private String lastUpdatedBy;

	public DeliveryNote() {
	}


	
}
