package com.vantec.epr.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "purchase_order_admin_details")
public class PurchaseOrderAdminDetails {
	
	public String getIsProcessFollowed() {
		return isProcessFollowed;
	}

	public void setIsProcessFollowed(String isProcessFollowed) {
		this.isProcessFollowed = isProcessFollowed;
	}

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "purchase_order_id", referencedColumnName = "purchase_order_id")
	private PurchaseOrder purchaseOrder;

	@Column(name = "handled_by")
	private String handledBy;
	
	@Column(name = "is_already_purchased")
	private Boolean isAlreadyPurchased;
	
	@Column(name = "supplier_name_code")
	private String supplierNameCode;
	
	@Column(name = "payment_terms")
	private String paymentTerms;
	
	@Column(name = "payment_type")
	private String paymentType;

	
	@Column(name = "purchasing_comments")
	private String purchasingComments;
	
	@Column(name = "is_company_asset")
	private Boolean isCompanyAsset;
	
	@Column(name = "capex_complete")
	private Boolean capexComplete;
	
	@Column(name = "expense_type")
	private String expenseType;
	
	@Column(name = "is_process_followed")
	private String isProcessFollowed;
	
	@Column(name = "order_placed_date")
	private Date orderPlacedDate;
	
	
	public String getPaymentTerms() {
		return paymentTerms;
	}

	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public Date getOrderPlacedDate() {
		return orderPlacedDate;
	}

	public void setOrderPlacedDate(Date orderPlacedDate) {
		this.orderPlacedDate = orderPlacedDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PurchaseOrder getPurchaseOrder() {
		return purchaseOrder;
	}

	public void setPurchaseOrder(PurchaseOrder purchaseOrder) {
		this.purchaseOrder = purchaseOrder;
	}

	public String getHandledBy() {
		return handledBy;
	}

	public void setHandledBy(String handledBy) {
		this.handledBy = handledBy;
	}

	public Boolean getIsAlreadyPurchased() {
		return isAlreadyPurchased;
	}

	public void setIsAlreadyPurchased(Boolean isAlreadyPurchased) {
		this.isAlreadyPurchased = isAlreadyPurchased;
	}

	public String getSupplierNameCode() {
		return supplierNameCode;
	}

	public void setSupplierNameCode(String supplierNameCode) {
		this.supplierNameCode = supplierNameCode;
	}

	public String getPurchasingComments() {
		return purchasingComments;
	}

	public void setPurchasingComments(String purchasingComments) {
		this.purchasingComments = purchasingComments;
	}

	public Boolean getIsCompanyAsset() {
		return isCompanyAsset;
	}

	public void setIsCompanyAsset(Boolean isCompanyAsset) {
		this.isCompanyAsset = isCompanyAsset;
	}

	public Boolean getCapexComplete() {
		return capexComplete;
	}

	public void setCapexComplete(Boolean capexComplete) {
		this.capexComplete = capexComplete;
	}

	public String getExpenseType() {
		return expenseType;
	}

	public void setExpenseType(String expenseType) {
		this.expenseType = expenseType;
	}

	public String getFinanceNotes() {
		return financeNotes;
	}

	public void setFinanceNotes(String financeNotes) {
		this.financeNotes = financeNotes;
	}

	public Float getSavings() {
		return savings;
	}

	public void setSavings(Float savings) {
		this.savings = savings;
	}

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	@Column(name = "notes_to_finance")
	private String financeNotes;

	@Column(name = "savings")
	private Float savings;

	@Column(name = "created_at")
	private Date createAt;
	
	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "updated_at")
	private Date updatedAt;

	@Column(name = "last_updated_by")
	private String lastUpdatedBy;
	
	 public PurchaseOrderAdminDetails() {
	
	}

}
