package com.vantec.epr.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "travel_accommodation_details")
public class TravelAccommodationDetails implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "travel_request_id", referencedColumnName = "travel_request_id")
	private TravelRequest travelRequest;


	@Column(name = "checkin_date")
	private Date checkinDate;

	@Column(name = "checkout_date")
	private Date checkoutDate;
	
	@Column(name = "hotel_breakfast")
	private Boolean isHotelBreakfast;
	
	@Column(name = "hotel_evening_meal")
	private Boolean hotelEveningMeal;
	
	@Column(name = "per_night_charge")
	private Float perNightCharge;
	
	@Column(name = "breakfast_charge")
	private Float breakfastCharge;

	@Column(name = "meal_charge")
	private Float mealCharge;
	
	@Column(name = "supplier_name")
	private String suuplierName;



	public TravelAccommodationDetails() {
	}

	public Float getPerNightCharge() {
		return perNightCharge;
	}

	public void setPerNightCharge(Float perNightCharge) {
		this.perNightCharge = perNightCharge;
	}

	public Float getBreakfastCharge() {
		return breakfastCharge;
	}

	public void setBreakfastCharge(Float breakfastCharge) {
		this.breakfastCharge = breakfastCharge;
	}

	public Float getMealCharge() {
		return mealCharge;
	}

	public void setMealCharge(Float mealCharge) {
		this.mealCharge = mealCharge;
	}

	public String getSuuplierName() {
		return suuplierName;
	}

	public void setSuuplierName(String suuplierName) {
		this.suuplierName = suuplierName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TravelRequest getTravelRequest() {
		return travelRequest;
	}

	public void setTravelRequest(TravelRequest travelRequest) {
		this.travelRequest = travelRequest;
	}

	public Date getCheckinDate() {
		return checkinDate;
	}

	public void setCheckinDate(Date checkinDate) {
		this.checkinDate = checkinDate;
	}

	public Date getCheckoutDate() {
		return checkoutDate;
	}

	public void setCheckoutDate(Date checkoutDate) {
		this.checkoutDate = checkoutDate;
	}

	public Boolean getIsHotelBreakfast() {
		return isHotelBreakfast;
	}

	public void setIsHotelBreakfast(Boolean isHotelBreakfast) {
		this.isHotelBreakfast = isHotelBreakfast;
	}

	public Boolean getHotelEveningMeal() {
		return hotelEveningMeal;
	}

	public void setHotelEveningMeal(Boolean hotelEveningMeal) {
		this.hotelEveningMeal = hotelEveningMeal;
	}

	
}
