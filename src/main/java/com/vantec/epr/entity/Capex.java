package com.vantec.epr.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "capex")
public class Capex implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "payment_terms_id")
	private Long paymentTermsId;
	@Column(name = "months_depreciation")
	private Integer monthsDepreciation;
	@Column(name = "equipment_replacement")
	private Boolean equipmentReplacement;
	@Column(name = "purchace_type")
	private String purchaceType;
	@Column(name = "desc_justification")
	private String descJustification;
	@Column(name = "effect_on_ops")
	private String effectOnOps;
	@Column(name = "gm_approve_id")
	private Long gmApproveId;
	@Column(name = "finance_approve_id")
	private Long financeApproveId;
	@Column(name = "status")
	private String status;
	@Column(name = "requestor_id")
	private Long requestorId;
	@Column(name = "comments")
	private String comments;
	
	@Column(name = "created_at")
	private Date createdAt;
	
	@Column(name = "created_by")
	private String createdBy;
	
	@Column(name = "updated_at")
	private Date updatedAt;
	@Column(name = "last_updated_by")
	private String lastUpdatedBy;
	
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Long getRequestorId() {
		return requestorId;
	}

	public void setRequestorId(Long requestorId) {
		this.requestorId = requestorId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Capex() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	public Long getPaymentTermsId() {
		return paymentTermsId;
	}

	public void setPaymentTermsId(Long paymentTermsId) {
		this.paymentTermsId = paymentTermsId;
	}

	public Integer getMonthsDepreciation() {
		return monthsDepreciation;
	}

	public void setMonthsDepreciation(Integer monthsDepreciation) {
		this.monthsDepreciation = monthsDepreciation;
	}

	public Boolean getEquipmentReplacement() {
		return equipmentReplacement;
	}

	public void setEquipmentReplacement(Boolean equipmentReplacement) {
		this.equipmentReplacement = equipmentReplacement;
	}

	public String getPurchaceType() {
		return purchaceType;
	}

	public void setPurchaceType(String purchaceType) {
		this.purchaceType = purchaceType;
	}

	public String getDescJustification() {
		return descJustification;
	}

	public void setDescJustification(String descJustification) {
		this.descJustification = descJustification;
	}

	public String getEffectOnOps() {
		return effectOnOps;
	}

	public void setEffectOnOps(String effectOnOps) {
		this.effectOnOps = effectOnOps;
	}

	public Long getGmApproveId() {
		return gmApproveId;
	}

	public void setGmApproveId(Long gmApproveId) {
		this.gmApproveId = gmApproveId;
	}

	public Long getFinanceApproveId() {
		return financeApproveId;
	}

	public void setFinanceApproveId(Long financeApproveId) {
		this.financeApproveId = financeApproveId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	
	
	
}
