package com.vantec.epr.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "travel_request_details")
public class TravelRequestDetails implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "travel_request_id", referencedColumnName = "travel_request_id")
	private TravelRequest travelRequest;

	@Column(name = "travel_mode")
	private String travelMode;

	@Column(name = "outbound_date_time")
	private Date outboundDateTime;

	@Column(name = "inbound_date_time")
	private Date inboundDateTime;
	
	@Column(name = "is_roundtrip")
	private Boolean isRoundTrip;
	
	@Column(name = "is_hand_luggage")
	private Boolean isHandLuggage;

	@Column(name = "is_hold_luggage")
	private Boolean isHoldLuggage;
	
	@Column(name = "roundtrip_mileage")
	private Float roundTripMileage;

	@Column(name = "from_location")
	private String fromLocation;

	@Column(name = "to_location")
	private String toLocation;

	@Column(name = "is_car_parking")
	private Boolean carParking;
	
	@Column(name = "supplier_name")
	private String supplierName;
	
	@Column(name = "total_charge")
	private Float totalCharge;
	


	public TravelRequestDetails() {
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public Float getTotalCharge() {
		return totalCharge;
	}

	public void setTotalCharge(Float totalCharge) {
		this.totalCharge = totalCharge;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TravelRequest getTravelRequest() {
		return travelRequest;
	}

	public void setTravelRequest(TravelRequest travelRequest) {
		this.travelRequest = travelRequest;
	}

	public String getTravelMode() {
		return travelMode;
	}

	public void setTravelMode(String travelMode) {
		this.travelMode = travelMode;
	}

	public Date getOutboundDateTime() {
		return outboundDateTime;
	}

	public void setOutboundDateTime(Date outboundDateTime) {
		this.outboundDateTime = outboundDateTime;
	}

	public Date getInboundDateTime() {
		return inboundDateTime;
	}

	public void setInboundDateTime(Date inboundDateTime) {
		this.inboundDateTime = inboundDateTime;
	}

	public Boolean getIsRoundTrip() {
		return isRoundTrip;
	}

	public void setIsRoundTrip(Boolean isRoundTrip) {
		this.isRoundTrip = isRoundTrip;
	}

	public Boolean getIsHandLuggage() {
		return isHandLuggage;
	}

	public void setIsHandLuggage(Boolean isHandLuggage) {
		this.isHandLuggage = isHandLuggage;
	}

	public Boolean getIsHoldLuggage() {
		return isHoldLuggage;
	}

	public void setIsHoldLuggage(Boolean isHoldLuggage) {
		this.isHoldLuggage = isHoldLuggage;
	}

	public Float getRoundTripMileage() {
		return roundTripMileage;
	}

	public void setRoundTripMileage(Float roundTripMileage) {
		this.roundTripMileage = roundTripMileage;
	}

	public String getFromLocation() {
		return fromLocation;
	}

	public void setFromLocation(String fromLocation) {
		this.fromLocation = fromLocation;
	}

	public String getToLocation() {
		return toLocation;
	}

	public void setToLocation(String toLocation) {
		this.toLocation = toLocation;
	}

	public Boolean getCarParking() {
		return carParking;
	}

	public void setCarParking(Boolean carParking) {
		this.carParking = carParking;
	}

	
	
}
