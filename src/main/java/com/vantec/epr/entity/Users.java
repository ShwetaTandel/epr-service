package com.vantec.epr.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class Users implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "employee_id")
	private Long employeeId;
	
	@Column(name = "last_name")
	private String lastName;
	
	@Column(name = "position")
	private String position;
	
	@Column(name = "user_name")
	private String userName;
	
	@Column(name = "mobile_number")
	private String mobileNumber;
	
	@Column(name = "active")
	private Boolean active;
	
	@Column(name = "is_approver")
	private Boolean isApprover;
	
	@Column(name = "is_super_user")
	private Boolean isSuperUser;
	
	@Column(name = "is_admin")
	private Boolean isAdmin;

	@Column(name = "can_raise_emergency")
	private Boolean canRaiseEmergency;

	@Column(name = "is_p0")
	private Boolean isP0;
	
	@Column(name = "is_l0")
	private Boolean isL0;

	@Column(name = "is_f0")
	private Boolean isF0;
	
	@Column(name = "password")
	private String password;
	
	@Column(name = "email_id")
	private String emailId;
	
	@OneToMany(mappedBy = "user")
	private List<UserDivisionDepartment> userDivDeptList = new ArrayList<UserDivisionDepartment>();
	
	public List<UserDivisionDepartment> getUserDivDeptList() {
		return userDivDeptList;
	}
	public void setUserDivDeptList(List<UserDivisionDepartment> userDivDeptList) {
		this.userDivDeptList = userDivDeptList;
	}
	@Column(name = "created_at")
	private Date createAt;
	
	@Column(name = "createdBy")
	private String createdBy;

	@Column(name = "updated_at")
	private Date updatedAt;

	@Column(name = "last_updated_by")
	private String lastUpdatedBy;

	public Users() {
	}
	
	public Boolean getIsF0() {
		return isF0;
	}
	public void setIsF0(Boolean isF0) {
		this.isF0 = isF0;
	}
	public Boolean getIsL0() {
		return isL0;
	}
	public void setIsL0(Boolean isL0) {
		this.isL0 = isL0;
	}
	public Boolean getCanRaiseEmergency() {
		return canRaiseEmergency;
	}
	public void setCanRaiseEmergency(Boolean canRaiseEmergency) {
		this.canRaiseEmergency = canRaiseEmergency;
	}
	public Boolean getIsAdmin() {
		return isAdmin;
	}
	public void setIsAdmin(Boolean isAdmin) {
		this.isAdmin = isAdmin;
	}
	public Boolean getIsP0() {
		return isP0;
	}
	public void setIsP0(Boolean isP0) {
		this.isP0 = isP0;
	}

	
	public Long getId() {
		return id;
	}




	public void setId(Long id) {
		this.id = id;
	}




	public String getFirstName() {
		return firstName;
	}




	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}




	public Long getEmployeeId() {
		return employeeId;
	}




	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}




	public String getLastName() {
		return lastName;
	}




	public void setLastName(String lastName) {
		this.lastName = lastName;
	}




	public String getPosition() {
		return position;
	}




	public void setPosition(String position) {
		this.position = position;
	}




	public String getUserName() {
		return userName;
	}




	public void setUserName(String userName) {
		this.userName = userName;
	}




	public String getMobileNumber() {
		return mobileNumber;
	}




	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}




	public Boolean getActive() {
		return active;
	}




	public void setActive(Boolean active) {
		this.active = active;
	}




	public Boolean getIsApprover() {
		return isApprover;
	}




	public void setIsApprover(Boolean isApprover) {
		this.isApprover = isApprover;
	}




	public Boolean getIsSuperUser() {
		return isSuperUser;
	}




	public void setIsSuperUser(Boolean isSuperUser) {
		this.isSuperUser = isSuperUser;
	}




	public String getPassword() {
		return password;
	}




	public void setPassword(String password) {
		this.password = password;
	}




	public String getEmailId() {
		return emailId;
	}




	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}




	public Date getCreateAt() {
		return createAt;
	}




	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}




	public String getCreatedBy() {
		return createdBy;
	}




	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}




	public Date getUpdatedAt() {
		return updatedAt;
	}




	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}




	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}




	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}


}
