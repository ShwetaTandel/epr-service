package com.vantec.epr.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "purchase_order_quotes")
public class PurchaseOrderQuotes implements Serializable {

	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "purchase_order_id")
	private Long purchaseOrderId;


	@Column(name = "quote")
	private String quote;
	
	@Column(name = "path")
	private String path;
	
	public PurchaseOrderQuotes() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPurchaseOrder() {
		return purchaseOrderId;
	}

	public void setPurchaseOrder(Long orderId) {
		this.purchaseOrderId = orderId;
	}

	public String getQuote() {
		return quote;
	}

	public void setQuote(String quote) {
		this.quote = quote;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}


}
