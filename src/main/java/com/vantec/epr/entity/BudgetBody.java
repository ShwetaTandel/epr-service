package com.vantec.epr.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;




@Entity
@Table(name = "budget_body")
public class BudgetBody implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	
	@Column(name = "item")
	private String item;

	
	@Column(name = "category_name")
	private String categoryName;
	
	@Column(name = "cost_centre_name")
	private String costCentreName;
	
	@Column(name = "budget_header_id")
	private Long budgetheaderId;
	
	@Column(name = "sage_code")
	private String sageCode;
	
	@Column(name = "payment_frequency")
	private String paymentFrequency;
	
	@Column(name = "planned")
	private Double planned;
	
	@Column(name = "commited")
	private Double commited;
	
	@Column(name = "unit_price")
	private Double unitPrice;
	
	@Column(name = "no_of_items")
	private Integer noOfItems;

	@Column(name = "spend")
	private Double spend;

	@Column(name = "accrued")
	private Double accrued;
	
	@Column(name = "available")
	private Double available;
	
	@Column(name = "date_created")
	private Date dateCreated;
	
	@Column(name = "created_by")
	private String createdBy;

	@Column(name = "last_updated")
	private Date dateUpdated;

	@Column(name = "last_updated_by")
	private String updatedBy;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCostCentreName() {
		return costCentreName;
	}

	public void setCostCentreName(String costCentreName) {
		this.costCentreName = costCentreName;
	}

	public String getSageCode() {
		return sageCode;
	}

	public void setSageCode(String sageCode) {
		this.sageCode = sageCode;
	}

	public Double getPlanned() {
		return planned;
	}

	public void setPlanned(Double planned) {
		this.planned = planned;
	}

	public Double getSpend() {
		return spend;
	}

	public void setSpend(Double spend) {
		this.spend = spend;
	}

	public Double getAccrued() {
		return accrued;
	}

	public void setAccrued(Double accrued) {
		this.accrued = accrued;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getDateUpdated() {
		return dateUpdated;
	}

	public void setDateUpdated(Date dateUpdated) {
		this.dateUpdated = dateUpdated;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Long getBudgetheaderId() {
		return budgetheaderId;
	}

	public void setBudgetheaderId(Long budgetheaderId) {
		this.budgetheaderId = budgetheaderId;
	}

	public String getPaymentFrequency() {
		return paymentFrequency;
	}

	public void setPaymentFrequency(String paymentFrequency) {
		this.paymentFrequency = paymentFrequency;
	}

	public Double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Integer getNoOfItems() {
		return noOfItems;
	}

	public void setNoOfItems(Integer noOfItems) {
		this.noOfItems = noOfItems;
	}

	public Double getCommited() {
		return commited;
	}

	public void setCommited(Double commited) {
		this.commited = commited;
	}

	public Double getAvailable() {
		return available;
	}

	public void setAvailable(Double available) {
		this.available = available;
	}
    
    
    
    




}
