package com.vantec.epr.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "purchase_order_credit_note")
public class CreditNote implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "purchase_order_id", referencedColumnName = "purchase_order_id")
	private PurchaseOrder purchaseOrder;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "invoice_id", referencedColumnName = "id")
	private Invoice invoice;
	
	
	@Column(name = "credit_note_number")
	private String creditNoteNumber;
	
	@Column(name = "credit_note_description")
	private String creditNoteDesc;

	
	@Column(name = "credit_note_document")
	private String creditNoteDoc;
	
	@Column(name = "credit_note_date")
	private Date creditNoteDate;

	
	@Column(name = "credit_amount")
	private Float creditAmount;

	
	@Column(name = "created_at")
	private Date createAt;
	
	@Column(name = "created_by")
	private String createdBy;

	
	@Column(name = "updated_at")
	private Date updatedAt;

	@Column(name = "last_updated_by")
	private String lastUpdatedBy;
	

	public CreditNote() {
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public PurchaseOrder getPurchaseOrder() {
		return purchaseOrder;
	}


	public void setPurchaseOrder(PurchaseOrder purchaseOrder) {
		this.purchaseOrder = purchaseOrder;
	}


	public Invoice getInvoice() {
		return invoice;
	}


	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}


	public String getCreditNoteNumber() {
		return creditNoteNumber;
	}


	public void setCreditNoteNumber(String creditNoteNumber) {
		this.creditNoteNumber = creditNoteNumber;
	}


	public String getCreditNoteDesc() {
		return creditNoteDesc;
	}


	public void setCreditNoteDesc(String creditNoteDesc) {
		this.creditNoteDesc = creditNoteDesc;
	}


	public String getCreditNoteDoc() {
		return creditNoteDoc;
	}


	public void setCreditNoteDoc(String creditNoteDoc) {
		this.creditNoteDoc = creditNoteDoc;
	}


	public Date getCreditNoteDate() {
		return creditNoteDate;
	}


	public void setCreditNoteDate(Date creditNoteDate) {
		this.creditNoteDate = creditNoteDate;
	}


	public Float getCreditAmount() {
		return creditAmount;
	}


	public void setCreditAmount(Float creditAmount) {
		this.creditAmount = creditAmount;
	}


	public Date getCreateAt() {
		return createAt;
	}


	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}


	public String getCreatedBy() {
		return createdBy;
	}


	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}


	public Date getUpdatedAt() {
		return updatedAt;
	}


	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}


	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}


	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}



		
}
